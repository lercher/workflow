# Operational Data and Configuration for a Workflow Application Server

This directory contains the proposed structure for a fully
operational workflow processing engine.
It is usually specified as a parameter to the workflow application
server process but it can contain data and configuration
for an attached UI server process. The application server
normally scans subdirectories with well-known names at startup.

This is the particular substructure:

## config

subdir | comment
-------|--------
wf | contains the names *wf* of workflow types defining bahaviour of a workflow of type *wf*
view | global views on workflow envelope data
*N.N.* | other non workflow specific configuration *to be defined*, e.g. static assets, UI assets, sql queries, domains, etc.

## config/wf/*wf*

Contains at least the file `workflow.xaml` with the XAML stored definition
of a workflow (state engine) and a subdirectory `ts` with Typescript.
We currently have only `sample` for *wf* in this repository. It is
recommended to have a separate git repository for each group of
operational workflow definitions.

file | comment
-----|---------
workflow.xaml | contains a workflow definition
config.yaml | properties for this workflow, like creatable name, cratable by, access rights and so on. *to be definied*

## config/wf/*wf*/ts

Contains a bunch of name sorted Typescript files that define the
data model (`010model.ts`) and its metadata like envelope and index.
Furthermore files starting at `100*.ts` that implement Rulesets
that can be executed by the workflow activity `ExecuteRuleset`.

The application server associates a pool of goja VMs for each
bundle of scripts keyed by the name *wf*.

file | comment
-----|---------
010model.ts | Model(), $index() and $envelope() function
050interaction.ts | $rule() function
100-999*.ts | Ruleset functions

## config/wf/*wf*/*other*

Other ressources associated with a particular workflow *wf*. *To be defined.*
Examples are: text templates, UI templates, sql queries, maps, grids, domain-values, roles, rights etc.

file | comment
-----|---------
* | N.N.

## config/view

Views on workflow envelopes of all types

file | comment
-----|---------
my.yaml | my workflows
mine.yaml | created by me but owned by someone else currently
*.yaml | other view

## badger

Contains all files for storing Key/Value pairs of a kv-store
like Badger.

file | comment
-----|---------
* | badger defined
