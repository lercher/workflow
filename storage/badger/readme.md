# Key Value Store

Storing

- Atoms
- Schema
- Properties
- Index
- etc.

as defined by `pkg/atom`. In particular: workflow instances and their associated data, index and envelopes.
Plus logins, roles and user's properties.
