/// <reference path="010model.ts" />
/// <reference path="050interaction.ts" />

// files starting at number 100 are rules implementations
// files in the range 000-099 are system infrastructure scripts
// except 010model, which defines the model and its metadata

function Calculate(ws : WorkingSet) : void {
    const d = ws.data;
    d.terms.installment = d.asset.price / d.terms.duration;
}
