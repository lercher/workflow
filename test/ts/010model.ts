// file 010model.ts is used to specify
// Model data and its index and envelope properties

function Model() {
    return {
        customer: {
            name: "Simple Solutions PLC",
            street: "Homestreet 40a",
            contact: "Ms. Jane Simple",
        },
        contract: {
            nr: "C1001",
            type: "Mortgage",
        },
        asset: {
            description: "Drilling machine",
            price: 3601.78,
            currency: "EUR",
        },
        terms: {
            duration: 48,
            installment: 123.45,
            start: new Date("2019-12-03T00:04:37.288Z"),
        },
    };    
}


// see http://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-8.html#predefined-conditional-types

type $model = ReturnType<typeof Model>;

// $envelope creates a flat map from d
// for storing it as an envelope for listing multiple
// model data in a grid.
function $envelope(d : $model) {
    return {
        text1: d.customer.name,
        text2: d.asset.description,
        number1: d.asset.price,
        number2: d.terms.installment,
    }
}

// $index creates a flat map from d
// for creating inversion entry indices in a database
function $index(d : $model) {
    return {
        customername: d.customer.name,
        asset: d.asset.description,
        assetprice: d.asset.price,
        installment: d.terms.installment,
    }
}
