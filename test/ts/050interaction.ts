/// <reference path="010model.ts" />

// $create is called, when a new data model instance is created
function $create() : string {
    return JSON.stringify(Model());
}

interface WorkingSet {
    data : $model
    ruleset: string // from ExecuteRuleset activity
    action: string // from ExecuteRuleset activity
    envelope: ReturnType<typeof $envelope>
    index: ReturnType<typeof $index>
    user: any // current user, its roles and properties
    controller: any // interaction between UI, wf host and model
}

// $rule is called by the wf engine
// workingset is JSON serialized model data plus metadata described in WorkingSet
// rule is the name of a ruleset the wf engine wants to execute
// return value is the JSON serialized modified WorkingSet
function $rule(workingset : string) : string {
    let ws = <WorkingSet> JSON.parse(workingset);
    this[ws.ruleset](ws);
    ws.envelope = $envelope(ws.data);
    ws.index = $index(ws.data);    
    return JSON.stringify(ws);
}
