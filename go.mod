module gitlab.com/lercher/workflow

go 1.12

require (
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/dop251/goja v0.0.0-20190912223329-aa89e6a4c733
	github.com/go-sourcemap/sourcemap v2.1.2+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/kr/pretty v0.1.0 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
