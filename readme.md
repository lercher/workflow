# Workflow ![Build Status](https://gitlab.com/lercher/workflow/badges/master/pipeline.svg) ![License MIT](https://img.shields.io/badge/license-MIT-green.svg)

![Status](https://img.shields.io/badge/Status-DEV-red.svg)  
under development, many features missing, API unstable.

This library intends to make some .Net WF4 StateMachines
executable under Go. It features (not yet of course!) these main components:

* Loading a StateMachine workflow definition from a XAML stream
* Graphical workflow design can be done via Visual Studio
* It is planned to provide a .Net 4 designer component and
  executable to design StateEngines stand-alone
* .Net 4 Activities driving the extensions described below
  for use in the graphical designer
* A rich Go type model for StateEngines and Activities
* Go instances of workflows with a simple persistance model
  (i.e. a history of statenames plus one time)
* Rich inspection of workflow instances, i.e. list of active
  triggers, list of current source and target states, etc.
* Timeouts of states (after a timespan and immediate)
* Go execution engine
* Go scheduler
* Extensible runtime services in Go for activity use
* Extensible activity set in Go with .Net designer support.
  However, it's neccessary to co-implement and
  co-evolute (tivial) .Net Activities.

As an application of a workflow, we provide

* Operation on JSON based data (i.e. as `map[string]interface{}`)
  via JavaScript interpreted by dop251/goja as business rules
* Data with embedded workflow persistance most probably
  via dgraph-io/dgraph or its key value store badger

With that, we can have a data agnostic execution environment
with configurable behaviour via JavaScript files (or streams).

However, besides many goodies, WF4 lacks some functionality that
WF3 StateMachines were capable of. In particular

* Nesting of states with trigger (aka. event) inheritance
* Global triggers
* Queued state changes inside of sequences
* History-Back activity (not even in WF3)

we plan to provide this functionality in the processor.

## License

[MIT](LICENSE)

## The Scheduler

The scheduler coordinates wake-up timer events
of workflow instances stored in an (abstract) database
and the serialized execution of single instances.
The scheduler guarantees, that a particular workflow
instance identified by its ID is processed at most once
at any time.

It regularly polls against the data store and loads the
IDs and wakeup times of instances within a planning period
of time. It then programs a timer for the wakeup time
of the next instance due. On the timer fireing it sends
the correponding ID to its Due channel and moves the ID
to a list of running instances. On finished processing
the processing go routine returns the ID to the
scheduler's Return channel, which removes it from the
running list and places it back to the planned IDs list,
if it is due within the scheduler's planning period.

If an instance needs to be processed, because of
a trigger condition is met, the trigger/signal plus
the instance's ID is sent to the scheduler's Trigger
channel. The scheduler then does its housekeeping
and sends the ID/signal on the Due channel for
processing by the host.

The host decides on how to do concurrency on instances
with different IDs. There are this three principal options:

* Start a new goroutine on every message
  on the Due channel `for m := range sch.Due { go process(m) }`
* Pre-start N goroutines processing the same
  Due channel `for N-times { go func() { for m := range sch.Due { process(m) } }() }`
* Strict serialized execution `for m := range sch.Due {process(m)}`

## Typical Use Cases

See also the example programs in the `cmd` folder, like [wf-simulator](cmd/wf-simulator/main.go).

### Typical Imports

````go
import (
 "gitlab.com/lercher/workflow/pkg/workflow"
 "gitlab.com/lercher/workflow/pkg/workflow/activity"
 "gitlab.com/lercher/workflow/pkg/workflow/engine"
 "gitlab.com/lercher/workflow/pkg/workflow/inspect"
 "gitlab.com/lercher/workflow/pkg/workflow/scheduler"
)

````

### Loading a xaml workflow definition from a file

````go
func load(xamlfile string) (*workflow.Definition, error) {
 f, err := os.Open(xamlfile)
 if err != nil {
  return nil, err
 }
 defer f.Close()

 ld := workflow.NewLoader(f)
 activity.RegisterAll(ld)
 def, err := ld.Decode()
 if err != nil {
  return nil, err
 }

 return def, nil
}
````

### Creating and Starting an Instance from a Definition

````go
func create(def *workflow.Definition) (*workflow.Context, error) {
 // Creating an instance and an execution context
 ctx := workflow.NewContext(def.CreateInstance(), time.Now())
 // Initialize it
 err := engine.Run(ctx)
 return ctx, err
}
````

### Sending a Signal to Trigger an Event

````go
tis := inspect.Triggers(ctx.Instance) // all available TriggerInfos
ti := tis[n] // select an enabled TriggerInfo of kind Await
t := ti.Trigger // and its trigger (aka signal)
ctx.Now = time.Now()
err := engine.Process(ctx, t)
if err != nil {
    return err
}
````

### Timeout Events

If you have a Trigger variable `t` already:

````go
t := workflow.NullTrigger
ctx.Now = time.Now()
err := engine.Process(ctx, t)
if err != nil {
    return err
}
````

or just

````go
ctx.Now = time.Now()
err := engine.Run(ctx)
if err != nil {
    return err
}
````

### Using the Scheduler

TBD

### Operating a Workflow on a Data Model

TBD

### Dehydrating and Rehydrating a Workflow State

TBD

## Other Resources

We consider these components as infrastructure
well suited to compose a complete solution with:

* [https://github.com/json-iterator/go](https://github.com/json-iterator/go) - fast JSON encoding
* [https://github.com/dop251/goja](https://github.com/dop251/goja) - JavaScript processor
* [https://github.com/dgraph-io/dgraph](https://github.com/dgraph-io/dgraph) - Persistance (distributed graph db)
* [https://github.com/dgraph-io/badger](https://github.com/dgraph-io/badger) - Persistance (embeddable key value store)
* [https://nsq.io/](https://nsq.io/) - Message Queuing
* [https://github.com/julienschmidt/httprouter](https://github.com/julienschmidt/httprouter) - http mux
* [https://github.com/google/uuid](https://github.com/google/uuid) - UUID
