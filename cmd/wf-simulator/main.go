package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"
	"text/tabwriter"
	"time"

	"github.com/google/uuid"
	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/activity"
	"gitlab.com/lercher/workflow/pkg/workflow/behaviour"
	"gitlab.com/lercher/workflow/pkg/workflow/engine"
	"gitlab.com/lercher/workflow/pkg/workflow/inspect"
	"gitlab.com/lercher/workflow/pkg/workflow/scheduler"
)

var (
	flagLogScheduler = flag.Bool("s", false, "log scheduler messages")
	flagLogProcessor = flag.Bool("p", false, "log processor messages")
)

func main() {
	log.SetFlags(log.Lshortfile | log.Ltime)
	log.Println("This is", os.Args[0], "(C) 2019 by Martin Lercher")
	log.Println("See https://gitlab.com/lercher/workflow for details")
	flag.Parse()

	if flag.NArg() != 1 {
		log.Fatalln("no xaml-file given (or too many:)")
	}
	xamlfile := flag.Args()[0]

	def, err := load(xamlfile)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Loaded WF with", len(def.State), "state(s) from", xamlfile)

	svc := behaviour.New(false, nil)
	svc.SetOwner("Me")
	svc.SetTab("900ALL")

	errs := def.Validate(svc)
	if len(errs) > 0 {
		log.Fatalln("got validation errors:", errs)
	}

	err = run(def, svc)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Done")
}

func load(xamlfile string) (*workflow.Definition, error) {
	f, err := os.Open(xamlfile)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	ld := workflow.NewLoader(f)
	activity.RegisterAll(ld)
	def, err := ld.Decode()
	if err != nil {
		return nil, err
	}

	return def, nil
}

type db struct {
	workflow.Instance
}

func (db *db) InstancesDue(initial bool, until time.Time, item func(workflow.InstanceDescriptor)) error {
	item(workflow.InstanceDescriptor{ID: db.ID, WakeUp: db.WakeUp})
	return nil
}

func run(def *workflow.Definition, svc *behaviour.Behaviour) error {
	log.Println("Creating an instance and an execution context ...")
	db := &db{def.CreateInstance()}
	db.Instance.ID = workflow.ID(uuid.New())

	log.Println("wf", asGUID(db.Instance.ID), "will be initialized by the scheduler, b/c it is Due at", db.Instance.WakeUp)
	log.Println()
	log.Println()

	sch := scheduler.New(db, time.Hour, time.Minute)
	if *flagLogScheduler {
		sch.Logf = log.Printf
	}

	// Processor
	go func() {
		for sig := range sch.Due {
			ctx := workflow.NewContext(db.Instance, svc, time.Now())
			if *flagLogProcessor {
				ctx.Logf = log.Printf
			}
			err := engine.Process(ctx, sig.Trigger)
			if err != nil {
				log.Println("ERROR", err)
			}

			db.Instance = ctx.Instance
			w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', 0)
			list(w, db.Instance, svc)
			w.Flush()
			fmt.Printf("Enter # of signal to send to the WF, 'r'/Enter to just run or 'q' to quit:\n")

			sch.Return <- ctx.InstanceDescriptor()
		}
	}()

	var wg sync.WaitGroup
	wg.Add(1)
	// dispatcher
	go func() {
		err := sch.Run()
		if err != nil {
			log.Println("ERROR", err)
		}
		wg.Done()
	}()

	for db.Instance.Alive() {
	again:
		var input string
		fmt.Scanln(&input)
		if input == "" {
			input = "r"
		}
		if input == "q" {
			sch.Close <- true
			wg.Wait()
			return errors.New("quit by user request")
		}

		t := workflow.NullTrigger

		if input == "r" {
			fmt.Println()
			fmt.Println("******", "just", "***", "run", "*******")
		} else {
			n, err := strconv.Atoi(input)
			if err != nil {
				fmt.Println(err)
				goto again
			}

			tis := inspect.Triggers(db.Instance)
			if n < 0 || len(tis) <= n {
				fmt.Printf("#%d is out of bounds\n", n)
				goto again
			}
			ti := tis[n]

			fmt.Println()
			fmt.Println("******", ti.ID, "***", ti.Display, "*******")

			if !ti.Enabled {
				fmt.Println("Warning: Triggering the disabled", ti.String())
			}
			if ti.Kind != workflow.TriggerKindAwait {
				fmt.Println("Warning: Trigger is not TriggerKindAwait", ti.String())
			}

			t = ti.Trigger
		}

		sch.Signal <- t.Signal(db.InstanceDescriptor)
	}
	fmt.Println("workflow terminated")
	return nil
}

const line = "-----------------------------------------------------------"

func list(w io.Writer, in workflow.Instance, svc *behaviour.Behaviour) {
	fmt.Fprintln(w)
	fmt.Fprintln(w, line)
	if in.Alive() {
		fmt.Fprintln(w, "---\tState\t", in.Current)
	} else {
		fmt.Fprintln(w, "---\tState\t", in.Current, "(Final)")
	}
	fmt.Fprintln(w, "---\tID\t", asGUID(in.ID))
	fmt.Fprintln(w, "---\tEntered\t", in.StateEntered.Format("2006-01-02 15:04:05"))
	fmt.Fprintln(w, "---\tWake-up\t", in.WakeUp.Format("2006-01-02 15:04:05"))
	fmt.Fprintln(w, "---\tClock Now\t", time.Now().Format("2006-01-02 15:04:05"))
	fmt.Fprintln(w, "---\tOwner\t", svc.Owner)
	fmt.Fprintln(w, "---\tTab\t", svc.Tab)
	fmt.Fprintln(w, line)
	fmt.Fprintln(w)

	tis := inspect.Triggers(in)
	if len(tis) > 0 {
		fmt.Fprintf(w, "#\tKind\tEnabled\tDisplay\t\n")
		fmt.Fprintf(w, "-\t----\t-------\t-------\t\n")
		for i, ti := range tis {
			fmt.Fprintf(w, "%d.\t%v\t%v\t%v\t\n", i, ti, ti.Enabled, ti.Display)
		}
		fmt.Fprintln(w, line)
	}
}

func asGUID(ID workflow.ID) uuid.UUID {
	return uuid.UUID(ID)
}
