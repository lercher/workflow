package script

import (
	"sync/atomic"
	"time"

	"github.com/dop251/goja"
)

type poolItem struct {
	Locater
	key       Key
	timestamp time.Time
	es5       string
	rtpool    *runtimepool
	poolsize  int32
}

// vm returns an initialized goja.Runtime wrapped in a VM for this pi
func (pi *poolItem) vm() (*VM, error) {
	rt, err := pi.runtime()
	if err != nil {
		return nil, err
	}
	return &VM{Key: pi.key, Timestamp: pi.timestamp, Runtime: rt}, nil
}

// runtime either returns an initialized goja.Runtime from pi's pool
// or creates a new one and executes the cached transpiled ES5 script in it.
func (pi *poolItem) runtime() (*goja.Runtime, error) {
	if rt := pi.rtpool.Get(); rt != nil {
		return rt, nil
	}

	rt := goja.New()
	atomic.AddInt32(&pi.poolsize, 1)
	_, err := rt.RunScript(string(pi.key), pi.es5)
	if err != nil {
		return nil, err
	}
	return rt, nil
}
