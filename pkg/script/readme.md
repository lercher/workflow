# Where to Get Typescript 3.7

Download [https://github.com/microsoft/TypeScript/releases/download/v3.7.2/typescript-3.7.2.tgz](https://github.com/microsoft/TypeScript/releases/download/v3.7.2/typescript-3.7.2.tgz) or newer

Then extract `package/lib/typescriptServices.js`
