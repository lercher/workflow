// Package script provides a thread safe Typescript based
// execution environment based on the JS interpreter goja
package script

import (
	"fmt"
	"io"
	"sync"
	"sync/atomic"
	"time"
)

// Key denotes the key of a Locater in a Pool
type Key string

// Factory opens a ReadCloser to a typescript script in case a Locater is modified
// from a stored one
type Factory func() (io.ReadCloser, error)

// Locater is used to find a script and its last modification
type Locater interface {
	// Locate returns a last modification timestamp and a Reader factory function
	// for a script by its key. A pool uses it for initial and optimized
	// load and transpilation of a particular typescript script.
	// io.MultiReader and MultiLocater is handy, if scripts need to be combined.
	Locate() (timestamp time.Time, sf Factory, err error)
}

// Pool is a threadsafe pool of goja engines to execute
// Typescript scripts
type Pool struct {
	mu             sync.RWMutex
	m              map[Key]*poolItem
	TranspileCount int64
}

// NewSingle creates a Pool for a Single Locater
func NewSingle(l Locater) *Pool {
	return New(map[Key]Locater{"": l})
}

// New creates a new Pool of scripts for some Locators indexed by a Key
func New(ml map[Key]Locater) *Pool {
	p := &Pool{
		m: make(map[Key]*poolItem),
	}
	for k, l := range ml {
		p.m[k] = &poolItem{
			Locater: l,
			key:     k,
			rtpool:  nil,
		}
	}
	return p
}

// Get allocates a new goja VM from a pool of VMs kept by each script.
// The Pool's Locater is asked for the current modification timestamp,
// if it differs or the script wasn't in the Pool in the first place
// it is loaded and transpiled to es5 and then run in a new goja VM.
// If no available VM is in the script's pool, the cached es5 script
// is run in a new VM.
//
// If a script gets replaced by a newer one, all it's VMs are dropped
// either immediatly or on return via Put.
//
// The whole Pool is only read locked during script loading and transpiling,
// so under pressure multiple Locater.ScriptFactory Reads and transpiles
// can happen simultainously and all but one of them are overwritten.
//
// There is no method to delete a script or remove it from the pool
// other than to destroy the whole Pool itsef.
func (p *Pool) Get(key Key) (*VM, error) {
	p.mu.RLock() // can't use defer, b/c we need to lock p.mu later
	if pi, ok := p.m[key]; ok {
		p.mu.RUnlock()

		ts, sf, err := pi.Locate()
		if err != nil {
			return nil, err
		}

		if pi.rtpool != nil && pi.timestamp.Equal(ts) {
			// script found and nothing changed
			return pi.vm()
		}
		// different timestamp

		// this can happen in parallel and be overwritten by a 2nd read request:
		atomic.AddInt64(&p.TranspileCount, 1)
		es5, err := es5of(key, sf)
		if err != nil {
			return nil, err
		}

		p.mu.Lock()
		defer p.mu.Unlock()
		pi.timestamp = ts
		pi.es5 = es5
		pi.rtpool = newRuntimePool()
		atomic.StoreInt32(&pi.poolsize, 0)
		return pi.vm()
	}
	p.mu.RUnlock()
	return nil, fmt.Errorf("scriptkey '%v' not found in pool", key)
}

// es5of reads and transpiles the typescript to ES5
func es5of(key Key, sf Factory) (string, error) {
	rd, err := sf()
	if err != nil {
		return "", err
	}
	defer rd.Close()

	es5, err := Transpile(string(key), rd)
	if err != nil {
		return "", err
	}
	return es5, nil
}

// Put returns a VM to the Pool for future use by Get.
// It must be clean to reuse.
func (p *Pool) Put(vm *VM) {
	p.mu.RLock()
	defer p.mu.RUnlock()

	if pi, ok := p.m[vm.Key]; ok {
		if pi.timestamp.Equal(vm.Timestamp) {
			pi.rtpool.Put(vm.Runtime)
		}
		// different timestamp: drop vm
	}
	// script not found, should not happen
	// but if it does, we drop the vm gracefully
}

// Count returns approximative numbers of goja vms allocated by goja.New() for a key
func (p *Pool) Count(key Key) int {
	p.mu.RLock()
	defer p.mu.RUnlock()

	if pi, ok := p.m[key]; ok {
		return int(atomic.LoadInt32(&pi.poolsize))
	}
	return -1
}

// CountPool returns how many items we get from the vm pool until it returns nil.
// After counting we put all vms back to the pool.
func (p *Pool) CountPool(key Key) int {
	p.mu.RLock()
	defer p.mu.RUnlock()

	if pi, ok := p.m[key]; ok {
		return pi.rtpool.Count()
	}
	return -1
}
