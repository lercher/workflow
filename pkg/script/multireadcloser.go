package script

import "io"

type multireadcloser struct {
	io.Reader
	cs []io.Closer
}

func (mrc multireadcloser) Close() error {
	var err error
	for _, c := range mrc.cs {
		innererr := c.Close()
		if err == nil {
			err = innererr
		}
	}
	return err // first error, other errors ignored
}

// MultiReadCloser returns a ReadCloser that reads its components
// in sequence and closes them not until Close is called.
func MultiReadCloser(rcs ...io.ReadCloser) io.ReadCloser {
	rs, cs := make([]io.Reader, 0, len(rcs)), make([]io.Closer, 0, len(rcs))
	for _, r := range rcs {
		rs, cs = append(rs, r), append(cs, r)
	}

	return multireadcloser{
		Reader: io.MultiReader(rs...),
		cs:     cs,
	}
}
