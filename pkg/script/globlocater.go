package script

import (
	"path/filepath"
	"sort"
)

// GlobLocater creates a MultiLocator in file-path order.
// Pattern is passed to filepath.Glob to get a list of files.
// Its error can only be an invalid pattern.
func GlobLocater(pattern string) (Locater, error) {
	names, err := filepath.Glob(pattern)
	if err != nil {
		return nil, err
	}
	sort.Strings(names) // in file name order!
	list := make([]Locater, 0, len(names))
	for _, fn := range names {
		list = append(list, FileLocater(fn))
	}
	return MultiLocater(list...), nil
}
