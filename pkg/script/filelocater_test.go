package script

import (
	"io/ioutil"
	"strings"
	"testing"
)

const testscript = `../../test/ts/050interaction.ts`

func TestFileLocater(t *testing.T) {
	l := FileLocater(testscript)

	_, factory, err := l.Locate()
	if err != nil {
		t.Fatal(err)
	}

	rc, err := factory()
	if err != nil {
		t.Fatal(err)
	}
	defer rc.Close()

	all, err := ioutil.ReadAll(rc)
	if err != nil {
		t.Fatal(err)
	}

	want := 250
	if len(all) < want {
		t.Errorf("wanted at least %d bytes, got only %d", want, len(all))
	}

	prefix := `/// <reference path="010model.ts" />`
	suffix := `}`
	alltext := strings.TrimSpace(string(all))

	if !strings.HasPrefix(alltext, prefix) {
		t.Errorf("want start %s, got %s", prefix, alltext[:len(prefix)])
	}
	if !strings.HasSuffix(alltext, suffix) {
		t.Errorf("want tail %s, got %s", suffix, alltext[len(alltext)-len(suffix):])
	}

}
