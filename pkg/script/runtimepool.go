package script

import (
	"sync"

	"github.com/dop251/goja"
)

// yes, we know sync.Pool
// But as initialization of a runtime is quite expensive
// due to script loading, we need to preserve every
// runtime instance. sync.Pool does not guarantee this,

/* see this BM with sync.Pool that created 25 runtimes on 4 processing threads

--------------- old one with sync.Pool:
BenchmarkScriptBundleSingleThread-4        10000            132331 ns/op
BenchmarkScriptBundleAllThread-4           10000            335371 ns/op
--- BENCH: BenchmarkScriptBundleAllThread-4
    script_test.go:112: vm pool size: GET=1, NEW=1 at N=1
    script_test.go:112: vm pool size: GET=1, NEW=4 at N=100
    script_test.go:112: vm pool size: GET=77, NEW=7396 at N=10000
BenchmarkScriptBundleAllCPU-4              20000             93619 ns/op
--- BENCH: BenchmarkScriptBundleAllCPU-4
    script_test.go:154: vm pool size: GET=1, NEW=1 at N=1
    script_test.go:154: vm pool size: GET=1, NEW=4 at N=100
    script_test.go:154: vm pool size: GET=1, NEW=9 at N=10000
    script_test.go:154: vm pool size: GET=2, NEW=25 at N=20000

--------------- New one with runtimepool:
BenchmarkScriptBundleSingleThread-4        10000            132453 ns/op
BenchmarkScriptBundleAllThread-4           10000            406360 ns/op
--- BENCH: BenchmarkScriptBundleAllThread-4
    script_test.go:112: vm pool size: GET=1, NEW=1 at N=1
    script_test.go:112: vm pool size: GET=4, NEW=4 at N=100
    script_test.go:112: vm pool size: GET=7384, NEW=7384 at N=10000
BenchmarkScriptBundleAllCPU-4              20000             94234 ns/op
--- BENCH: BenchmarkScriptBundleAllCPU-4
    script_test.go:154: vm pool size: GET=1, NEW=1 at N=1
    script_test.go:154: vm pool size: GET=4, NEW=4 at N=100
    script_test.go:154: vm pool size: GET=4, NEW=4 at N=10000
    script_test.go:154: vm pool size: GET=4, NEW=4 at N=20000
*/

type runtimepool struct {
	mu    *sync.Mutex
	slice []*goja.Runtime
}

func newRuntimePool() *runtimepool {
	return &runtimepool{mu: new(sync.Mutex)}
}

func (rtp *runtimepool) Get() *goja.Runtime {
	rtp.mu.Lock()
	defer rtp.mu.Unlock()

	l := len(rtp.slice)
	if l == 0 {
		return nil
	}
	h, t := rtp.slice[l-1], rtp.slice[:l-1]
	rtp.slice = t
	return h
}

func (rtp *runtimepool) Put(rt *goja.Runtime) {
	rtp.mu.Lock()
	rtp.slice = append(rtp.slice, rt)
	rtp.mu.Unlock()
}

func (rtp *runtimepool) Count() int {
	rtp.mu.Lock()
	defer rtp.mu.Unlock()

	return len(rtp.slice)
}
