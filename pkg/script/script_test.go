package script

import (
	"runtime"
	"sync"
	"testing"
	"time"
)

const (
	testScriptCode = `var ws = {data: Model(), ruleset: "Calculate", action: ""}; var jws = JSON.stringify(ws); $rule(jws);`
	testResultJSON = `{"data":{"customer":{"name":"Simple Solutions PLC","street":"Homestreet 40a","contact":"Ms. Jane Simple"},"contract":{"nr":"C1001","type":"Mortgage"},"asset":{"description":"Drilling machine","price":3601.78,"currency":"EUR"},"terms":{"duration":48,"installment":75.03708333333334,"start":"2019-12-03T00:04:37.288Z"}},"ruleset":"Calculate","action":"","envelope":{"text1":"Simple Solutions PLC","text2":"Drilling machine","number1":3601.78,"number2":75.03708333333334},"index":{"customername":"Simple Solutions PLC","asset":"Drilling machine","assetprice":3601.78,"installment":75.03708333333334}}`
)

type tb interface {
	Fatal(...interface{})
}

func preparePool(t tb) (*Pool, *VM) {
	l, err := GlobLocater(testscripts)
	if err != nil {
		t.Fatal(err)
	}

	p := NewSingle(l)

	vm, err := p.Get("")
	if err != nil {
		t.Fatal(err)
	}
	return p, vm
}

func TestScriptBundle(t *testing.T) {
	p, vm := preparePool(t)
	defer p.Put(vm)

	tt := time.Now()
	json, err := vm.Runtime.RunString(testScriptCode)
	if err != nil {
		t.Log(vm.Timestamp)
		t.Fatal(err)
	}

	v := json.Export()
	want := testResultJSON
	got, ok := v.(string)
	diff := time.Now().Sub(tt)
	t.Logf("script duration: %v", diff)
	if !ok {
		t.Fatalf("expected string, got %T", v)
	}
	if want != got {
		t.Errorf("want JSON %s, got '%s'", want, got)
	}

	if p.TranspileCount != 1 {
		t.Errorf("want TranspileCount 1, got %d", p.TranspileCount)
	}
}

var v string

func BenchmarkScriptBundleSingleThread(b *testing.B) {
	p, vm := preparePool(b)
	defer p.Put(vm)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		json, _ := vm.Runtime.RunString(testScriptCode)
		v = json.Export().(string)
		if v != testResultJSON {
			b.Errorf("want JSON %s, got '%s'", testResultJSON, v)
		}
	}
	if p.TranspileCount != 1 {
		b.Errorf("want TranspileCount 1, got %d", p.TranspileCount)
	}
}

func BenchmarkScriptBundleAllThread(b *testing.B) {
	p, vm0 := preparePool(b)
	p.Put(vm0)

	b.ResetTimer()
	wg := new(sync.WaitGroup)
	for i := 0; i < b.N; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			vm, err := p.Get("")
			if err != nil {
				b.Errorf("get vm failed: %v", err)
				return
			}
			defer p.Put(vm)

			json, _ := vm.Runtime.RunString(testScriptCode)
			v = json.Export().(string)
			if v != testResultJSON {
				b.Errorf("want JSON %s, got '%s'", testResultJSON, v)
			}
		}()
	}
	wg.Wait()

	if p.TranspileCount != 1 {
		b.Errorf("want TranspileCount 1, got %d", p.TranspileCount)
	}
	cnt := p.CountPool("")
	b.Logf("vm pool size: GET=%d, NEW=%d at N=%d", cnt, p.Count(""), b.N)
}

func BenchmarkScriptBundleAllCPU(b *testing.B) {
	p, vm0 := preparePool(b)
	p.Put(vm0)

	c1 := make(chan bool)
	wg := new(sync.WaitGroup)
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			for range c1 {
				vm, err := p.Get("")
				if err != nil {
					b.Errorf("get vm failed: %v", err)
					return
				}

				json, _ := vm.Runtime.RunString(testScriptCode)
				v = json.Export().(string)
				p.Put(vm)

				if v != testResultJSON {
					b.Errorf("want JSON %s, got '%s'", testResultJSON, v)
				}
			}
			wg.Done()
		}()
	}

	// warm up
	for i := 0; i < 2*runtime.NumCPU(); i++ {
		c1 <- true
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		c1 <- true
	}
	close(c1)
	wg.Wait()

	if p.TranspileCount != 1 {
		b.Errorf("want TranspileCount 1, got %d", p.TranspileCount)
	}
	cnt := p.CountPool("")
	b.Logf("vm pool size: GET=%d, NEW=%d at N=%d", cnt, p.Count(""), b.N)
	if cnt > runtime.NumCPU() {
		b.Fatalf("want max vm count %d, got %d", runtime.NumCPU(), cnt)
	}
}
