package script

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

type filelocater string

// FileLocater locates a single file
func FileLocater(file string) Locater {
	p := string(file)
	p = filepath.ToSlash(p)
	p = filepath.Clean(p)
	return filelocater(p)
}

func (fl filelocater) Locate() (timestamp time.Time, sf Factory, err error) {
	p := string(fl)
	stat, err := os.Stat(p)
	if err != nil {
		return time.Time{}, nil, err
	}
	if stat.IsDir() {
		return time.Time{}, nil, fmt.Errorf("not a file: %s", p)
	}
	return stat.ModTime(), func() (io.ReadCloser, error) {
		return os.Open(p)
	}, nil
}
