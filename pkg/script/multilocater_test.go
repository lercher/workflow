package script

import (
	"io/ioutil"
	"testing"
	"time"
)

func TestMultiLocater(t *testing.T) {
	l1, l2 := typescriptStatic("loc1"), typescriptVolatile("loc2")
	ml := MultiLocater(l1, l2)

	ts, sf, err := ml.Locate()
	if err != nil {
		t.Fatal(err)
	}
	if ts.After(time.Now()) {
		t.Errorf("timstamp want not after now, got %s", ts)
	}
	if ts.Before(time.Now().Add(-1 * time.Second)) {
		t.Errorf("timstamp want not before now-1s, got %s", ts)
	}

	rd, err := sf()
	if err != nil {
		t.Fatal(err)
	}

	b, err := ioutil.ReadAll(rd)
	if err != nil {
		t.Fatal(err)
	}
	got := string(b)
	want := `loc1loc2`
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}

	err = rd.Close()
	if err != nil {
		t.Fatal(err)
	}
}
