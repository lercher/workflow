package script

import (
	"fmt"
	"io"
	"time"
)

type multilocater struct {
	ls []Locater
}

func (ml multilocater) Locate() (timestamp time.Time, sf Factory, err error) {
	var maxts time.Time

	tss := make([]time.Time, 0, len(ml.ls))
	sfs := make([]Factory, 0, len(ml.ls))
	for i, l := range ml.ls {
		ts, sf, err := l.Locate()
		if err != nil {
			return time.Time{}, nil, fmt.Errorf("locater #%d: %v", i, err)
		}
		tss = append(tss, ts)
		sfs = append(sfs, sf)
		if i == 0 || ts.After(maxts) {
			maxts = ts
		}
	}

	multifactory := func() (io.ReadCloser, error) {
		rcs := make([]io.ReadCloser, 0, len(ml.ls))
		for _, sf := range sfs {
			rc, err := sf()
			if err != nil {
				// close all previously opened readers, ignoring close errors
				for _, c := range rcs {
					_ = c.Close()
				}
				return nil, err
			}
			rcs = append(rcs, rc)
		}
		return MultiReadCloser(rcs...), nil
	}

	return maxts, multifactory, nil
}

// MultiLocater combines a list of Locaters to a single Locater
// using the newest timestamp of all Locaters and creating a MultiReader if asked for.
func MultiLocater(ls ...Locater) Locater {
	ml := multilocater{ls: make([]Locater, len(ls))}
	copy(ml.ls, ls)
	return ml
}
