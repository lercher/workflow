package script

import (
	"strings"
	"testing"
)

func TestTranspileString(t *testing.T) {
	tests := []string{
		`1+1;`, `1 + 1;`,
		`let x = 1+1;`, `var x = 1 + 1;`,
		`const f = (x:integer,y:integer) => x*y;`, `var f = function (x, y) { return x * y; };`,
		`const x = 1; self['x'] === 1;`, `var x = 1; self['x'] === 1;`,
	}
	for i := 0; i < len(tests); i = i + 2 {
		script, want := tests[i], tests[i+1]
		got := TranspileString("", script)
		got = strings.TrimSpace(got)
		got = strings.ReplaceAll(got, "\n", " ")
		got = strings.ReplaceAll(got, "\r", "")
		if got != want {
			t.Errorf("want transpile('%s') = '%s', got '%s'", script, want, got)
		}
	}

	const wantTypescriptVersion = `3.7.2`
	if TypescriptVersion != wantTypescriptVersion {
		t.Errorf("want TypescriptVersion %s, got %s", wantTypescriptVersion, TypescriptVersion)
	}
}
