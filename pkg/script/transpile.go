package script

import (
	"fmt"
	"io"
	"io/ioutil"
	"sync"

	"github.com/dop251/goja"
)

type transpiler struct {
	mu        *sync.Mutex
	vm        *goja.Runtime
	transpile func(filename string, src string) string
}

var (
	once = sync.Once{}
	tp   transpiler

	// TypescriptServicesFile can be customized before first use to point to a different ES5 js file
	TypescriptServicesFile = `typescriptServices.js`

	// TypescriptVersion is filled after first use by the version of the Typescript transpiler
	TypescriptVersion = `(unknown, call Transpile() first)`
)

// Transpile reads the whole Reader rd and then calls TranspileString
func Transpile(scriptname string, rd io.Reader) (string, error) {
	script, err := ioutil.ReadAll(rd)
	if err != nil {
		return "", fmt.Errorf("%v: %v", scriptname, err)
	}
	return TranspileString(scriptname, string(script)), nil
}

// TranspileString compiles Typescript to ES5 suitable to be executed via goja.
// It panics, if it can't read the file TypescriptServicesFile on first use
// which defaults to `typescriptServices.js` or compilation of it results in
// an other error. As a side effect it places the Typescript version into
// TypescriptVersion.
func TranspileString(scriptname, script string) string {
	once.Do(mustLoadTypescriptOnce)

	// a goja vm is not thread safe, therefor we lock it
	tp.mu.Lock()
	defer tp.mu.Unlock()

	return tp.transpile(scriptname, script)
}

func mustLoadTypescriptOnce() {
	tp = transpiler{
		mu: new(sync.Mutex),
		vm: goja.New(),
	}

	ts, err := ioutil.ReadFile(TypescriptServicesFile)
	if err != nil {
		panic(err)
	}

	_, err = tp.vm.RunScript(TypescriptServicesFile, string(ts))
	if err != nil {
		panic(err)
	}

	tsver, err := tp.vm.RunScript("", "ts.version;")
	if err != nil {
		panic(err)
	}
	TypescriptVersion = tsver.Export().(string)

	// https://www.typescriptlang.org/docs/handbook/compiler-options.html at '--lib' for ts.ScriptTarget.ES5,
	// goja supports ES5.1 so we choose ES5 as target
	// export function transpile(input: string, compilerOptions?: CompilerOptions, fileName?: string, diagnostics?: Diagnostic[], moduleName?: string): string;
	_, err = tp.vm.RunScript("transpileFunc", `function transpile(filename, src) { return ts.transpile(src, { target: ts.ScriptTarget.ES5, inlineSourceMap: false }, filename) };`)
	if err != nil {
		panic(err)
	}

	tp.vm.ExportTo(tp.vm.Get("transpile"), &tp.transpile)
}
