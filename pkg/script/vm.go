package script

import (
	"time"

	"github.com/dop251/goja"
)

// VM is what you Get from and Put back to a Pool.
// Its Runtime can be used to execute ES5 on it.
type VM struct {
	Key       Key
	Timestamp time.Time
	Runtime   *goja.Runtime
}
