// Package script provides a thread safe Typescript based
// execution environment based on the JS interpreter goja
package script

import (
	"io"
	"io/ioutil"
	"strings"
	"testing"
	"time"
)

type addfunc func(int, int) int

type typescriptStatic string

func (ts typescriptStatic) Locate() (timestamp time.Time, sf Factory, err error) {
	return time.Time{},
		func() (io.ReadCloser, error) {
			return ioutil.NopCloser(strings.NewReader(string(ts))), nil
		},
		nil
}

type typescriptVolatile string

func (ts typescriptVolatile) Locate() (timestamp time.Time, sf Factory, err error) {
	return time.Now(),
		func() (io.ReadCloser, error) {
			return ioutil.NopCloser(strings.NewReader(string(ts))), nil
		},
		nil
}

func TestPoolGetStatic(t *testing.T) {
	const add = typescriptStatic("const add = (a,b) => a+b;")
	p := NewSingle(add)
	exercise(t, p)

	const tc = 1
	if p.TranspileCount != tc {
		t.Errorf("want TranspileCount %d, got %d", tc, p.TranspileCount)
	}
}

func TestPoolGetVolatile(t *testing.T) {
	const add = typescriptVolatile("const add = (a,b) => a+b;")
	p := NewSingle(add)
	exercise(t, p)

	const tc = 2
	if p.TranspileCount != tc {
		t.Errorf("want TranspileCount %d, got %d", tc, p.TranspileCount)
	}
}

func exercise(t *testing.T, p *Pool) {
	const tskey = Key("")

	// new alloc
	vm, err := p.Get(tskey)
	if err != nil {
		t.Error(err)
	}
	if vm == nil {
		t.Errorf("expected non nil vm")
	}

	v, err := vm.Runtime.RunString("add(4, 5);")
	if err != nil {
		t.Error(err)
		return
	}
	if i, ok := v.Export().(int64); ok {
		if i != 9 {
			t.Errorf("expected add(4,5)==9, got %d", i)
		}
	} else {
		t.Errorf("expected int64, got %T", v.Export())
	}
	p.Put(vm)

	// reuse vm
	vm, err = p.Get(tskey)
	if err != nil {
		t.Error(err)
	}
	if vm == nil {
		t.Errorf("expected non nil vm")
	}

	// we don't have a special name "self" for the global context, we need to use "this"
	v, err = vm.Runtime.RunString("this['add'](44, 55);")
	if err != nil {
		t.Error(err)
		return
	}
	if i, ok := v.Export().(int64); ok {
		if i != 99 {
			t.Errorf("expected add(44,55)==99, got %d", i)
		}
	} else {
		t.Errorf("expected int64, got %T", v.Export())
	}
	p.Put(vm)

	v, err = vm.Runtime.RunString("(function() { return this['add'](18, -13); })();")
	if err != nil {
		t.Error(err)
	}
	if i, ok := v.Export().(int64); ok {
		if i != 5 {
			t.Errorf("expected add(18,-13)==5, got %d", i)
		}
	} else {
		t.Errorf("expected int64, got %T", v.Export())
	}
	p.Put(vm)
}
