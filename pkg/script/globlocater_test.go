package script

import (
	"io/ioutil"
	"strings"
	"testing"
)

const testscripts = `../../test/ts/*.ts`

func TestGlobLocater(t *testing.T) {
	l, err := GlobLocater(testscripts)
	if err != nil {
		t.Fatal(err)
	}

	_, factory, err := l.Locate()
	if err != nil {
		t.Fatal(err)
	}

	rc, err := factory()
	if err != nil {
		t.Fatal(err)
	}
	defer rc.Close()

	all, err := ioutil.ReadAll(rc)
	if err != nil {
		t.Fatal(err)
	}

	want := 2500
	if len(all) < want {
		t.Errorf("wanted at least %d bytes, got only %d", want, len(all))
	}

	prefix := `// file 010model.ts is used to specify`
	suffix := `// this file test\ts\999last.ts must be last for tests to succeed`
	alltext := strings.TrimSpace(string(all))

	if !strings.HasPrefix(alltext, prefix) {
		t.Errorf("want start %s, got %s", prefix, alltext[:len(prefix)])
	}
	if !strings.HasSuffix(alltext, suffix) {
		t.Errorf("want tail %s, got %s", suffix, alltext[len(alltext)-len(suffix):])
	}
}
