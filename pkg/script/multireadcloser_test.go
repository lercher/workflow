package script

import (
	"io/ioutil"
	"strings"
	"testing"
)

func TestMultiReadCloser(t *testing.T) {
	rd1, rd2 := ioutil.NopCloser(strings.NewReader("rd1")), ioutil.NopCloser(strings.NewReader("rd2"))
	mrc := MultiReadCloser(rd1, rd2)

	b, err := ioutil.ReadAll(mrc)
	if err != nil {
		t.Fatal(err)
	}
	got := string(b)
	want := `rd1rd2`
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}

	err = mrc.Close()
	if err != nil {
		t.Fatal(err)
	}
}
