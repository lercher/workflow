// Package atom defines a keyspace for storing workflows
// in a KV-store like badger.
package atom

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

// Atom represents a particular string by a non negative number
// relative to a Registry. Atoms are inserted into Keys
// in a compact way as defined by encoding/binary
type Atom int

// Constants for well-known atoms of our standard ontology
// Every other atom should be of the form "namespace:name"
const (
	AtomAtom     = Atom(0) // Atom for Atom, it must be 0 so no iota
	AtomList     = Atom(1) // a list of atoms
	AtomSchema   = Atom(2) // Schemas describe how keys and values are shaped
	AtomSequence = Atom(3) // A sequence is used to store an atomic counter
	AtomGlobal   = Atom(4) // a global property
	AtomProperty = Atom(5) // a property of a blob
	AtomName     = Atom(6) // a name of sth, encoded as length plus this many bytes
	AtomBlob     = Atom(7) // a blob, i.e. the rest of the key or value
	AtomIndex    = Atom(8) // an index entry
	AtomValue    = Atom(9) // like a property but for a name with namespace
)

// StdOntology adds our standard ontology of atoms to the Registry r and the Schema s.
// It panics, if the Registry or the Schema is not empty.
// If Newkey is not nil, it is called for all the atoms and for
// initial schematic data to be stored in a KV store.
func StdOntology(r *Registry, s *Schema) {
	if r.Count() != 0 {
		panic("can't init an ontology in a non empty registry")
	}
	if s.Count() != 0 {
		panic("can't init an ontology in a non empty schema map")
	}

	r.set(AtomAtom, "atom")
	r.set(AtomSchema, "schema")
	r.set(AtomSequence, "sequence")
	r.set(AtomGlobal, "global")
	r.set(AtomProperty, "property")
	r.set(AtomName, "name")
	r.set(AtomBlob, "blob")
	r.set(AtomIndex, "index")
	r.set(AtomList, "list")
	r.set(AtomValue, "value")

	s.Add(AtomAtom, []Atom{AtomAtom}, []Atom{AtomBlob})                      // atom:atom -> blob
	s.Add(AtomSchema, []Atom{AtomAtom}, []Atom{AtomList, AtomList})          // schema:atom -> atom...:atom...
	s.Add(AtomSequence, []Atom{AtomAtom}, []Atom{AtomBlob})                  // sequence:atom -> blob
	s.Add(AtomGlobal, []Atom{AtomAtom}, []Atom{AtomBlob})                    // global:atom -> blob
	s.Add(AtomProperty, []Atom{AtomAtom, AtomBlob}, []Atom{AtomBlob})        // property:atom:blob -> blob
	s.Add(AtomIndex, []Atom{AtomAtom, AtomName, AtomBlob}, []Atom{})         // index:atom:name:blob -> nil. Useful, b/c we can list all index:atom:name:* keys
	s.Add(AtomValue, []Atom{AtomAtom, AtomAtom, AtomName}, []Atom{AtomBlob}) // value:atom:atom:name -> blob, 1st atom is the property and the second atom is the namespace for the name (e.g. a user-domain:user-name)
}

// Builder creates a new Builder and adds the Atom as start
func (a Atom) Builder() *Builder {
	buf := new(bytes.Buffer)
	b := (*Builder)(buf)
	return b.Atom(a)
}

// BuilderWith resets the buffer buf, wraps it in a Builder and adds the Atom as start
func (a Atom) BuilderWith(buf *bytes.Buffer) *Builder {
	buf.Reset()
	b := (*Builder)(buf)
	return b.Atom(a)
}

func writeUvarint(w io.ByteWriter, x int) error {
	// adapted from /usr/lib64/go/1.12/src/encoding/binary/varint.go - PutUvarint
	for x >= 0x80 {
		b := byte(x) | 0x80
		err := w.WriteByte(b)
		if err != nil {
			return err
		}
		x >>= 7
	}
	return w.WriteByte(byte(x))
}

func readUvarint(buf []byte) (Atom, []byte, error) {
	atom, l := binary.Uvarint(buf)
	if l <= 0 {
		return 0, nil, fmt.Errorf("not an uvarint encoded atom (%d)", l)
	}
	return Atom(atom), buf[l:], nil
}
