package atom

import (
	"fmt"
	"testing"
)

func TestStdOntology(t *testing.T) {
	var ks, vs [][]byte
	newkey := func(k, v []byte) {
		ks = append(ks, k)
		vs = append(vs, v)
	}

	r, s := NewRegistry(), NewSchema()
	r.Newkey, s.NewSchema = newkey, newkey
	StdOntology(r, s)

	// index:atom:name:blob -> nil
	phone := r.Atom("tns:phone") // 9/phone
	newkey(AtomIndex.Builder().Atom(phone).String(`+49 89 7482400`).Blob([]byte{99, 88, 77, 66}), nil)
	// [8/index  9/phone  14/len  43 52 57 32 56 57 32 55 52 56 50 52 48 48/+49...400  99 88 77 66/blob] -> []/nil

	want := `[[0 0] [0 2] [0 3] [0 4] [0 5] [0 6] [0 7] [0 8] [0 1] [0 9] [2 0] [2 2] [2 3] [2 4] [2 5] [2 8] [2 9] [0 10] [8 10 14 43 52 57 32 56 57 32 55 52 56 50 52 48 48 99 88 77 66]] [[1 0 1 7] [1 0 2 1 1] [1 0 1 7] [1 0 1 7] [2 0 7 1 7] [3 0 6 7 0] [3 0 0 6 1 7] [116 110 115 58 112 104 111 110 101] []]`
	got := fmt.Sprint(ks, vs[10:]) // 10 std atoms ignored
	if got != want {
		t.Errorf("got\n%v,\nwant\n%v", got, want)
	}

	// now try to decode every entry
	if len(ks) != len(vs) {
		t.Fatalf("want lengths to match, got %d != %d", len(ks), len(vs))
	}

	for i := 0; i < len(ks); i++ {
		sch, dk, dv, err := s.Decode(ks[i], vs[i])
		if err != nil {
			t.Error(i, err)
		}
		if sch == AtomIndex {
			want := `index tns:phone=+49 89 7482400 of [99 88 77 66]`
			got := fmt.Sprint(r.String(sch), " ", r.String(dk.First()), "=", string(dk.FirstName()), " of ", dk.Blob)
			if got != want {
				t.Errorf("want %s, got %s", want, got)
			}
		}
		if sch == AtomSchema && dk.First() == AtomSchema {
			// schema of schema
			want := `schema of schema = [0] [1 1]` // i.e. schema:atom -> atom...:atom..., meaning [0/atom] [1/list 1/list]
			got := fmt.Sprint(r.String(sch), " of ", r.String(dk.First()), " = ", dv.FirstList(), dv.SecondList())
			if got != want {
				t.Errorf("want %s, got %s", want, got)
			}
		}
	}
}
