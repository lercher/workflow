package atom

import (
	"bytes"
	"errors"
	"fmt"
	"sync"
)

// Schema describes, how a key and its value is shaped acording to its leading Atom.
// The description is a slice of atoms specifying the data in the rest of the key
type Schema struct {
	mu        *sync.Mutex
	key       map[Atom][]Atom
	val       map[Atom][]Atom
	NewSchema func(k, v []byte)
}

// NewSchema creates a new Schema
func NewSchema() *Schema {
	return &Schema{
		mu:        new(sync.Mutex),
		key:       make(map[Atom][]Atom),
		val:       make(map[Atom][]Atom),
		NewSchema: nil,
	}
}

// Count returns the count of different Schemata
func (s *Schema) Count() int {
	s.mu.Lock()
	defer s.mu.Unlock()

	return len(s.key)
}

// Add adds a KV schema for Atom a to the Schema map.
// Add panics, if the Schema was present already.
// If NewSchema is not nil, it is called for storing the new schema in the KV store
func (s *Schema) Add(start Atom, rest, value []Atom) {
	s.mu.Lock()
	defer s.mu.Unlock()

	if _, ok := s.key[start]; ok {
		panic("can't add a schema for an already known one")
	}
	s.key[start] = rest
	s.val[start] = value

	if s.NewSchema != nil {
		k := AtomSchema.Builder().Atom(start).Key()

		buf := new(bytes.Buffer)
		b := (*Builder)(buf)
		v := b.List(rest).List(value).Key()

		s.NewSchema(k, v)
	}
}

// KeyValOf returns the key and value schemata of start and a flag if it's known
func (s *Schema) KeyValOf(start Atom) ([]Atom, []Atom, bool) {
	rest, ok := s.key[start]
	if !ok {
		return nil, nil, false
	}
	val, ok := s.val[start]
	return rest, val, ok
}

// Decode decodes a KV pair by their schema information. It is allowed for v to be nil. In this case the second Decoded is nil.
func (s *Schema) Decode(k, v []byte) (Atom, *Decoded, *Decoded, error) {
	if len(k) == 0 {
		return AtomAtom, nil, nil, errors.New("no schema information in the zero sized key")
	}
	start, rest, err := readUvarint(k)
	if err != nil {
		return start, nil, nil, fmt.Errorf("no proper schema information in the key: %v", err)
	}
	ks, vs, ok := s.KeyValOf(start)
	if !ok {
		return start, nil, nil, fmt.Errorf("no schema information for atom(%v)", start)
	}
	dk, err := decode(rest, ks)
	if err != nil {
		return start, nil, nil, fmt.Errorf("can't decode key: %v", err)
	}
	if v == nil {
		return start, dk, nil, nil
	}
	dv, err := decode(v, vs)
	if err != nil {
		return start, dk, nil, fmt.Errorf("can't decode value: %v", err)
	}
	return start, dk, dv, nil
}
