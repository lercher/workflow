package atom

import (
	"bytes"
	"fmt"
)

// Builder is used to construct a Key starting from an Atom
// which needs to be always first in a key
type Builder bytes.Buffer

// Atom adds an Atom to a Builder
func (b *Builder) Atom(a Atom) *Builder {
	buf := (*bytes.Buffer)(b)
	err := writeUvarint(buf, int(a))
	if err != nil {
		panic(fmt.Sprint("writing atom(", a, "):", err))
	}
	return b
}

// List adds the len and then all the atoms in the slice
func (b *Builder) List(slice []Atom) *Builder {
	buf := (*bytes.Buffer)(b)
	err := writeUvarint(buf, len(slice))
	if err != nil {
		panic(fmt.Sprint("writing atom slice len: ", err))
	}
	for _, a := range slice {
		b = b.Atom(a)
	}
	return b
}

// String adds a string as a slice to a Builder
func (b *Builder) String(s string) *Builder {
	return b.Slice([]byte(s))
}

// Slice adds the len and the the slice itself to a Builder
func (b *Builder) Slice(slice []byte) *Builder {
	buf := (*bytes.Buffer)(b)
	err := writeUvarint(buf, len(slice))
	if err != nil {
		panic(fmt.Sprint("writing slice len: ", err))
	}
	_, err = buf.Write(slice)
	if err != nil {
		panic(fmt.Sprint("writing slice: ", err))
	}
	return b
}

// Blob adds just the the slice to a Builder and returns the bytes, as it must be last
func (b *Builder) Blob(blob []byte) []byte {
	buf := (*bytes.Buffer)(b)
	_, err := buf.Write(blob)
	if err != nil {
		panic(fmt.Sprint("writing blob: ", err))
	}
	return buf.Bytes()
}

// Key returns the bytes of the Key constructed
func (b *Builder) Key() []byte {
	buf := (*bytes.Buffer)(b)
	return buf.Bytes()
}
