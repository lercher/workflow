package atom

import (
	"fmt"
)

// Decoded lists items decoded via a schema item from a byte slice
type Decoded struct {
	Atoms []Atom
	Lists [][]Atom
	Names [][]byte
	Blob  []byte
}

// First returns the first Atom or AtomAtom if the list is empty
func (d *Decoded) First() Atom {
	return d.Nth(0)
}

// Nth returns the n-th Atom starting at zero. It returns AtomAtom if its not in the list.
func (d *Decoded) Nth(n int) Atom {
	if 0 <= n && n < len(d.Atoms) {
		return d.Atoms[n]
	}
	return AtomAtom
}

// FirstList returns the first AtomList or nil if the list is empty
func (d *Decoded) FirstList() []Atom {
	return d.NthList(0)
}

// SecondList returns the 2nd AtomList or nil if the list is empty
func (d *Decoded) SecondList() []Atom {
	return d.NthList(1)
}

// NthList returns the n-th AtomList starting at zero. It returns nil if its not in the list.
func (d *Decoded) NthList(n int) []Atom {
	if 0 <= n && n < len(d.Lists) {
		return d.Lists[n]
	}
	return nil
}

// FirstName returns the first Name or nil if the list is empty
func (d *Decoded) FirstName() []byte {
	return d.NthName(0)
}

// NthName returns the n-th Name starting at zero. It returns nil if its not in the list.
func (d *Decoded) NthName(n int) []byte {
	if 0 <= n && n < len(d.Names) {
		return d.Names[n]
	}
	return nil
}

func decode(slice []byte, sch []Atom) (*Decoded, error) {
	d := &Decoded{}
	bs := slice
	for idx, s := range sch {
		bs1, err := decode1(bs, s, d)
		if err != nil {
			return nil, fmt.Errorf("can't decode schema atom(%d) #%d at %v: %v", s, idx+1, bs, err)
		}
		bs = bs1
	}
	if len(bs) > 0 {
		return nil, fmt.Errorf("unexpected bytes %v", bs)
	}
	return d, nil
}

func decode1(bs []byte, s Atom, d *Decoded) ([]byte, error) {
	switch s {
	case AtomBlob:
		if d.Blob != nil {
			return nil, fmt.Errorf("blob must be last and unique, would overwrite %v", d.Blob)
		}
		d.Blob = bs
		return nil, nil
	}

	// needing an atom or a length first
	a, rest, err := readUvarint(bs)
	if err != nil {
		return nil, err
	}

	switch s {
	case AtomAtom:
		d.Atoms = append(d.Atoms, a)
		return rest, nil
	case AtomList:
		l := int(a)
		list := make([]Atom, l)
		for i := 0; i < l; i++ {
			a, rest, err = readUvarint(rest)
			if err != nil {
				return nil, fmt.Errorf("#%d: %v", i+1, err)
			}
			list[i] = a
		}
		d.Lists = append(d.Lists, list)
		return rest, nil
	case AtomName:
		l := int(a)
		if len(rest) < l {
			return nil, fmt.Errorf("wanted to read %d bytes, got only %d", l, len(rest))
		}
		d.Names = append(d.Names, rest[:l])
		return rest[l:], nil
	default:
		return nil, fmt.Errorf("unsupported schema atom")
	}
}
