package atom

import (
	"bytes"
	"fmt"
	"testing"
)

func TestAtomAtomSerialize(t *testing.T) {
	bs := AtomAtom.Builder().Key()
	want := `[0]`
	got := fmt.Sprint(bs)
	if got != want {
		t.Errorf("AtomAtom want %v, got %v", want, got)
	}
}

func TestSliceSerialize(t *testing.T) {
	bs := Atom(55).Builder().Slice([]byte{11, 22, 33}).Slice([]byte{}).Slice([]byte{77}).Key()
	want := `[55 3 11 22 33 0 1 77]` // atom(55) len(3) [11 22 33] len(0) [] len(1) [77]
	got := fmt.Sprint(bs)
	if got != want {
		t.Errorf("SliceKey want %v, got %v", want, got)
	}
}

func TestAtomChain(t *testing.T) {
	bs := AtomAtom.Builder().Atom(1).Atom(0).Atom(100).Atom(0).Atom(256).Atom(0).Atom(1027).Key()
	want := `[0 1 0 100 0 128 2 0 131 8]`
	got := fmt.Sprint(bs)
	if got != want {
		t.Errorf("AtomAtom/1/0/100/0/256/0/1027 want %v, got %v", want, got)
	}
}

func TestBuilderWith(t *testing.T) {
	buf := new(bytes.Buffer)
	buf.WriteByte(100) // not empty
	bs := AtomAtom.BuilderWith(buf).Atom(5).Atom(19).Key()
	want := `[0 5 19]`
	got := fmt.Sprint(bs)
	if got != want {
		t.Errorf("AtomAtom/5/19 want %v, got %v", want, got)
	}
}
