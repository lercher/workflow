package atom

import (
	"fmt"
	"io"
	"testing"
)

func TestRegister(t *testing.T) {
	r := NewRegistry()
	n := 0
	sa := "system.atom"

	r.Newkey = func(k, v []byte) {
		want := fmt.Sprint([]byte{0, 0}, sa)
		got := fmt.Sprint(k, string(v))
		if got != want {
			t.Errorf("Newkey want %s, got %s", want, got)
		}
		n++
	}

	got := r.Atom(sa)
	want := AtomAtom
	if got != want {
		t.Errorf("1st Atom should be %v, got %v", want, got)
	}

	got2 := r.Atom(sa)
	if got2 != want {
		t.Errorf("1st Atom should be %v (2nd pass), got %v", want, got2)
	}

	s := r.String(got)
	if s != sa {
		t.Errorf("AtomAtom want %s, got %s", sa, s)
	}

	if n != 1 {
		t.Errorf("want 1 new key, got %d", n)
	}

	ss := "system.sequence"
	r.Newkey = func(k, v []byte) {
		want := fmt.Sprint([]byte{0, 1}, ss)
		got := fmt.Sprint(k, string(v))
		if got != want {
			t.Errorf("Newkey want %s, got %s", want, got)
		}
		n++
	}

	_, _, _ = r.Atom(ss), r.Atom(ss), r.Atom(ss)

	s = r.String(got)
	if s != sa {
		t.Errorf("AtomAtom want %s, got %s", sa, s)
	}

	s = r.String(Atom(1))
	if s != ss {
		t.Errorf("atom(1) want %s, got %s", sa, s)
	}

	if r.Count() != 2 {
		t.Errorf("want Count 2, got %d", r.Count())
	}
	if n != 2 {
		t.Errorf("want 2 new keys, got %d", n)
	}
}

func TestStorage(t *testing.T) {
	var ks, vs [][]byte

	r := NewRegistry()
	r.Newkey = func(k, v []byte) {
		ks = append(ks, k)
		vs = append(vs, v)
	}
	r.Atom("system.atom:atom")
	r.Atom("system.sequence:id")
	r.Atom("system.global:name")
	r.Atom("system.property:name:id")
	r.Atom("workflow.data:id")
	r.Atom("workflow.envelope:id")
	r.Atom("workflow.property:name:value:id")

	if len(ks) != len(vs) || len(ks) != 7 {
		t.Errorf("want 7 ks and vs, got %d ks and %d vs", len(ks), len(vs))
	}

	n := -1
	r2, err := RegistryFrom(func() ([]byte, []byte, error) {
		n++
		if n >= len(ks) {
			return nil, nil, io.EOF
		}
		return ks[n], vs[n], nil
	})
	if err != nil {
		t.Fatal(err)
	}
	if r2.Count() != len(ks) {
		t.Errorf("r2 want %d items, got %d", len(ks), r2.Count())
	}
	for i := 0; i < len(ks); i++ {
		a := Atom(i)
		s1, s2 := r.String(a), r2.String(a)
		if s1 != s2 {
			t.Errorf("atom(%d) want %q, got %q", a, s1, s2)
		}
	}
}
