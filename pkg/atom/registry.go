package atom

import (
	"fmt"
	"io"
	"sync"
)

// Registry is used to convert strings to associated
// integers (Atom) and vice versa.
type Registry struct {
	mu          *sync.Mutex
	atom2string []string
	string2atom map[string]Atom
	// Newkey is a callback to store a new Atom in Atom() in a KV store
	// that can be used to initialize a Registry with RegistryFrom(). It defaults to nil.
	// The function is called while the Registry is locked.
	Newkey func(k, v []byte)
}

// NewRegistry creates a new Registry for Atoms
func NewRegistry() *Registry {
	return &Registry{
		mu:          new(sync.Mutex),
		string2atom: make(map[string]Atom),
		Newkey:      nil,
	}
}

// RegistryFrom creates a new Registry from storage.
// next() is called until io.EOF is returned, any other error is propagated.
// next needs to enumerate key-value pairs, with value being the string of the atom
// and key being the concatenation of atom(0) and the atom associated with the value
func RegistryFrom(next func() ([]byte, []byte, error)) (*Registry, error) {
	r := NewRegistry()
	r.mu.Lock()
	defer r.mu.Unlock()

	for {
		k, v, err := next()
		if err == io.EOF {
			return r, r.rebuildArray()
		}
		if err != nil {
			return nil, err
		}

		buf := k
		aa, buf, err := readUvarint(buf)
		if err != nil {
			return nil, fmt.Errorf("can't decode 1st atom of key: %v", err)
		}
		if aa != AtomAtom {
			return nil, fmt.Errorf("the key for a stored atom table must start with atom(%v) followed by the atom, but it starts with %v", AtomAtom, aa)
		}
		a, buf, err := readUvarint(buf)
		if err != nil {
			return nil, fmt.Errorf("can't decode 2nd atom of key: %v", err)
		}
		r.string2atom[string(v)] = a
	}
}

func (r *Registry) rebuildArray() error {
	l := len(r.string2atom)
	r.atom2string = make([]string, l)
	for s, a := range r.string2atom {
		index := int(a)
		if index < 0 || l <= index {
			return fmt.Errorf("atom(%d) out of range 0..%d", a, l-1)
		}
		r.atom2string[index] = s
	}
	return nil
}

// Count is the length of the registry table
func (r *Registry) Count() int {
	r.mu.Lock()
	defer r.mu.Unlock()

	return len(r.atom2string)
}

// Atom gets the Atom for a particular string
func (r *Registry) Atom(s string) Atom {
	r.mu.Lock()
	defer r.mu.Unlock()

	if a, ok := r.string2atom[s]; ok {
		return a
	}

	// New Atom
	a := Atom(len(r.atom2string))
	r.set(a, s)
	return a
}

func (r *Registry) set(a Atom, s string) {
	idx := int(a)
	for idx >= len(r.atom2string) {
		r.atom2string = append(r.atom2string, "")
	}
	r.atom2string[idx] = s
	r.string2atom[s] = a
	if r.Newkey != nil {
		k := AtomAtom.Builder().Atom(a).Key()
		v := []byte(s)
		r.Newkey(k, v)
	}
}

// String gets tha string value of an Atom.
// The function panics, if the Atom was not part of this
// Registry
func (r *Registry) String(a Atom) string {
	r.mu.Lock()
	defer r.mu.Unlock()

	index := int(a)
	if 0 <= index && index < len(r.atom2string) {
		return r.atom2string[index]
	}
	panic(fmt.Sprint("atom(", a, ") is not known in this registry"))
}
