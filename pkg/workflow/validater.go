package workflow

// Validater is an interface for activities to validate their proper function
type Validater interface {
	// Validate should check all properties for validity
	// and the existence of the required runtime services,
	// i.e. that required type casts succeed
	Validate(list *[]error, services interface{}, path string)
}

// TryValidate tests if v is a Validater and then validates it, otherwise it does nothing
func TryValidate(list *[]error, v interface{}, services interface{}, path, prop string) {
	if vv, ok := v.(Validater); ok {
		vv.Validate(list, services, path+prop)
	}
}
