package workflow

// Well-know XML namespaces for XAML produced by the .Net 4 WF designer
const (
	XmlnsStd     = "http://schemas.microsoft.com/netfx/2009/xaml/activities"
	XmlnsCcg     = "clr-namespace:System.Collections.Generic;assembly=mscorlib"
	XmlnsCco     = "clr-namespace:System.Collections.ObjectModel;assembly=mscorlib"
	XmlnsX       = "http://schemas.microsoft.com/winfx/2006/xaml"
	XmlnsSads    = "http://schemas.microsoft.com/netfx/2010/xaml/activities/debugger"
	XmlnsSap     = "http://schemas.microsoft.com/netfx/2009/xaml/activities/presentation"
	XmlnsSap2010 = "http://schemas.microsoft.com/netfx/2010/xaml/activities/presentation"
)

var ignoredNamespaces = map[string]bool{
	XmlnsSap:     true,
	XmlnsSap2010: true,
	XmlnsSads:    true,
}
