package workflow

// TriggerKind is the type of a Trigger
type TriggerKind int

const (
	// TriggerKindNull is used for the NullTrigger
	TriggerKindNull = TriggerKind(iota)
	// TriggerKindAwait is used for triggering Commands without attached data, i.e. usually for plain UI buttons
	TriggerKindAwait
	// TriggerKindAwaitData is used for triggering async responses, intended to be called by an MQ message with data
	TriggerKindAwaitData
	// TriggerKindImmediate is used by the immediate activity, which wants to be triggered as sonn as it is active
	TriggerKindImmediate
	// TriggerKindWakeup is used by Delay and Every activities, that want to be triggered after a timespan
	TriggerKindWakeup
	// TriggerKindHost is used by the host to signal something to its processor go routines, like read or change associated data. Such triggers are not intended to be signaled to the workflow instance.
	TriggerKindHost
)
