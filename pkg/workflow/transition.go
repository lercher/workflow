package workflow

import (
	"fmt"
)

// Transition stores metadata about a particular workflow state change
type Transition struct {
	DisplayName string
	To          *State
	Trigger     Triggerable
	Action      Runner
}

// Decode decodes the XAML representation of a state transition
func (tr *Transition) Decode(ld *Loader) error {
	ctx := ld.NewContext(tr)
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		tr.DisplayName = vv
		return nil
	}
	ctx.XamlProperty(XmlnsStd, "Transition", "To", func(v interface{}) error {
		vv := v.(*State)
		tr.To = vv
		return nil
	})
	ctx.XamlProperty(XmlnsStd, "Transition", "Trigger", func(v interface{}) error {
		if vv, ok := v.(Triggerable); ok {
			tr.Trigger = vv
			return nil
		}
		return fmt.Errorf("%T is no Triggerable as %T.Trigger", v, tr)
	})
	ctx.XamlProperty(XmlnsStd, "Transition", "Action", func(v interface{}) error {
		if vv, ok := v.(Runner); ok {
			tr.Action = vv
			return nil
		}
		return fmt.Errorf("%T is no Runner as %T.Action", v, tr)
	})
	return ctx.Continue()
}

func (tr *Transition) String() string {
	return fmt.Sprintf("%q -> %s", tr.DisplayName, tr.To.DisplayName)
}

// Validate validates this Transition itself and its Trigger and Action childs
func (tr *Transition) Validate(list *[]error, services interface{}, path string) {
	path += "/" + tr.DisplayName
	if tr.DisplayName == "" {
		path += "Transition"
	}
	TryValidate(list, tr.Trigger, services, path, ".Trigger")
	TryValidate(list, tr.Action, services, path, ".Action")
}
