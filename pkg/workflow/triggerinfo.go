package workflow

import (
	"fmt"
	"time"
)

type TriggerInfo struct {
	Trigger
	Display string
	Enabled bool
	WakeUp  time.Time
}

func NewTriggerInfo(kind TriggerKind, id, display string, enabled bool) TriggerInfo {
	return TriggerInfo{
		Trigger: Trigger{
			Kind: kind,
			ID:   TriggerID(id),
		},
		Display: display,
		Enabled: enabled,
	}
	// we intentionally leave out WakeUp here
	// it can be set to a proper value after creation
	// for a Delay or an Every
}

func (ti TriggerInfo) String() string {
	if ti.Enabled {
		if ti.WakeUp.After(time.Time{}) {
			return fmt.Sprintf("{%d:%s %s}", ti.Kind, ti.ID, ti.WakeUp.Format("15:04:05"))
		}
		return fmt.Sprintf("{%d:%s}", ti.Kind, ti.ID)
	}
	return fmt.Sprintf("(%d:%s)", ti.Kind, ti.ID)
}
