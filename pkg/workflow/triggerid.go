package workflow

type TriggerID string

const (
	// NullTriggerID is the empty TriggerID
	NullTriggerID = TriggerID("")

	// HostLoadAssociatedDataTriggerID is the TriggerID to signal a host data load
	HostLoadAssociatedDataTriggerID = TriggerID("load")

	// HostChangeAssociatedDataTriggerID is the TriggerID to signal a change and store data read
	HostChangeAssociatedDataTriggerID = TriggerID("change")
)
