package workflow

// Decoder is an interface that is implemented by an activity
// that wants to be deserialized from some XAML source
type Decoder interface {
	Decode(*Loader) error
}
