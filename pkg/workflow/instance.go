package workflow

import (
	"fmt"
	"time"
)

// ID identifies a workflow instance.
// It's long enough to store a GUID in it.
type ID [16]byte

func (id ID) String() string {
	return fmt.Sprintf("%x", [16]byte(id))
}

// ByteID returns 256 different special IDs
func ByteID(i byte) ID {
	ar := [16]byte{}
	ar[0] = i
	return ID(ar)
}

// InstanceDescriptor stores identity and the next time
// for action of a workflow instance. It is the most basic
// information for a scheduler to track instances
type InstanceDescriptor struct {
	ID     ID
	WakeUp time.Time
}

// Instance is an instance of a Definition.
// It represents a particular workflow in state Current,
// where all wake-up triggers were last executed at Timer.
// It's null value represents a created but not yet started workflow instance.
type Instance struct {
	InstanceDescriptor
	Definition   *Definition
	Current      Statename
	StateEntered time.Time
	Timer        map[TriggerID]time.Time
}

// Never is in the eternal future
var Never = time.Date(9999, 1, 1, 0, 0, 0, 0, time.UTC)

// NextWakeup calculates the next tick time after now of a regular ticker
// every this timespan that started at start
func NextWakeup(start, now time.Time, every time.Duration) time.Time {
	dist := now.Sub(start).Truncate(every) // dist is a multiple of every
	t := start.Add(dist)                   // right before now or now itself
	if t == now || t.Before(now) {
		t = t.Add(every) // right after now
	}
	return t
}

// Goto returns a new Instance with reset timers at the state sn entered at now
func (i Instance) Goto(sn Statename, now time.Time) Instance {
	i.Current = sn
	i.StateEntered = now
	i.Timer = make(map[TriggerID]time.Time)
	return i
}

// Alive returns true, if the Instance was not yet started or its current state is non-final
func (i Instance) Alive() bool {
	if i.Current == "" {
		return true
	}
	if st, ok := i.Definition.State[i.Current]; ok {
		return !st.IsFinal
	}
	return false
}
