package workflow

import (
	"bytes"
	"errors"
	"fmt"
)

type Statename string

type State struct {
	DisplayName Statename
	IsFinal     bool
	Transitions []*Transition
	Entry       Runner
	Exit        Runner
}

func (state *State) Decode(ld *Loader) error {
	ctx := ld.NewContext(state)
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		state.DisplayName = Statename(vv)
		return nil
	}
	ctx.Attribute[Xname("", "IsFinal")] = func(v interface{}) error {
		vv := v.(string)
		state.IsFinal = vv == "True"
		return nil
	}
	ctx.XamlProperty(XmlnsStd, "State", "Entry", func(v interface{}) error {
		if vv, ok := v.(Runner); ok {
			state.Entry = vv
			return nil
		}
		return fmt.Errorf("%T for State.Entry is not a Runner", v)
	})
	ctx.XamlProperty(XmlnsStd, "State", "Exit", func(v interface{}) error {
		if vv, ok := v.(Runner); ok {
			state.Exit = vv
			return nil
		}
		return fmt.Errorf("%T for State.Exit is not a Runner", v)
	})
	ctx.XamlProperty(XmlnsStd, "State", "Transitions", func(v interface{}) error {
		vv := v.(*Transition)
		// log.Println(" STATE", state.DisplayName, "ADD Transition", vv.DisplayName)
		state.Transitions = append(state.Transitions, vv)
		return nil
	})
	return ctx.Continue()
}

func (state *State) String() string {
	buf := new(bytes.Buffer)
	if state.IsFinal {
		fmt.Fprintf(buf, "Final ")
	}
	fmt.Fprintf(buf, "State %q ", state.DisplayName)
	fmt.Fprintf(buf, "{")
	for _, t := range state.Transitions {
		fmt.Fprint(buf, " ", t, " ")
	}
	fmt.Fprintf(buf, "}")
	return buf.String()
}

func (state *State) Validate(list *[]error, services interface{}, path string) {
	path += "/" + string(state.DisplayName)
	if state.DisplayName == "" {
		*list = append(*list, errors.New("State must have a DisplayName"))
	}
	TryValidate(list, state.Entry, services, path, ".Entry")
	TryValidate(list, state.Exit, services, path, ".Exit")
	for _, tr := range state.Transitions {
		TryValidate(list, tr, services, path, ".Transitions")
	}
}
