package workflow

import (
	"fmt"
	"strings"
)

type xamlreference struct {
	s string
}

func (ref *xamlreference) Decode(ld *Loader) error {
	s, err := ld.TokenString()
	ref.s = s
	return err
}

func xamlreferenceFrom(s string) (*xamlreference, bool) {
	if strings.HasPrefix(s, "{x:Reference ") && strings.HasSuffix(s, "}") {
		inner := s
		inner = strings.TrimPrefix(inner, "{x:Reference ")
		inner = strings.TrimSuffix(inner, "}")
		return &xamlreference{inner}, true
	}
	return nil, false
}

func (ref *xamlreference) ForwardDereference(ld *Loader, r Resolver) Forward {
	return func() error {
		v, ok := ld.Reference[ref.s]
		if ok {
			return r(v)
		}
		return fmt.Errorf("xaml reference %q not found", ref.s)
	}
}

// ForwardDereferencer is an interface a type implements, when its XAML decoded state
// describes a reference to a not yet decoded object like a XAML {x:Reference name}
// as text of an attribute. Normally only xamlreference needs to implement it.
type ForwardDereferencer interface {
	ForwardDereference(ld *Loader, r Resolver) Forward
}
