package inspect

import (
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// WakeUp returns the next Enabled WakeUp in the list.
// If there is none, Never,"" is returned
func WakeUp(list []workflow.TriggerInfo) (time.Time, workflow.TriggerID) {
	min, id := workflow.Never, workflow.TriggerID("")
	for _, ti := range list {
		if ti.Enabled && ti.IsTimeoutKind() && ti.WakeUp.Before(min) {
			min, id = ti.WakeUp, ti.ID
		}
	}
	return min, id
}

// Runner finds the consequence of a particular trigger tr in the current state hierarchy of Instance i
func Runner(i workflow.Instance, tr workflow.Trigger) (workflow.Runner, *workflow.State, bool) {
	for _, st := range StateHierarchy(i.Definition, i.Current) {
		if r, to, ok := RunnerIn(st, i, tr); ok {
			return r, to, ok
		}
	}
	return nil, nil, false
}

// RunnerIn finds the consequence of the Trigger tr in State st only
func RunnerIn(st *workflow.State, i workflow.Instance, tr workflow.Trigger) (rr workflow.Runner, toto *workflow.State, ok bool) {
	visit(st, func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool {
		if t != nil {
			ti := t.Trigger(i)
			if ti.Is(tr) {
				rr, toto, ok = r, to, true
				return false // break visit
			}
		}
		return true // continue visit
	})
	return rr, toto, ok
}

// Triggers lists all SignalIDs that the state sn and its hierarchy handles
func Triggers(i workflow.Instance) (list []workflow.TriggerInfo) {
	if i.Alive() {
		for _, st := range StateHierarchy(i.Definition, i.Current) {
			list = AppendTriggers(list, st, i)
		}
	}
	return list
}

// TriggerNextWakeUp returns the next enabled Trigger of timeout kind
// of the workflow instance that is due strictly after now in the complete state hierarchy
func TriggerNextWakeUp(i workflow.Instance, now time.Time) (trtr workflow.Trigger, wu time.Time, ok bool) {
	isRelevant := func(ti workflow.TriggerInfo) bool {
		return ti.Enabled && ti.IsTimeoutKind() && ti.WakeUp.After(now)
	}
	wu = workflow.Never
	for _, st := range StateHierarchy(i.Definition, i.Current) {
		visit(st, func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool {
			if t != nil {
				ti := t.Trigger(i)
				if isRelevant(ti) && wu.After(ti.WakeUp) {
					trtr, wu, ok = ti.Trigger, ti.WakeUp, true
				}
			}
			return true // continue visit
		})
	}
	return trtr, wu, ok
}

// TriggerDue returns an enabled Trigger of timeout kind
// of the workflow instance that is due at or before now in the complete state hierarchy
func TriggerDue(i workflow.Instance, now time.Time) (workflow.Trigger, time.Time, bool) {
	if i.Alive() {
		for _, st := range StateHierarchy(i.Definition, i.Current) {
			if tr, wakeup, ok := TriggerDueIn(st, i, now); ok {
				return tr, wakeup, ok
			}
		}
	}
	return workflow.NullTrigger, time.Time{}, false
}

// TriggerDueIn returns an enabled Trigger of timeout kind
// of the workflow instance that is due at now or before now
func TriggerDueIn(st *workflow.State, i workflow.Instance, now time.Time) (trtr workflow.Trigger, wakeup time.Time, okok bool) {
	visit(st, func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool {
		if t != nil {
			ti := t.Trigger(i)
			if ti.Enabled && ti.Trigger.IsTimeoutKind() {
				if ti.WakeUp.Equal(now) || ti.WakeUp.Before(now) {
					trtr, wakeup, okok = ti.Trigger, ti.WakeUp, true
					return false // break visit
				}
			}
		}
		return true // continue visit
	})
	return trtr, wakeup, okok
}

// AppendTriggers appends all SignalIDs to list that the state st handles directly
func AppendTriggers(list []workflow.TriggerInfo, st *workflow.State, i workflow.Instance) []workflow.TriggerInfo {
	visit(st, func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool {
		if t != nil {
			ti := t.Trigger(i)
			list = append(list, ti)
		}
		return true // continue visit
	})
	return list
}

func visit(st *workflow.State, v func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool) {
	// we look into st's transition triggers and into termination pick branches (via Visiter)
	for _, tr := range st.Transitions {
		cont := v(tr.Trigger, tr.To, tr.Action)
		if !cont {
			return
		}
	}
	if pick, ok := st.Exit.(Visiter); ok && pick != nil {
		cont := pick.Visit(v)
		if !cont {
			return
		}
	}
}
