package inspect

import (
	"fmt"
	"testing"
)

func TestTriggersGlobal(t *testing.T) {
	def, _, _, global := setup(t)
	i := def.CreateInstance()
	i = i.Goto(global.DisplayName, testNow)
	ts := Triggers(i)
	want := `[{1:#Global} {1:#Change} {1:AnbGlobal} {2:MQ-MSG}]`
	got := fmt.Sprint(ts)
	if got != want {
		t.Errorf("Triggers(def, %q) got %s, want %s", i.Current, got, want)
	}
}

func TestTriggersStornoContainer(t *testing.T) {
	def, _, sc, _ := setup(t)
	i := def.CreateInstance()
	i = i.Goto(sc.DisplayName, testNow)
	ts := Triggers(i)
	want := `[{1:Cancel} {1:#Global} {1:#Change} {1:AnbGlobal} {2:MQ-MSG}]`
	got := fmt.Sprint(ts)
	if got != want {
		t.Errorf("Triggers(def, %q) got %s, want %s", i.Current, got, want)
	}
}

func TestTriggersAngebot(t *testing.T) {
	def, angebot, _, _ := setup(t)
	i := def.CreateInstance()
	i = i.Goto(angebot.DisplayName, testNow)
	ts := Triggers(i)
	want := `[{1:Close} {4:After 10s 12:00:10} {1:Rest} {4:After 4s 12:00:04} {1:Cancel} {1:#Global} {1:#Change} {1:AnbGlobal} {2:MQ-MSG}]`
	got := fmt.Sprint(ts)
	if got != want {
		t.Errorf("Triggers(def, %q) got %s, want %s", i.Current, got, want)
	}

	wu, id := WakeUp(ts)
	wantti := ts[3]
	if id != wantti.ID {
		t.Errorf("WakeUp want %v %v, got %v %v", wantti.ID, wantti.WakeUp, id, wu)
	}
}
