package inspect

import (
	"gitlab.com/lercher/workflow/pkg/workflow"
)

type Visiter interface {
	Visit(func(triggerable workflow.Triggerable, targetstate *workflow.State, runnable workflow.Runner) bool) bool
}
