package inspect

import (
	"fmt"
	"testing"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

func TestSuperState(t *testing.T) {
	_, angebot, sc, _ := setup(t)

	have := angebot
	want := sc
	got, ok := SuperState(have)
	if !ok {
		t.Fatalf("SuperState(%s) not found", have.DisplayName)
	}
	if got.DisplayName != want.DisplayName {
		t.Errorf("SuperState(%s) want %s, got %s", have.DisplayName, want.DisplayName, got.DisplayName)
	}

	have = sc
	got, ok = SuperState(have)
	if ok {
		t.Errorf("SuperState(%s) want <nil>, got %s", have.DisplayName, got.DisplayName)
	}
}

func TestParentState(t *testing.T) {
	def, angebot, sc, global := setup(t)

	have := angebot
	want := sc
	got, ok := ParentState(def, have)
	if !ok {
		t.Fatalf("ParentState(%s) not found", have.DisplayName)
	}
	if got.DisplayName != want.DisplayName {
		t.Errorf("ParentState(%s) want %s, got %s", have.DisplayName, want.DisplayName, got.DisplayName)
	}

	have = sc
	want = global
	got, ok = ParentState(def, have)
	if !ok {
		t.Fatalf("ParentState(%s) not found", have.DisplayName)
	}
	if got.DisplayName != want.DisplayName {
		t.Errorf("ParentState(%s) want %s, got %s", have.DisplayName, want.DisplayName, got.DisplayName)
	}
}

func TestHierarchyAngebot(t *testing.T) {
	def, angebot, sc, global := setup(t)
	got := StateHierarchy(def, angebot.DisplayName)
	want := []*workflow.State{angebot, sc, global}
	if fmt.Sprint(got) != fmt.Sprint(want) {
		t.Errorf("StateHierarchy(def, %q) want %v, got %v", angebot.DisplayName, want, got)
	}
}

func TestHierarchyStornoContainer(t *testing.T) {
	def, _, sc, global := setup(t)
	got := StateHierarchy(def, sc.DisplayName)
	want := []*workflow.State{sc, global}
	if fmt.Sprint(got) != fmt.Sprint(want) {
		t.Errorf("StateHierarchy(def, %q) want %v, got %v", sc.DisplayName, want, got)
	}
}

func TestHierarchyGlobal(t *testing.T) {
	def, _, _, global := setup(t)
	got := StateHierarchy(def, GlobalStatename)
	want := []*workflow.State{global}
	if fmt.Sprint(got) != fmt.Sprint(want) {
		t.Errorf("StateHierarchy(def, %q) want %v, got %v", global.DisplayName, want, got)
	}
}
