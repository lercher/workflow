package inspect

import (
	"gitlab.com/lercher/workflow/pkg/workflow"
)

// GlobalStatename is the well-known name of state in a workflow.Definition.
// If that state exists, it is on top of every StateHierarchy
// and it is the last state that repeated calls to ParentState return.
const GlobalStatename = workflow.Statename("_Global")

// StateHierarchy returns sn and all states it inherits of, including _Global.
// The list is ordered by inheritance with sn at position 0 and _Global last.
// If def has no state named _Global, the list ends with the topmost state according to the ordering.
// If sn is undefined in def, nil is returned.
func StateHierarchy(def *workflow.Definition, sn workflow.Statename) (list []*workflow.State) {
	st, ok := def.State[sn]
	if !ok {
		return nil
	}
	for {
		list = append(list, st)
		if st, ok = ParentState(def, st); !ok {
			return list
		}
	}
}

// ParentState returns the declared parent state of st, true if it exists.
// If st has no SuperState, ParentState returns _Global, true if _Global exists or nil, false else.
// If st is _Global it returns _Global, false.
func ParentState(def *workflow.Definition, st *workflow.State) (*workflow.State, bool) {
	if st == nil {
		return nil, false
	}
	if st.DisplayName == GlobalStatename {
		return st, false
	}
	if super, ok := SuperState(st); ok {
		return super, true
	}
	if global, ok := def.State[GlobalStatename]; ok {
		return global, true
	}
	return nil, false
}

// SuperState returns the first triggerless transition with a target state and returns that state.
// It is also called "declared parent state of st",
// i.e. the state, st wants to inherit transitions from.
// Use ParentState with a workflow.Definition to include _Global.
// SuperState of _Global is not meaningful for this package and thus undefined,
// even if it might return a state.
func SuperState(st *workflow.State) (*workflow.State, bool) {
	if st == nil {
		return nil, false
	}
	for _, t := range st.Transitions {
		if t.Trigger == nil && t.To != nil {
			return t.To, true
		}
	}
	return nil, false
}
