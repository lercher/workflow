package inspect

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/activity"
	"gitlab.com/lercher/workflow/pkg/workflow/behaviour"
)

var testNow = time.Date(2019, 12, 21, 12, 0, 0, 0, time.Local)

func setup(t *testing.T) (*workflow.Definition, *workflow.State, *workflow.State, *workflow.State) {
	t.Helper()
	rd := strings.NewReader(xaml)
	ld := workflow.NewLoader(rd)
	activity.RegisterAll(ld)
	def, err := ld.Decode()
	if err != nil {
		t.Fatal(err)
	}
	errs := def.Validate(behaviour.New(false, nil))
	if len(errs) > 0 {
		t.Errorf("got validation errors %v", errs)
	}
	angebot := state(t, def, "Angebot")
	sc := state(t, def, "StornoContainer")
	global := state(t, def, GlobalStatename)
	return def, angebot, sc, global
}

func state(t *testing.T, def *workflow.Definition, sn workflow.Statename) *workflow.State {
	st, ok := def.State[sn]
	if !ok {
		t.Fatalf("State(%s) not found", sn)
	}
	return st
}

const xaml = `<Activity 
    mc:Ignorable="sap sap2010 sads" 
    x:Class="CasFiniteStateSample.PosLikeWorkflow" 
    sap2010:ExpressionActivityEditor.ExpressionActivityEditor="C#" 
    sap2010:WorkflowViewState.IdRef="CasFiniteStateSample.PosLikeWorkflow_1"
 
    xmlns="http://schemas.microsoft.com/netfx/2009/xaml/activities"
    xmlns:av="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:lwa="clr-namespace:Lercher.Workflow.Activities;assembly=Lercher.Workflow.Activities"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:mca="clr-namespace:Microsoft.CSharp.Activities;assembly=System.Activities"
    xmlns:sads="http://schemas.microsoft.com/netfx/2010/xaml/activities/debugger"
    xmlns:sap="http://schemas.microsoft.com/netfx/2009/xaml/activities/presentation"
    xmlns:sap2010="http://schemas.microsoft.com/netfx/2010/xaml/activities/presentation"
    xmlns:scg="clr-namespace:System.Collections.Generic;assembly=mscorlib"
    xmlns:sco="clr-namespace:System.Collections.ObjectModel;assembly=mscorlib"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
  >
  <TextExpression.NamespacesForImplementation>
    <sco:Collection x:TypeArguments="x:String">
      <x:String>System</x:String>
      <x:String>System.Collections.Generic</x:String>
      <x:String>System.Data</x:String>
      <x:String>System.Linq</x:String>
      <x:String>System.Text</x:String>
    </sco:Collection>
  </TextExpression.NamespacesForImplementation>
  <TextExpression.ReferencesForImplementation>
    <sco:Collection x:TypeArguments="AssemblyReference">
      <AssemblyReference>Microsoft.CSharp</AssemblyReference>
      <AssemblyReference>System</AssemblyReference>
      <AssemblyReference>System.Activities</AssemblyReference>
      <AssemblyReference>System.Core</AssemblyReference>
      <AssemblyReference>System.Data</AssemblyReference>
      <AssemblyReference>System.Runtime.Serialization</AssemblyReference>
      <AssemblyReference>System.ServiceModel</AssemblyReference>
      <AssemblyReference>System.ServiceModel.Activities</AssemblyReference>
      <AssemblyReference>System.Xaml</AssemblyReference>
      <AssemblyReference>System.Xml</AssemblyReference>
      <AssemblyReference>System.Xml.Linq</AssemblyReference>
      <AssemblyReference>mscorlib</AssemblyReference>
      <AssemblyReference>CasFiniteStateSample</AssemblyReference>
    </sco:Collection>
  </TextExpression.ReferencesForImplementation>
  <StateMachine DisplayName="Sample POS Workflow" sap2010:WorkflowViewState.IdRef="StateMachine_1">
    <StateMachine.InitialState>
      <State x:Name="__ReferenceID4" DisplayName="Setup only" sap2010:WorkflowViewState.IdRef="State_6">
        <State.Entry>
          <lwa:ExecuteRulesetActivity Action="{x:Null}" DisplayName="Setup Rules" Enabled="True" sap2010:WorkflowViewState.IdRef="ExecuteRulesetActivity_6" RulesetName="Setup" />
        </State.Entry>
        <State.Transitions>
          <Transition DisplayName="T9" sap2010:WorkflowViewState.IdRef="Transition_9">
            <Transition.Trigger>
              <lwa:ImmediateActivity Enabled="True" sap2010:WorkflowViewState.IdRef="ImmediateActivity_1" />
            </Transition.Trigger>
            <Transition.To>
              <State x:Name="__ReferenceID1" DisplayName="Neu" sap2010:WorkflowViewState.IdRef="State_3">
                <State.Entry>
                  <Sequence sap2010:WorkflowViewState.IdRef="Sequence_2">
                    <lwa:SetOwnerActivity DisplayName="#C-Owner" Enabled="True" sap2010:WorkflowViewState.IdRef="SetOwnerActivity_1" NewOwner="#C" />
                  </Sequence>
                </State.Entry>
                <State.Exit>
                  <Pick DisplayName="Events (PickActivity)" sap2010:WorkflowViewState.IdRef="Pick_2">
                    <PickBranch DisplayName="Export ls.pro" sap2010:WorkflowViewState.IdRef="PickBranch_5">
                      <PickBranch.Trigger>
                        <lwa:CommandActivity CommandKey="ExportLSP" CommandRoles="*" DisplayName="Export LeasySOFT.pro" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_4" />
                      </PickBranch.Trigger>
                      <Sequence sap2010:WorkflowViewState.IdRef="Sequence_5">
                        <lwa:DataExporterActivity Destination="LeasySOFT.pro" Enabled="True" sap2010:WorkflowViewState.IdRef="DataExporterActivity_1" />
                        <lwa:TransactMQActivity ConfigurationKey="A-Transact-Key" Enabled="True" sap2010:WorkflowViewState.IdRef="TransactMQActivity_1">
                          <lwa:TransactMQActivity.DelayedResult>
                            <Sequence DisplayName="Delayed Sequence" sap2010:WorkflowViewState.IdRef="Sequence_8">
                              <lwa:RecordStatisticsActivity ConstantValues="{x:Null}" Enabled="True" sap2010:WorkflowViewState.IdRef="RecordStatisticsActivity_2" StatisticsKey="Delayed" />
                              <lwa:SendTextMailActivity DisplayName="Interactive Mail" EmailMode="Interactive" Enabled="True" sap2010:WorkflowViewState.IdRef="SendTextMailActivity_1" XSLTransformKey="IM" />
                            </Sequence>
                          </lwa:TransactMQActivity.DelayedResult>
                          <lwa:TransactMQActivity.ImmediateResult>
                            <Sequence DisplayName="Immediate Sequence" sap2010:WorkflowViewState.IdRef="Sequence_7">
                              <lwa:PopupMessageActivity Enabled="True" sap2010:WorkflowViewState.IdRef="PopupMessageActivity_3" Message="Immediate" />
                              <lwa:NewStateActivity Enabled="True" sap2010:WorkflowViewState.IdRef="NewStateActivity_2" NewStatename="Ruhe" />
                            </Sequence>
                          </lwa:TransactMQActivity.ImmediateResult>
                        </lwa:TransactMQActivity>
                      </Sequence>
                    </PickBranch>
                    <PickBranch DisplayName="Export CAS" sap2010:WorkflowViewState.IdRef="PickBranch_6">
                      <PickBranch.Trigger>
                        <lwa:CommandActivity CommandKey="ExportCAS" CommandRoles="*" DisplayName="Export Cassiopae" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_5" />
                      </PickBranch.Trigger>
                      <lwa:DataExporterActivity Destination="CAS" Enabled="True" sap2010:WorkflowViewState.IdRef="DataExporterActivity_2" />
                    </PickBranch>
                    <PickBranch DisplayName="Every 20s" sap2010:WorkflowViewState.IdRef="PickBranch_11">
                      <PickBranch.Trigger>
                        <lwa:EveryActivity Enabled="True" Every="00:00:20" sap2010:WorkflowViewState.IdRef="EveryActivity_2" />
                      </PickBranch.Trigger>
                      <lwa:PopupMessageActivity Enabled="True" sap2010:WorkflowViewState.IdRef="PopupMessageActivity_2" Message="20s timeout" />
                    </PickBranch>
                  </Pick>
                </State.Exit>
                <State.Transitions>
                  <Transition DisplayName="Anbieten" sap2010:WorkflowViewState.IdRef="Transition_2">
                    <Transition.Trigger>
                      <lwa:CommandActivity CommandKey="Offer" CommandRoles="#Offer-Role" DisplayName="Anbieten" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_1" />
                    </Transition.Trigger>
                    <Transition.To>
                      <State x:Name="__ReferenceID3" DisplayName="Angebot" sap2010:WorkflowViewState.IdRef="State_1">
                        <State.Entry>
                          <Sequence sap2010:WorkflowViewState.IdRef="Sequence_1">
                            <lwa:SetOwnerActivity Enabled="True" sap2010:WorkflowViewState.IdRef="SetOwnerActivity_2" NewOwner="Angebot-Owner" />
                          </Sequence>
                        </State.Entry>
                        <State.Exit>
                          <Pick sap2010:WorkflowViewState.IdRef="Pick_3">
                            <PickBranch DisplayName="4s keep state" sap2010:WorkflowViewState.IdRef="PickBranch_10">
                              <PickBranch.Trigger>
                                <lwa:DelayActivity AfterStateEntry="00:00:04" DisplayName="After 4s" Enabled="True" sap2010:WorkflowViewState.IdRef="DelayActivity_2" />
                              </PickBranch.Trigger>
                              <lwa:AttachmentsExporterActivity Destination="Attatchement Store" Enabled="True" sap2010:WorkflowViewState.IdRef="AttachmentsExporterActivity_1" />
                            </PickBranch>
                          </Pick>
                        </State.Exit>
                        <State.Transitions>
                          <Transition DisplayName="Beenden" sap2010:WorkflowViewState.IdRef="Transition_4">
                            <Transition.Trigger>
                              <lwa:CommandActivity CommandKey="Close" CommandRoles="#Closer" DisplayName="Beenden" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_2" />
                            </Transition.Trigger>
                            <Transition.To>
                              <State x:Name="__ReferenceID0" DisplayName="_Beendet" sap2010:WorkflowViewState.IdRef="State_2" IsFinal="True">
                                <State.Entry>
                                  <lwa:SetOwnerActivity Enabled="True" sap2010:WorkflowViewState.IdRef="SetOwnerActivity_4" NewOwner="Beendet-Owner" />
                                </State.Entry>
                              </State>
                            </Transition.To>
                          </Transition>
                          <Transition DisplayName="T5" sap2010:WorkflowViewState.IdRef="Transition_5">
                            <Transition.To>
                              <State x:Name="__ReferenceID2" DisplayName="StornoContainer" sap2010:WorkflowViewState.IdRef="State_5">
                                <State.Transitions>
                                  <Transition DisplayName="Stornieren" sap2010:WorkflowViewState.IdRef="Transition_7">
                                    <Transition.Trigger>
                                      <lwa:CommandActivity CommandKey="Cancel" CommandRoles="#CancelRole" DisplayName="Stornieren" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_3" />
                                    </Transition.Trigger>
                                    <Transition.To>
                                      <x:Reference>__ReferenceID0</x:Reference>
                                    </Transition.To>
                                  </Transition>
                                </State.Transitions>
                              </State>
                            </Transition.To>
                          </Transition>
                          <Transition DisplayName="Zurück nach 10s" sap2010:WorkflowViewState.IdRef="Transition_8">
                            <Transition.Trigger>
                              <lwa:DelayActivity AfterStateEntry="00:00:10" DisplayName="After 10s" Enabled="True" sap2010:WorkflowViewState.IdRef="DelayActivity_1" />
                            </Transition.Trigger>
                            <Transition.To>
                              <x:Reference>__ReferenceID1</x:Reference>
                            </Transition.To>
                            <Transition.Action>
                              <lwa:AddCommentActivity Enabled="True" sap2010:WorkflowViewState.IdRef="AddCommentActivity_1" Line="Zurück wegen 10s Timeout" />
                            </Transition.Action>
                          </Transition>
                          <Transition DisplayName="Ruhe" sap2010:WorkflowViewState.IdRef="Transition_11">
                            <Transition.Trigger>
                              <lwa:CommandActivity CommandKey="Rest" CommandRoles="*" DisplayName="Ruhen" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_9" />
                            </Transition.Trigger>
                            <Transition.To>
                              <State x:Name="__ReferenceID5" DisplayName="Ruhe" sap2010:WorkflowViewState.IdRef="State_7">
                                <State.Transitions>
                                  <Transition DisplayName="T10" sap2010:WorkflowViewState.IdRef="Transition_10" To="{x:Reference __ReferenceID2}" />
                                </State.Transitions>
                              </State>
                            </Transition.To>
                          </Transition>
                        </State.Transitions>
                      </State>
                    </Transition.To>
                    <Transition.Action>
                      <Sequence sap2010:WorkflowViewState.IdRef="Sequence_3">
                        <lwa:SetOwnerActivity Enabled="True" sap2010:WorkflowViewState.IdRef="SetOwnerActivity_3" NewOwner="Anbieten-Owner" />
                      </Sequence>
                    </Transition.Action>
                  </Transition>
                  <Transition DisplayName="T6" sap2010:WorkflowViewState.IdRef="Transition_6" To="{x:Reference __ReferenceID2}" />
                  <Transition DisplayName="Timeout" sap2010:WorkflowViewState.IdRef="Transition_1">
                    <Transition.Trigger>
						<lwa:DelayActivity AfterStateEntry="12:00:00" DisplayName="nach einem halben Tag" Enabled="True" sap2010:WorkflowViewState.IdRef="Delay_1" />
                    </Transition.Trigger>
                    <Transition.To>
                      <x:Reference>__ReferenceID0</x:Reference>
                    </Transition.To>
                  </Transition>
                </State.Transitions>
              </State>
            </Transition.To>
          </Transition>
        </State.Transitions>
      </State>
    </StateMachine.InitialState>
    <x:Reference>__ReferenceID1</x:Reference>
    <x:Reference>__ReferenceID0</x:Reference>
    <x:Reference>__ReferenceID3</x:Reference>
    <State DisplayName="_Global" sap2010:WorkflowViewState.IdRef="State_4">
      <State.Exit>
        <Pick sap2010:WorkflowViewState.IdRef="Pick_1">
          <PickBranch DisplayName="#Global" sap2010:WorkflowViewState.IdRef="PickBranch_1">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Global" CommandRoles="*" DisplayName="#Global" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_6" />
            </PickBranch.Trigger>
            <Sequence sap2010:WorkflowViewState.IdRef="Sequence_4">
              <lwa:ExecuteRulesetActivity Action="{x:Null}" DisplayName="#Global" Enabled="True" sap2010:WorkflowViewState.IdRef="ExecuteRulesetActivity_1" RulesetName="#Global" />
              <lwa:LookupExternalDataActivity Parameter="{x:Null}" DisplayName="LookupKunde" Enabled="True" sap2010:WorkflowViewState.IdRef="LookupExternalDataActivity_1" LookupName="Lookup_Kunde" />
              <lwa:ExecuteRulesetActivity Action="{x:Null}" DisplayName="Rechnen Rules" Enabled="True" sap2010:WorkflowViewState.IdRef="ExecuteRulesetActivity_5" RulesetName="Rechnen" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="#Change" sap2010:WorkflowViewState.IdRef="PickBranch_2">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Change" CommandRoles="*" DisplayName="#Change" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_7" />
            </PickBranch.Trigger>
            <Sequence sap2010:WorkflowViewState.IdRef="Sequence_6">
              <lwa:ExecuteRulesetActivity Action="{x:Null}" DisplayName="#Change" Enabled="True" sap2010:WorkflowViewState.IdRef="ExecuteRulesetActivity_2" RulesetName="#Change" />
              <lwa:HistoryBackActivity Enabled="True" sap2010:WorkflowViewState.IdRef="HistoryBackActivity_2" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="Global anbieten" sap2010:WorkflowViewState.IdRef="PickBranch_8">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="AnbGlobal" CommandRoles="*" DisplayName="Anb(global)" Enabled="True" sap2010:WorkflowViewState.IdRef="CommandActivity_8" />
            </PickBranch.Trigger>
            <lwa:NewStateActivity DisplayName="Gehe zu Angebot" Enabled="True" sap2010:WorkflowViewState.IdRef="NewStateActivity_1" NewStatename="Angebot" />
          </PickBranch>
          <PickBranch DisplayName="MQ MSG branch" sap2010:WorkflowViewState.IdRef="PickBranch_9">
            <PickBranch.Trigger>
              <lwa:ReceiveMQActivity ConfigurationKey="MQ-MSG" DisplayName="MQ Receive Msg" Enabled="True" sap2010:WorkflowViewState.IdRef="MQReceiveActivity_1" />
            </PickBranch.Trigger>
            <lwa:ExecuteRulesetActivity Action="{x:Null}" DisplayName="OnMSG Rules" Enabled="True" sap2010:WorkflowViewState.IdRef="ExecuteRulesetActivity_4" RulesetName="OnMSG" />
          </PickBranch>
        </Pick>
      </State.Exit>
    </State>
    <x:Reference>__ReferenceID2</x:Reference>
    <x:Reference>__ReferenceID4</x:Reference>
    <x:Reference>__ReferenceID5</x:Reference>
    <sads:DebugSymbol.Symbol>d0pDOlxEYXRlblxMZWFzeVNPRlQud29ya2Zsb3dcQ2Fzc2lvcGFlLlBvczIuV29ya2Zsb3dSdW5uZXJcUG9zV29ya2Zsb3cueGFtbC4nA/IBEgIBATMPwAEXAgEcex9/JwIBGWUXqgEfAgEMhAEfjwEnAgEJKQfEAQ8CAQWiAR+mAScCAQI1EzceAgE2OhNdGgIBJGIXYrcBAgEjrQEXrwEiAgEhtQEXuQEfAgEefSN9mAECARtnG2kmAgEXbBtzIgIBE3gfeLoBAgESlAEflAGoAQIBEJoBH5oBnwECAQ+fAR+fAbEBAgEOiAEniAHKAQIBCysLK7MBAgEIMA8wbQIBBzYVNpYBAgE3OxVQIgIBK1EVViICAShXFVwiAgElrgEZrgGPAQIBIrcBG7cBcQIBH2gdaJIBAgEYbR1yKgIBFD0ZPb8BAgE1PxdPIgIBLFMZU7sBAgEqVRdVjQECASlZGVmAAQIBJ1sXW5EBAgEmbyFvqQECARZxH3GyAQIBFUAZQJkBAgE0QRlOMgIBLUMdRigCATFJHUwoAgEuRB9EvQECATNFH0XPAQIBMkofSpcBAgEwSx9LjwECAS8=</sads:DebugSymbol.Symbol>
  </StateMachine>
  <sap2010:WorkflowViewState.ViewStateManager>
    <sap2010:ViewStateManager>
      <sap2010:ViewStateData Id="ExecuteRulesetActivity_6" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="ImmediateActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="SetOwnerActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_2" sap:VirtualizedContainerService.HintSize="222,146">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_4" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="DataExporterActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="RecordStatisticsActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="SendTextMailActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_8" sap:VirtualizedContainerService.HintSize="222,208">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="PopupMessageActivity_3" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="NewStateActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_7" sap:VirtualizedContainerService.HintSize="222,208">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="TransactMQActivity_1" sap:VirtualizedContainerService.HintSize="200,52.6666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">False</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Sequence_5" sap:VirtualizedContainerService.HintSize="222,238.666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="PickBranch_5" sap:VirtualizedContainerService.HintSize="252.666666666667,501.333333333333" />
      <sap2010:ViewStateData Id="CommandActivity_5" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="DataExporterActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PickBranch_6" sap:VirtualizedContainerService.HintSize="230.666666666667,501.333333333333" />
      <sap2010:ViewStateData Id="EveryActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PopupMessageActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PickBranch_11" sap:VirtualizedContainerService.HintSize="230.666666666667,501.333333333333" />
      <sap2010:ViewStateData Id="Pick_2" sap:VirtualizedContainerService.HintSize="908,547.333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="SetOwnerActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_1" sap:VirtualizedContainerService.HintSize="222,146">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="DelayActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="AttachmentsExporterActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PickBranch_10" sap:VirtualizedContainerService.HintSize="230.666666666667,322.666666666667" />
      <sap2010:ViewStateData Id="Pick_3" sap:VirtualizedContainerService.HintSize="344.666666666667,368.666666666667" />
      <sap2010:ViewStateData Id="CommandActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="SetOwnerActivity_4" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="State_2" sap:VirtualizedContainerService.HintSize="146,98">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">457,451</av:Point>
            <av:Size x:Key="ShapeSize">146,98</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
            <x:Double x:Key="StateContainerWidth">132</x:Double>
            <x:Double x:Key="StateContainerHeight">62.040000000000077</x:Double>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_4" sap:VirtualizedContainerService.HintSize="430,496">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">330,310.833333333333 330,340.833333333333 544.6,340.833333333333 544.6,451</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">39</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">46</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_3" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Transition_7" sap:VirtualizedContainerService.HintSize="430,496">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">407,134.033333333333 633,134.033333333333 633,485.3 603,485.3</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">28</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">24</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_5" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">293,109.5</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_5" sap:VirtualizedContainerService.HintSize="430,507">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">347.1,249.5 347.1,219.5 344.3,219.5 344.3,170.833333333333</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">50</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">35</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="DelayActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="AddCommentActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Transition_8" sap:VirtualizedContainerService.HintSize="430,496">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">307.2,310.833333333333 307.2,340.833333333333 102.8,340.833333333333 102.8,310.833333333333</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">23</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">55</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_9" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Transition_10">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">514.3,249.333333333333 514.3,219.333333333333 361.4,219.333333333333 361.4,170.833333333333</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">34</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">47</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_7" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">463,249.333333333333</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_11" sap:VirtualizedContainerService.HintSize="431.333333333333,512">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">387,280.166666666667 417,280.166666666667 417,280 463,280</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">36</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">37</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_1" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">273,249.5</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="SetOwnerActivity_3" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_3" sap:VirtualizedContainerService.HintSize="222,146">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">False</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_2" sap:VirtualizedContainerService.HintSize="430,496">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">137,286.3 273,286.3</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">44</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">45</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_6" sap:VirtualizedContainerService.HintSize="431.333333333333,512">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">91.4,249.5 91.4,219.5 327.2,219.5 327.2,170.833333333333</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">46</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">23</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Delay_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Transition_1" sap:VirtualizedContainerService.HintSize="430,496">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:PointCollection x:Key="ConnectorLocation">74.3,310.833333333333 74.3,500 457,500</av:PointCollection>
            <x:Int32 x:Key="SrcConnectionPointIndex">35</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">37</x:Int32>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_3" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">23,249.5</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Transition_9" sap:VirtualizedContainerService.HintSize="431.333333333333,512">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Int32 x:Key="SrcConnectionPointIndex">39</x:Int32>
            <x:Int32 x:Key="DestConnectionPointIndex">34</x:Int32>
            <av:PointCollection x:Key="ConnectorLocation">70,180.666666666666 70,210.666666666666 74.3,210.666666666666 74.3,249.5</av:PointCollection>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_6" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">13,119.333333333333</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_6" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="ExecuteRulesetActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="LookupExternalDataActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="ExecuteRulesetActivity_5" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_4" sap:VirtualizedContainerService.HintSize="222,270">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="PickBranch_1" sap:VirtualizedContainerService.HintSize="252.666666666667,532.666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_7" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="ExecuteRulesetActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="HistoryBackActivity_2" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="Sequence_6" sap:VirtualizedContainerService.HintSize="222,208">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="PickBranch_2" sap:VirtualizedContainerService.HintSize="252.666666666667,532.666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CommandActivity_8" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="NewStateActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PickBranch_8" sap:VirtualizedContainerService.HintSize="230.666666666667,532.666666666667" />
      <sap2010:ViewStateData Id="MQReceiveActivity_1" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="ExecuteRulesetActivity_4" sap:VirtualizedContainerService.HintSize="200,22" />
      <sap2010:ViewStateData Id="PickBranch_9" sap:VirtualizedContainerService.HintSize="230.666666666667,532.666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="Pick_1" sap:VirtualizedContainerService.HintSize="1200.66666666667,578.666666666667">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">True</x:Boolean>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="State_4" sap:VirtualizedContainerService.HintSize="114,61.3333333333333">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <av:Point x:Key="ShapeLocation">173,9.5</av:Point>
            <av:Size x:Key="ShapeSize">114,61.3333333333333</av:Size>
            <x:Boolean x:Key="IsPinned">False</x:Boolean>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="StateMachine_1" sap:VirtualizedContainerService.HintSize="680,636">
        <sap:WorkflowViewStateService.ViewState>
          <scg:Dictionary x:TypeArguments="x:String, x:Object">
            <x:Boolean x:Key="IsExpanded">False</x:Boolean>
            <av:Point x:Key="ShapeLocation">40,2.5</av:Point>
            <x:Double x:Key="StateContainerWidth">666</x:Double>
            <x:Double x:Key="StateContainerHeight">600</x:Double>
            <av:PointCollection x:Key="ConnectorLocation">70,77.1666666666667 70,119.333333333333</av:PointCollection>
          </scg:Dictionary>
        </sap:WorkflowViewStateService.ViewState>
      </sap2010:ViewStateData>
      <sap2010:ViewStateData Id="CasFiniteStateSample.PosLikeWorkflow_1" sap:VirtualizedContainerService.HintSize="654,716" />
    </sap2010:ViewStateManager>
  </sap2010:WorkflowViewState.ViewStateManager>
</Activity>`
