package workflow

import (
	"bytes"
	"fmt"
	"sort"
)

// Definition describes the structure of a state machine (aka workflow or workflow type)
// in terms of events/triggers, timeouts and behaviourless activities
// that get executed when a trigger is signaled.
// It can create Instances that share this Definition
// and represent a state machine currently being in a particular State of this
// definition at any time until the Instance terminates.
type Definition struct {
	State        map[Statename]*State
	InitialState *State
}

// NewDefinition creates a new empty Definition
func NewDefinition() *Definition {
	return &Definition{
		State: make(map[Statename]*State),
	}
}

// CreateInstance creates a new Instance for this workflow type.
// It need to be run immediatly to be initialized in the InitialState.
func (def *Definition) CreateInstance() Instance {
	return Instance{
		Definition: def,
	}
}

// Decode loads a Definition from a XAML source
func (def *Definition) Decode(ld *Loader) error {
	// attribute InitialState with an {x:Reference ..}
	// list of (State or <x:Reference/>)
	ctx := ld.NewContext(def)
	ctx.XamlProperty(XmlnsStd, "StateMachine", "InitialState", func(v interface{}) error {
		def.InitialState = v.(*State)
		return nil
	})
	ctx.Children = func(v interface{}) error {
		st := v.(*State)
		def.State[st.DisplayName] = st
		return nil
	}
	return ctx.Continue()
}

// String forms a string description of a Definition
func (def *Definition) String() string {
	buf := new(bytes.Buffer)
	fmt.Fprintln(buf, "Definition {")
	var list = make([]string, 0, len(def.State))
	for _, s := range def.State {
		list = append(list, string(s.DisplayName))
	}
	sort.Strings(list)
	for _, sn := range list {
		s := def.State[Statename(sn)]
		fmt.Fprint(buf, "  ")
		if s == def.InitialState {
			fmt.Fprint(buf, "-> ")
		}
		fmt.Fprint(buf, s)
		fmt.Fprintln(buf)
	}
	fmt.Fprint(buf, "}")
	return buf.String()
}

// Validate returns a list of errors of all problems detected
// by the tree of States, Transistions, Triggers and other activities
func (def *Definition) Validate(services interface{}) (list []error) {
	if def.InitialState == nil {
		list = append(list, fmt.Errorf("InitialState must not be empty"))
	} else {
		if _, ok := def.State[def.InitialState.DisplayName]; !ok {
			list = append(list, fmt.Errorf("the InitialState %q must be a member of State", def.InitialState.DisplayName))
		}
	}
	for _, st := range def.State {
		TryValidate(&list, st, services, "", "States")
	}
	return list
}
