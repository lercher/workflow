package activity

import (
	"fmt"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Every is a Triggerable activity that regularly waits for
// a timespan to pass after state entry and is then
// repeatedly in the signaled state
type Every struct {
	Common
	Every time.Duration
}

// Decode decodes XAML properties of an Every activity
func (e *Every) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(e)
	e.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "Every")] = func(v interface{}) error {
		vv := v.(string)
		e.Every = parseDuration(vv)
		return nil
	}
	return ctx.Continue()
}

// Trigger signals engine and inspect, by what SignalID I want to be triggered
func (e *Every) Trigger(i workflow.Instance) workflow.TriggerInfo {
	ti := workflow.NewTriggerInfo(workflow.TriggerKindWakeup, e.DisplayName, "*EVERY*", e.Enabled)
	if t, ok := i.Timer[ti.ID]; ok {
		ti.WakeUp = workflow.NextWakeup(i.StateEntered, t, e.Every)
	} else {
		ti.WakeUp = workflow.NextWakeup(i.StateEntered, i.StateEntered, e.Every)
	}
	return ti
}

// Validate validates properties and required services type casts
func (e *Every) Validate(list *[]error, services interface{}, path string) {
	e.validate(list, &path, "Every")
	if e.Every.Nanoseconds() <= 0 {
		*list = append(*list, fmt.Errorf("%T.Every must be positive, got %v", e, e.Every))
	}
}
