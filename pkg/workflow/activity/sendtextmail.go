package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// <SendTextMailActivity [Description="Email Kunden" DisplayName="sendEmailAnschreiben" EmailMode="Interactive" Enabled="True" XSLTransformKey="Email_Kunde"]>

// SendTextMail is a Runner activity that instructs the host based on EmailMode
// to either directly send an email message or initiate a send email UI
type SendTextMail struct {
	Common
	EmailMode       string
	XSLTransformKey string
}

// Run executes the SendTextMail activity
func (stm *SendTextMail) Run(ctx *workflow.Context) error {
	if !stm.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s %s", stm, stm.DisplayName, stm.EmailMode, stm.XSLTransformKey)
	if sm, ok := ctx.RuntimeServices.(SendMailer); ok {
		sm.SendMail(stm.EmailMode, stm.XSLTransformKey)
	}
	return nil
}

// Decode decodes the XAML properties of a SendTextMail activity
func (stm *SendTextMail) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(stm)
	stm.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "EmailMode")] = func(v interface{}) error {
		vv := v.(string)
		stm.EmailMode = vv
		return nil
	}
	ctx.Attribute[Xname("", "XSLTransformKey")] = func(v interface{}) error {
		vv := v.(string)
		stm.XSLTransformKey = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (stm *SendTextMail) Validate(list *[]error, services interface{}, path string) {
	stm.validate(list, &path, "SendTextMail")
	stm.validateNonNull(list, path, "SendTextMail", "XSLTransformKey", stm.XSLTransformKey)
	if _, ok := services.(SendMailer); !ok {
		serviceError(list, services, stm, "SendMailer")
	}
}

// SendMailer must be implemented by a runtime service
type SendMailer interface {
	SendMail(emailmode, transformkey string) error
}
