package activity

import (
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// PickBranch is the child type of the Pick composite.
// It hosts Triggers and their Consequences to be signaled
// and processed by the host
type PickBranch struct {
	DisplayName string
	Trigger     workflow.Triggerable
	Action      workflow.Runner
}

// Decode decodes XAML propertis of the Pick dependent PickBranch type
func (br *PickBranch) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(br)
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		br.DisplayName = vv
		return nil
	}
	ctx.XamlProperty(workflow.XmlnsStd, "PickBranch", "Trigger", func(v interface{}) error {
		if vv, ok := v.(workflow.Triggerable); ok {
			br.Trigger = vv
			return nil
		}
		return fmt.Errorf("%T is no Triggerable as %T.Trigger", v, br)
	})
	ctx.Children = func(v interface{}) error {
		if vv, ok := v.(workflow.Runner); ok {
			br.Action = vv
			return nil
		}
		return fmt.Errorf("%T is no Runner as %T.Action", v, br)
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (br *PickBranch) Validate(list *[]error, services interface{}, path string) {
	path += "/" + br.DisplayName
	if br.DisplayName == "" {
		*list = append(*list, fmt.Errorf("%T must have a DisplayName [%s]", br, path))
	}
	workflow.TryValidate(list, br.Trigger, services, path, ".Trigger")
	workflow.TryValidate(list, br.Action, services, path, ".Action")
}
