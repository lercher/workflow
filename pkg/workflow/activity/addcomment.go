package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// AddComment is a Runner activity to have the host store a static comment in its trace
type AddComment struct {
	Common
	Line string
}

// Run executes the AddComment activity
func (ac *AddComment) Run(ctx *workflow.Context) error {
	if !ac.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %q", ac, ac.DisplayName, ac.Line)
	if c, ok := ctx.RuntimeServices.(Commenter); ok {
		c.Comment(ac.Line)
	}
	return nil
}

// Decode decodes the XAML representation of an AddComment activity
func (ac *AddComment) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(ac)
	ac.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "Line")] = func(v interface{}) error {
		vv := v.(string)
		ac.Line = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (ac *AddComment) Validate(list *[]error, services interface{}, path string) {
	ac.validate(list, &path, "AddComment")
	ac.validateNonNull(list, path, "AddComment", "Line", ac.Line)
	if _, ok := services.(Commenter); !ok {
		serviceError(list, services, ac, "Commenter")
	}
}

// Commenter needs to be implemented by a runtime service for AddComment
type Commenter interface {
	Comment(string)
}
