package activity

import (
	"testing"
	"time"
)

func TestParseDurationNet1(t *testing.T) {
	s := "01:20:33"
	got := parseDuration(s)
	want := 1*time.Hour + 20*time.Minute + 33*time.Second
	if got != want {
		t.Errorf("parseDuration(%s) got %v, want %v", s, got, want)
	}
}

func TestParseDurationNetDays(t *testing.T) {
	s := "15.01:20:33"
	got := parseDuration(s)
	want := (15*24+1)*time.Hour + 20*time.Minute + 33*time.Second
	if got != want {
		t.Errorf("parseDuration(%s) got %v, want %v", s, got, want)
	}
}

func TestParseDurationGo(t *testing.T) {
	s := "20m"
	got := parseDuration(s)
	want := 20 * time.Minute
	if got != want {
		t.Errorf("parseDuration(%s) got %v, want %v", s, got, want)
	}
}
