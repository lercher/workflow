// Package activity provides behaviour-less types
// that store properties and orchestrate a workflow (state machine).
// They are usually combined with a RuntimeServices interface{} that may
// or may not implement an interface specific to an activity.
// If the interface is satisfied, its methods are called, if not,
// nothing is called and no error is generated.
// To ensure proper operation of the activities of a workflow definition
// call the definition's Validate() method or an activity's respective
// Validate() method with a runtime service object.
// There will be errors if an activity's properties are not set
// or the runtime service object is missing a relevant interface
// implementation.
package activity

import (
	"encoding/xml"
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

const (
	// XmlnsStd is the standard XAML namespace for .net 4 activities
	XmlnsStd = "http://schemas.microsoft.com/netfx/2009/xaml/activities"
	// XmlnsLwa is the XAML namespace for custom activities driving this implementation
	XmlnsLwa = "clr-namespace:Lercher.Workflow.Activities;assembly=Lercher.Workflow.Activities"
)

// RegisterAll registers this package's activities under their respective XAML namespaces and
// element names to the given Loader
func RegisterAll(ld *workflow.Loader) {
	// .Net Standard WF4 activities
	ld.Register(XmlnsStd, "Pick", func() workflow.Decoder { return &Pick{} })
	ld.Register(XmlnsStd, "PickBranch", func() workflow.Decoder { return &PickBranch{} })
	ld.Register(XmlnsStd, "Sequence", func() workflow.Decoder { return &Sequence{} })

	// Triggers
	ld.Register(XmlnsLwa, "CommandActivity", func() workflow.Decoder { return &Command{} })
	ld.Register(XmlnsLwa, "ImmediateActivity", func() workflow.Decoder { return &Immediate{} })
	ld.Register(XmlnsLwa, "DelayActivity", func() workflow.Decoder { return &Delay{} })
	ld.Register(XmlnsLwa, "EveryActivity", func() workflow.Decoder { return &Every{} })
	ld.Register(XmlnsLwa, "ReceiveMQActivity", func() workflow.Decoder { return &ReceiveMQ{} })

	// Executables
	ld.Register(XmlnsLwa, "ExecuteRulesetActivity", func() workflow.Decoder { return &ExecuteRuleset{} })
	ld.Register(XmlnsLwa, "DataExporterActivity", func() workflow.Decoder { return &DataExporter{} })
	ld.Register(XmlnsLwa, "SendTextMailActivity", func() workflow.Decoder { return &SendTextMail{} })
	ld.Register(XmlnsLwa, "SetTabActivity", func() workflow.Decoder { return &SetTab{} })
	ld.Register(XmlnsLwa, "AddCommentActivity", func() workflow.Decoder { return &AddComment{} })
	ld.Register(XmlnsLwa, "CreateRelatedItemActivity", func() workflow.Decoder { return &CreateRelatedItem{} })
	ld.Register(XmlnsLwa, "HistoryBackActivity", func() workflow.Decoder { return &HistoryBack{} })
	ld.Register(XmlnsLwa, "LookupExternalDataActivity", func() workflow.Decoder { return &LookupExternalData{} })
	ld.Register(XmlnsLwa, "NewStateActivity", func() workflow.Decoder { return &NewState{} })
	ld.Register(XmlnsLwa, "PopupMessageActivity", func() workflow.Decoder { return &PopupMessage{} })
	ld.Register(XmlnsLwa, "SetOwnerActivity", func() workflow.Decoder { return &SetOwner{} })
	ld.Register(XmlnsLwa, "TransactMQActivity", func() workflow.Decoder { return &TransactMQ{} })
	ld.Register(XmlnsLwa, "RecordStatisticsActivity", func() workflow.Decoder { return &RecordStatistics{} })
	ld.Register(XmlnsLwa, "AttachmentsExporterActivity", func() workflow.Decoder { return &AttachmentsExporter{} })
}

// Xname creates a qualified XML Name
func Xname(xmlns, localname string) xml.Name {
	return xml.Name{
		Space: xmlns,
		Local: localname,
	}
}

func serviceError(list *[]error, services, a interface{}, i string) {
	err := fmt.Errorf("%T needs to implement activitiy.%s for %T", services, i, a)
	e := err.Error()
	for _, e1 := range *list {
		if e1.Error() == e {
			return
		}
	}
	*list = append(*list, err)
}
