package activity

import (
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Pick is a container activity that gathers PickBranches.
// It needs to be a Runner formally, but it is an error
// to actually Run it. That means that Pick is only allowed
// in the Exit property of a State.
type Pick struct {
	DisplayName string
	Branches    []*PickBranch
}

// Run satisfies the Runner interface, however it is an error to actually call it.
func (p *Pick) Run(ctx *workflow.Context) error {
	return fmt.Errorf("%T %q(%d) can't be run by this engine, it must be placed in the Exit property of a State only", p, p.DisplayName, len(p.Branches))
}

// Decode decodes the XAML properties of a Pick activity
func (p *Pick) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(p)
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		p.DisplayName = vv
		return nil
	}
	ctx.Children = func(v interface{}) error {
		vv := v.(*PickBranch)
		p.Branches = append(p.Branches, vv)
		return nil
	}
	return ctx.Continue()
}

// Visit visits all PickBranches of this Pick and raises its Trigger and Action properties
// and no State change,  i.e. nil, to the visitor function v.
// It satisfies the Visiter interface of inspect.
func (p *Pick) Visit(v func(t workflow.Triggerable, to *workflow.State, r workflow.Runner) bool) bool {
	for _, br := range p.Branches {
		if br.Trigger != nil {
			cont := v(br.Trigger, nil, br.Action)
			if !cont {
				return false
			}
		}
	}
	return true
}

// Validate validates properties and required services type casts
func (p *Pick) Validate(list *[]error, services interface{}, path string) {
	path += "/" + p.DisplayName
	if p.DisplayName == "" {
		path += "Pick"
	}
	for _, br := range p.Branches {
		workflow.TryValidate(list, br, services, path, ".Branches")
	}
}
