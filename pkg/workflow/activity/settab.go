package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// <SetTabActivity [Description="" DisplayName="AntragTabStornoauswahl" Enabled="True" NewTab="100ANTRAG"]>

// SetTab is an activity that instructs the host
// to immediatly change the tab property for the UI of the workflow
// instance's associated data
type SetTab struct {
	Common
	NewTab string
}

// Run executes the SetTab activity
func (st *SetTab) Run(ctx *workflow.Context) error {
	if !st.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s", st, st.DisplayName, st.NewTab)
	if t, ok := ctx.RuntimeServices.(Taber); ok {
		t.SetTab(st.NewTab)
	}
	return nil
}

// Decode decodes the XAML properties of a SetTab activity
func (st *SetTab) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(st)
	st.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "NewTab")] = func(v interface{}) error {
		vv := v.(string)
		st.NewTab = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (st *SetTab) Validate(list *[]error, services interface{}, path string) {
	st.validate(list, &path, "SetTab")
	st.validateNonNull(list, path, "SetTab", "NewTab", st.NewTab)
	if _, ok := services.(Taber); !ok {
		serviceError(list, services, st, "Taber")
	}
}

// Taber must be implemented by a runtime service
type Taber interface {
	SetTab(string)
}
