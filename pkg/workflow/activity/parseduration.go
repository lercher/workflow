package activity

import (
	"regexp"
	"strconv"
	"time"
)

var reDotNetDuration = regexp.MustCompile(`^((\d+)\.)?(\d\d):(\d\d):(\d\d)$`)

func parseDuration(s string) time.Duration {
	m := reDotNetDuration.FindStringSubmatch(s)
	switch m {
	case nil:
		// no match
		break
	default:
		d, h, m, s := mustInt(m[2]), mustInt(m[3]), mustInt(m[4]), mustInt(m[5])
		return (d*24+h)*time.Hour + m*time.Minute + s*time.Second
	}
	d, err := time.ParseDuration(s)
	if err == nil {
		return d
	}
	return 100000 * time.Hour // That's quite long to time out
}

func mustInt(s string) time.Duration {
	i, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return 0
	}
	return time.Duration(i)
}
