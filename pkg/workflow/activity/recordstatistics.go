package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// RecordStatistics is a Runner activity that instructs the host
// to trace either constant numeric data or figures extacted from
// the workflow instance's associated data
type RecordStatistics struct {
	Common
	StatisticsKey  string
	ConstantValues string
}

// Run executes the RecordStatistics activity
func (rs *RecordStatistics) Run(ctx *workflow.Context) error {
	if !rs.Enabled {
		return nil
	}
	ctx.Logf("%T %s: stats=%s consts=%s", rs, rs.DisplayName, rs.StatisticsKey, rs.ConstantValues)
	// TODO: cast and call runtime service
	return nil
}

// Decode decodes the XAML properties of a RecordStatistics activity
func (rs *RecordStatistics) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(rs)
	rs.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "StatisticsKey")] = func(v interface{}) error {
		vv := v.(string)
		rs.StatisticsKey = vv
		return nil
	}
	ctx.Attribute[Xname("", "ConstantValues")] = func(v interface{}) error {
		vv := v.(string)
		rs.ConstantValues = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (rs *RecordStatistics) Validate(list *[]error, services interface{}, path string) {
	rs.validate(list, &path, "RecordStatistics")
	rs.validateNonNull(list, path, "RecordStatistics", "StatisticsKey", rs.StatisticsKey)
}
