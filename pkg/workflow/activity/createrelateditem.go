package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// CreateRelatedItem is a Runner activity that
// instructs the host to create a new workflow instance of type WFName
// and join them via a forward and a backward relation.
// Portions of associated data in the source and the newly created
// target are identified via CreateRelatedItem's fields.
type CreateRelatedItem struct {
	Common
	WFName           string
	ForwardRelation  string
	BackwardRelation string
	SourceXPath      string
	TargetXPath      string
	MyNameXPath      string
}

// Run executes a CreateRelatedItem activity
func (cri *CreateRelatedItem) Run(ctx *workflow.Context) error {
	if !cri.Enabled {
		return nil
	}
	ctx.Logf("%T %s: WF=%s rel=%s", cri, cri.DisplayName, cri.WFName, cri.ForwardRelation)
	// TODO: cast and call runtime service
	return nil
}

// Decode decodes XAML properties of a CreateRelatedItem activity
func (cri *CreateRelatedItem) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(cri)
	cri.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "WFName")] = func(v interface{}) error {
		vv := v.(string)
		cri.WFName = vv
		return nil
	}
	ctx.Attribute[Xname("", "ForwardRelation")] = func(v interface{}) error {
		vv := v.(string)
		cri.ForwardRelation = vv
		return nil
	}
	ctx.Attribute[Xname("", "BackwardRelation")] = func(v interface{}) error {
		vv := v.(string)
		cri.BackwardRelation = vv
		return nil
	}
	ctx.Attribute[Xname("", "SourceXPath")] = func(v interface{}) error {
		vv := v.(string)
		cri.SourceXPath = vv
		return nil
	}
	ctx.Attribute[Xname("", "TargetXPath")] = func(v interface{}) error {
		vv := v.(string)
		cri.TargetXPath = vv
		return nil
	}
	ctx.Attribute[Xname("", "MyNameXPath")] = func(v interface{}) error {
		vv := v.(string)
		cri.MyNameXPath = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (cri *CreateRelatedItem) Validate(list *[]error, services interface{}, path string) {
	cri.validate(list, &path, "CreateRelatedItem")
	cri.validateNonNull(list, path, "CreateRelatedItem", "WFName", cri.WFName)
	cri.validateNonNull(list, path, "CreateRelatedItem", "ForwardRelation", cri.ForwardRelation)
	cri.validateNonNull(list, path, "CreateRelatedItem", "BackwardRelation", cri.BackwardRelation)
	cri.validateNonNull(list, path, "CreateRelatedItem", "SourceXPath", cri.SourceXPath)
	cri.validateNonNull(list, path, "CreateRelatedItem", "TargetXPath", cri.TargetXPath)
	cri.validateNonNull(list, path, "CreateRelatedItem", "MyNameXPath", cri.MyNameXPath)
}
