package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// ExecuteRuleset is a Runnable activity that
// instructs the host to evaluate a particular set
// of business rules to the workflow's associated data
type ExecuteRuleset struct {
	Common
	// RulesetName is the name of the set of business rules to be executed by the host
	RulesetName string
	// Action is a string parameter that should be passed to the business rules by the host
	Action string
}

// Run executes the ExecuteRuleset activity
func (er *ExecuteRuleset) Run(ctx *workflow.Context) error {
	if !er.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %q %q", er, er.DisplayName, er.RulesetName, er.Action)
	if r, ok := ctx.RuntimeServices.(ExecuteRuleseter); ok {
		return r.ExecuteRuleset(er.RulesetName, er.Action)
	}
	return nil
}

// Decode decodes the XAML properties of an ExecuteRuleset activity
func (er *ExecuteRuleset) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(er)
	er.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "RulesetName")] = func(v interface{}) error {
		vv := v.(string)
		er.RulesetName = vv
		return nil
	}
	ctx.Attribute[Xname("", "Action")] = func(v interface{}) error {
		vv := v.(string)
		er.Action = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (er *ExecuteRuleset) Validate(list *[]error, services interface{}, path string) {
	er.validate(list, &path, "ExecuteRuleset")
	er.validateNonNull(list, path, "ExecuteRuleset", "RulesetName", er.RulesetName)
	if _, ok := services.(ExecuteRuleseter); !ok {
		serviceError(list, services, er, "ExecuteRuleseter")
	}
}

// ExecuteRuleseter must be implemented by a runtime service
type ExecuteRuleseter interface {
	ExecuteRuleset(key, param string) error
}
