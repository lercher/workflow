package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// AttachmentsExporter is a Runner activity to have the host export an attachment
type AttachmentsExporter struct {
	Common
	Destination string
}

// Run executes the AttachmentsExporter activity
func (ae *AttachmentsExporter) Run(ctx *workflow.Context) error {
	if !ae.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s", ae, ae.DisplayName, ae.Destination)
	if e, ok := ctx.RuntimeServices.(AttsExporter); ok {
		return e.AttsExport(ae.Destination)
	}
	return nil
}

// Decode decodes the XAML representation of an AttachmentsExporter activity
func (ae *AttachmentsExporter) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(ae)
	ae.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "Destination")] = func(v interface{}) error {
		vv := v.(string)
		ae.Destination = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (ae *AttachmentsExporter) Validate(list *[]error, services interface{}, path string) {
	ae.validate(list, &path, "AttachmentsExporter")
	ae.validateNonNull(list, path, "AttachmentsExporter", "Destination", ae.Destination)
	if _, ok := services.(AttsExporter); !ok {
		serviceError(list, services, ae, "AttsExporter")
	}
}

// AttsExporter needs to be implemented by a runtime service
type AttsExporter interface {
	AttsExport(string) error
}
