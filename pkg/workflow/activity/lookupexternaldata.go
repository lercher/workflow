package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// LookupExternalData is a Runner activity that instructs
// the host to lookup data in an external data source and incorporate
// the results in the workflow instance's associated data.
type LookupExternalData struct {
	Common
	LookupName string
	Parameter  string
}

// Run executes the LookupExternalData activity
func (led *LookupExternalData) Run(ctx *workflow.Context) error {
	if !led.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s param=%q", led, led.DisplayName, led.LookupName, led.Parameter)
	if l, ok := ctx.RuntimeServices.(Lookuper); ok {
		return l.Lookup(led.LookupName, led.Parameter)
	}
	return nil
}

// Decode decodes the XAML properties of a LookupExternalData activity
func (led *LookupExternalData) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(led)
	led.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "LookupName")] = func(v interface{}) error {
		vv := v.(string)
		led.LookupName = vv
		return nil
	}
	ctx.Attribute[Xname("", "Parameter")] = func(v interface{}) error {
		vv := v.(string)
		led.Parameter = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (led *LookupExternalData) Validate(list *[]error, services interface{}, path string) {
	led.validate(list, &path, "LookupExternalData")
	led.validateNonNull(list, path, "LookupExternalData", "LookupName", led.LookupName)
	if _, ok := services.(Lookuper); !ok {
		serviceError(list, services, led, "Lookuper")
	}
}

// Lookuper needs to be implemented by a runtime service
type Lookuper interface {
	Lookup(string, string) error
}
