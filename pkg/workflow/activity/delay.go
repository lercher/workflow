package activity

import (
	"fmt"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Delay is a Triggerable activity that waits for
// a timespan to pass after state entry and is then
// once in the signaled state
type Delay struct {
	Common
	AfterStateEntry time.Duration
}

// Decode decodes XAML properties of a Delay activity
func (d *Delay) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(d)
	d.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "AfterStateEntry")] = func(v interface{}) error {
		vv := v.(string)
		d.AfterStateEntry = parseDuration(vv)
		return nil
	}
	return ctx.Continue()
}

// Trigger signals engine and inspect, by what SignalID I want to be triggered
func (d *Delay) Trigger(i workflow.Instance) workflow.TriggerInfo {
	ti := workflow.NewTriggerInfo(workflow.TriggerKindWakeup, d.DisplayName, "*WAKEUP*", d.Enabled)
	if _, ok := i.Timer[ti.ID]; ok {
		ti.WakeUp = workflow.Never
	} else {
		ti.WakeUp = i.StateEntered.Add(d.AfterStateEntry)
	}
	return ti
}

// Validate validates properties and required services type casts
func (d *Delay) Validate(list *[]error, services interface{}, path string) {
	d.validate(list, &path, "Delay")
	if d.AfterStateEntry.Nanoseconds() <= 0 {
		*list = append(*list, fmt.Errorf("%T.AfterStateEntry must be positive, got %v", d, d.AfterStateEntry))
	}
}
