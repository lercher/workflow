package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// NewState is a Runner activity that instructs the host
// to change the workflow instance's state to the given one
// on idle.
type NewState struct {
	Common
	NewStatename workflow.Statename
}

// Run executes the NewState activity
func (ns *NewState) Run(ctx *workflow.Context) error {
	if !ns.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s", ns, ns.DisplayName, ns.NewStatename)
	ctx.NextState = ns.NewStatename // not now, but after all Runners idle
	return nil
}

// Decode decodes the XAML properties of a NewState activity
func (ns *NewState) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(ns)
	ns.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "NewStatename")] = func(v interface{}) error {
		vv := v.(string)
		ns.NewStatename = workflow.Statename(vv)
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (ns *NewState) Validate(list *[]error, services interface{}, path string) {
	ns.validate(list, &path, "NewState")
	ns.validateNonNull(list, path, "NewState", "NewStatename", string(ns.NewStatename))
}
