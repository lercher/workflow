package activity

import (
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Common gathers common attributes of LWA activities
type Common struct {
	// DisplayName is the name of this activity in the designer and in the logs
	DisplayName string
	// Description provides space to informally describe this particular instance's purpose
	Description string
	// Enabled - if false, Run succeeds without any action for Runners and Triggers are marked as disabled
	Enabled bool
}

// CommonAttribs decodes DisplayName, Description and Enabled.
// It's usually called by activities Decode methods that aggregate Common.
func (c *Common) CommonAttribs(ctx *workflow.LoaderContext) {
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		c.DisplayName = vv
		return nil
	}
	ctx.Attribute[Xname("", "Description")] = func(v interface{}) error {
		vv := v.(string)
		c.Description = vv
		return nil
	}
	ctx.Attribute[Xname("", "Enabled")] = func(v interface{}) error {
		vv := v.(string)
		c.Enabled = vv == "True"
		return nil
	}
}

func (c *Common) validate(list *[]error, path *string, iam string) {
	if c.DisplayName == "" {
		*path += "/" + c.DisplayName
	} else {
		*path += "/" + iam
	}
}

func (c *Common) validateNonNull(list *[]error, path, iam, propname, propval string) {
	if propval == "" {
		*list = append(*list, fmt.Errorf("%s must have a %s [%s]", iam, propname, path))
	}
}
