package activity

import (
	"gitlab.com/lercher/workflow/pkg/workflow"
)

// ReceiveMQ is a Triggerable that goes into the signaled state
// when data is received asynchronuosly by the host on the channel
// ConfigurationKey. It is usually placed as a trigger in the
// _Global state of a workflow or in state that is inherited from.
// It normally needs to be paired with a TransactMQ activity
// with the same ConfigurationKey in the consequences of a different
// trigger that initates the communication (async request response
// initiated by the workflow). If it is used stand-alone, it implements
// a sink of messages of a channel.
type ReceiveMQ struct {
	Common
	ConfigurationKey string
}

// TODO: needs to have a function to let the host process the received data
// when this activity is triggered

// Decode decodes the XAML properties of a ReceiveMQ activity
func (rmq *ReceiveMQ) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(rmq)
	rmq.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "ConfigurationKey")] = func(v interface{}) error {
		vv := v.(string)
		rmq.ConfigurationKey = vv
		return nil
	}
	return ctx.Continue()
}

// Trigger signals engine and inspect, by what SignalID I want to be triggered
func (rmq *ReceiveMQ) Trigger(i workflow.Instance) workflow.TriggerInfo {
	return workflow.NewTriggerInfo(workflow.TriggerKindAwaitData, rmq.ConfigurationKey, rmq.DisplayName, rmq.Enabled)
}

// Validate validates properties and required services type casts
func (rmq *ReceiveMQ) Validate(list *[]error, services interface{}, path string) {
	rmq.validate(list, &path, "ReceiveMQ")
	rmq.validateNonNull(list, path, "ReceiveMQ", "ConfigurationKey", rmq.ConfigurationKey)
}
