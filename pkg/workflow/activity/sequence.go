package activity

import (
	"fmt"
	"strconv"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Sequence is a standard Runner activity
// that runs all its children in sequence and stops on the
// first error it receives
type Sequence struct {
	DisplayName string
	Activities  []workflow.Runner
}

// Run runs all Activities in slice order.
// It stops executing and reports the first
// error it encounters.
func (seq *Sequence) Run(ctx *workflow.Context) error {
	ctx.Logf("%T %s(%d)", seq, seq.DisplayName, len(seq.Activities))
	for _, r := range seq.Activities {
		if err := r.Run(ctx); err != nil {
			return err
		}
	}
	return nil
}

// Decode decodes the XAML properties of a Sequence activity
func (seq *Sequence) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(seq)
	ctx.Attribute[Xname("", "DisplayName")] = func(v interface{}) error {
		vv := v.(string)
		seq.DisplayName = vv
		return nil
	}
	ctx.Children = func(v interface{}) error {
		if vv, ok := v.(workflow.Runner); ok {
			seq.Activities = append(seq.Activities, vv)
			return nil
		}
		return fmt.Errorf("%T is no Runner for %T's children", v, seq)
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (seq *Sequence) Validate(list *[]error, services interface{}, path string) {
	path += "/" + seq.DisplayName + "#"
	for i, a := range seq.Activities {
		workflow.TryValidate(list, a, services, path, strconv.Itoa(i+1))
	}
}
