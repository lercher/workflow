package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// HistoryBack is a Runner activity that instructs the host
// to lookup the last proper state of the workflow instance and then
// requests the the host to change to that state on idle
type HistoryBack struct {
	Common
}

// Run executes the HistoryBack activity
func (hb *HistoryBack) Run(ctx *workflow.Context) error {
	if !hb.Enabled {
		return nil
	}
	ctx.Logf("%T %s", hb, hb.DisplayName)

	if ls, ok := ctx.RuntimeServices.(LastStater); ok {
		// History back must be implemented by the host, b/c it can have a different
		// understanding of what the last active state was
		last := ls.LastState()
		ctx.NextState = last
	}
	return nil
}

// Decode decodes the XAML properties of a HistoryBack activity
func (hb *HistoryBack) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(hb)
	hb.CommonAttribs(ctx)
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (hb *HistoryBack) Validate(list *[]error, services interface{}, path string) {
	hb.validate(list, &path, "HistoryBack")
	if _, ok := services.(LastStater); !ok {
		serviceError(list, services, hb, "LastStater")
	}
}

// LastStater needs to be implemented by a runtime service for HistoryBack
type LastStater interface {
	LastState() workflow.Statename
}
