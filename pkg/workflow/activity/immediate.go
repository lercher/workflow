package activity

import (
	"gitlab.com/lercher/workflow/pkg/workflow"
)

// <ImmediateActivity [Description="" DisplayName="Init Abgelehnt" Enabled="True"]>

// Immediate is a Triggerable activity that is in the signaled state immediatly.
// An Immediate can be placed in a Transition or a PickBranch of a State to have
// the engine process the consequences immediatly.
type Immediate struct {
	Common
}

// Decode decodes XAML standard properties for an Immediate activity
func (i *Immediate) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(i)
	i.CommonAttribs(ctx)
	return ctx.Continue()
}

// Trigger signals engine and inspect, by what SignalID I want to be triggered
func (i *Immediate) Trigger(ins workflow.Instance) workflow.TriggerInfo {
	ti := workflow.NewTriggerInfo(workflow.TriggerKindImmediate, i.DisplayName, "*NOW*", i.Enabled)
	ti.WakeUp = ins.StateEntered
	return ti
}

// Validate validates properties and required services type casts
func (i *Immediate) Validate(list *[]error, services interface{}, path string) {
	i.validate(list, &path, "Immediate")
}
