package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// PopupMessage is a Runner activity that instructs
// the host to show a popup message in the UI of the
// workflow instance's associated data
type PopupMessage struct {
	Common
	Message string
}

// Run executes the PopupMessage activity
func (pm *PopupMessage) Run(ctx *workflow.Context) error {
	if !pm.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %q", pm, pm.DisplayName, pm.Message)
	if m, ok := ctx.RuntimeServices.(PopupMessager); ok {
		m.PopupMessage(pm.Message)
	}
	return nil
}

// Decode decodes the XAML properties of a PopupMessage activity
func (pm *PopupMessage) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(pm)
	pm.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "Message")] = func(v interface{}) error {
		vv := v.(string)
		pm.Message = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (pm *PopupMessage) Validate(list *[]error, services interface{}, path string) {
	pm.validate(list, &path, "PopupMessage")
	pm.validateNonNull(list, path, "PopupMessage", "Message", pm.Message)
	if _, ok := services.(PopupMessager); !ok {
		serviceError(list, services, pm, "PopupMessager")
	}
}

// PopupMessager needs to be implemented by a runtime service
type PopupMessager interface {
	PopupMessage(string)
}
