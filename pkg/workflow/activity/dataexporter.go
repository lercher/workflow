package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// <DataExporterActivity [Description="Attachement Export" Destination="TextToArchivJob" DisplayName="TextToArchivJobExport" Enabled="True"]>

// DataExporter is a Runner activity
// that instructs the host to export associated data
// to a Destination
type DataExporter struct {
	Common
	Destination string
}

// Run executes a DataExporter activity
func (de *DataExporter) Run(ctx *workflow.Context) error {
	if !de.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %q", de, de.DisplayName, de.Destination)
	if e, ok := ctx.RuntimeServices.(Exporter); ok {
		return e.Export(de.Destination)
	}
	return nil
}

// Decode decodes the XAML properties of a DataExporter activity
func (de *DataExporter) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(de)
	de.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "Destination")] = func(v interface{}) error {
		vv := v.(string)
		de.Destination = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (de *DataExporter) Validate(list *[]error, services interface{}, path string) {
	de.validate(list, &path, "DataExporter")
	de.validateNonNull(list, path, "DataExporter", "Destination", de.Destination)
	if _, ok := services.(Exporter); !ok {
		serviceError(list, services, de, "Exporter")
	}
}

// Exporter needs to be implemented by a runtime service
type Exporter interface {
	Export(string) error
}
