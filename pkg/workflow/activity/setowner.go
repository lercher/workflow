package activity

import "gitlab.com/lercher/workflow/pkg/workflow"

// SetOwner is an activity that instructs the host
// to immediatly change the owner property of the workflow
// instance's associated data
type SetOwner struct {
	Common
	NewOwner string
}

// Run executes the SetOwner activity
func (so *SetOwner) Run(ctx *workflow.Context) error {
	if !so.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s", so, so.DisplayName, so.NewOwner)
	if o, ok := ctx.RuntimeServices.(Owner); ok {
		o.SetOwner(so.NewOwner)
	}
	return nil
}

// Decode decodes the XAML properties of a SetOwner activity
func (so *SetOwner) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(so)
	so.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "NewOwner")] = func(v interface{}) error {
		vv := v.(string)
		so.NewOwner = vv
		return nil
	}
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (so *SetOwner) Validate(list *[]error, services interface{}, path string) {
	so.validate(list, &path, "SetOwner")
	so.validateNonNull(list, path, "SetOwner", "NewOwner", so.NewOwner)
	if _, ok := services.(Owner); !ok {
		serviceError(list, services, so, "Owner")
	}
}

// Owner must be implemented by a runtime service
type Owner interface {
	SetOwner(string)
}
