package activity

import (
	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Command is the main trigger activity.
// It just waits for a signal to be raised by the host.
type Command struct {
	Common
	// CommandKey is the signals name
	CommandKey string
	// CommandRoles indicates to the host, which application roles are allowed to send the signal
	CommandRoles string
}

// Decode decodes a Command from a XAML stream
func (cmd *Command) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(cmd)
	cmd.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "CommandKey")] = func(v interface{}) error {
		vv := v.(string)
		cmd.CommandKey = vv
		return nil
	}
	ctx.Attribute[Xname("", "CommandRoles")] = func(v interface{}) error {
		vv := v.(string)
		cmd.CommandRoles = vv
		return nil
	}
	return ctx.Continue()
}

// Trigger signals engine and inspect, by what SignalID I want to be triggered
func (cmd *Command) Trigger(i workflow.Instance) workflow.TriggerInfo {
	return workflow.NewTriggerInfo(workflow.TriggerKindAwait, cmd.CommandKey, cmd.DisplayName, cmd.Enabled)
}

// Validate validates properties and required services type casts
func (cmd *Command) Validate(list *[]error, services interface{}, path string) {
	cmd.validate(list, &path, "Command")
	cmd.validateNonNull(list, path, "Command", "CommandKey", cmd.CommandKey)
}
