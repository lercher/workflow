package activity

import (
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// TransactMQ is a Runner activity that instructs the host
// to initiate an async request response communication
// described by ConfigurationKey (aka channel). If the host decides to
// return the requested data marked immediatly (i.e. normaly within
// some short timeout) the TransactMQ activity resumes
// execution with the ImmediateResult Runner. If the host
// decides to not present data immediatly the TransactMQ activity resumes
// execution with the DelayedResult Runner.
// Normally a TransactMQ must be paired with a ReceiveMQ activity
// with the same ConfigurationKey placed in the workflow's _Global
// state, to react properly when the response finally arrives.
type TransactMQ struct {
	Common
	ConfigurationKey string
	ImmediateResult  workflow.Runner
	DelayedResult    workflow.Runner
}

// Run executes the TransactMQ activity, if the host satisfies the
// request response within a short timeout it runs ImmediateResult
// and DelayedResult otherwise. It is completly up to the host
// to decide what a response "in-time" means.
func (tmq *TransactMQ) Run(ctx *workflow.Context) error {
	if !tmq.Enabled {
		return nil
	}
	ctx.Logf("%T %s: %s", tmq, tmq.DisplayName, tmq.ConfigurationKey)
	// TODO: cast and call runtime service
	// Then either run Immediate or Delayed
	inTime := true
	res := tmq.ImmediateResult
	if !inTime {
		res = tmq.DelayedResult
	}
	if res != nil {
		return res.Run(ctx)
	}
	return nil
}

// Decode decodes the XAML properties of a TransactMQ activity
func (tmq *TransactMQ) Decode(ld *workflow.Loader) error {
	ctx := ld.NewContext(tmq)
	tmq.CommonAttribs(ctx)
	ctx.Attribute[Xname("", "ConfigurationKey")] = func(v interface{}) error {
		vv := v.(string)
		tmq.ConfigurationKey = vv
		return nil
	}
	ctx.XamlProperty(XmlnsLwa, "TransactMQActivity", "ImmediateResult", func(v interface{}) error {
		if vv, ok := v.(workflow.Runner); ok {
			tmq.ImmediateResult = vv
			return nil
		}
		return fmt.Errorf("%T is no Runner for %T's ImmediateResult", v, tmq)
	})
	ctx.XamlProperty(XmlnsLwa, "TransactMQActivity", "DelayedResult", func(v interface{}) error {
		if vv, ok := v.(workflow.Runner); ok {
			tmq.DelayedResult = vv
			return nil
		}
		return fmt.Errorf("%T is no Runner for %T's DelayedResult", v, tmq)
	})
	return ctx.Continue()
}

// Validate validates properties and required services type casts
func (tmq *TransactMQ) Validate(list *[]error, services interface{}, path string) {
	tmq.validate(list, &path, "TransactMQ")
	tmq.validateNonNull(list, path, "TransactMQ", "ConfigurationKey", tmq.ConfigurationKey)
	path += "/" + tmq.DisplayName
	workflow.TryValidate(list, tmq.ImmediateResult, services, path, ".ImmediateResult")
	workflow.TryValidate(list, tmq.DelayedResult, services, path, ".DelayedResult")
}
