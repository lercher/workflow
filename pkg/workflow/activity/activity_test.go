package activity_test

import (
	"testing"

	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/activity"
	"gitlab.com/lercher/workflow/pkg/workflow/inspect"
)

// these tests won't compile if the interfaces are not implemented properly
// they don't really test something while running

func TestVisiter(t *testing.T) {
	_ = []inspect.Visiter{
		new(activity.Pick),
	}
}

func TestTriggerable(t *testing.T) {
	_ = []workflow.Triggerable{
		new(activity.Command),
		new(activity.Immediate),
		new(activity.Delay),
		new(activity.Every),
		new(activity.ReceiveMQ),
	}
}

func TestRunner(t *testing.T) {
	_ = []workflow.Runner{
		new(activity.AddComment),
		new(activity.AttachmentsExporter),
		new(activity.CreateRelatedItem),
		new(activity.DataExporter),
		new(activity.ExecuteRuleset),
		new(activity.HistoryBack),
		new(activity.LookupExternalData),
		new(activity.NewState),
		new(activity.Pick),
		new(activity.PopupMessage),
		new(activity.RecordStatistics),
		new(activity.SendTextMail),
		new(activity.Sequence),
		new(activity.SetOwner),
		new(activity.SetTab),
		new(activity.TransactMQ),
	}
}

func TestValidater(t *testing.T) {
	_ = []workflow.Validater{
		new(activity.Pick),
		new(activity.PickBranch),

		new(activity.Command),
		new(activity.Immediate),
		new(activity.Delay),
		new(activity.Every),
		new(activity.ReceiveMQ),

		new(activity.AddComment),
		new(activity.AttachmentsExporter),
		new(activity.CreateRelatedItem),
		new(activity.DataExporter),
		new(activity.ExecuteRuleset),
		new(activity.HistoryBack),
		new(activity.LookupExternalData),
		new(activity.NewState),
		new(activity.Pick),
		new(activity.PopupMessage),
		new(activity.RecordStatistics),
		new(activity.SendTextMail),
		new(activity.Sequence),
		new(activity.SetOwner),
		new(activity.SetTab),
		new(activity.TransactMQ),
	}
}
