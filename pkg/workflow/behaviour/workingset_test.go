package behaviour

import (
	"fmt"
	"strings"
	"testing"
)

func Test_workingset_mapThrough(t *testing.T) {
	ws := workingset{
		Data:    newRawJSON(`{"A": "a", "One": 1}`),
		Ruleset: "rule",
		Action:  "param",
	}
	wsnew, err := ws.mapThrough(func(s string) string {
		t.Log(s)
		s = strings.ReplaceAll(s, `"A"`, `"BBB"`)
		s = strings.ReplaceAll(s, `"a"`, `"bbb"`)
		t.Log(s)
		return s
	})
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprint(*wsnew)
	want := `{{"BBB":"bbb","One":1} rule param map[] map[] map[] map[]}`
	if got != want {
		t.Errorf("want %s, got %s", want, got)
	}
}
