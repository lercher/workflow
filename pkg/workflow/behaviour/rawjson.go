package behaviour

// rawjson has a custom marshaller that just keeps it's content as is.
// The json package compacts the bytes after marshalling however.
// It is usefull, if you want to keep pre-marshalled JSON in a struct's field.
type rawjson struct {
	b []byte
}

func (raw *rawjson) UnmarshalJSON(b []byte) error {
	raw.b = make([]byte, len(b))
	copy(raw.b, b)
	return nil
}

func (raw rawjson) MarshalJSON() ([]byte, error) {
	return raw.b, nil
}

func (raw rawjson) String() string {
	return string(raw.b)
}

func newRawJSON(s string) rawjson {
	return rawjson{
		b: []byte(s),
	}
}
