package behaviour

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/lercher/workflow/pkg/script"
	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Behaviour implements standard and lwa activity's
// run behaviour
type Behaviour struct {
	EnableErrors  bool
	Owner         string
	Tab           string
	PopupMessages []TimedLine
	Comments      []TimedLine
	LastStateName workflow.Statename
	VM            *script.VM
}

// New creates a new standard Behaviour
func New(enableErrors bool, vm *script.VM) *Behaviour {
	return &Behaviour{
		EnableErrors: enableErrors,
		VM:           vm,
	}
}

// SetTab sets Tab
func (b *Behaviour) SetTab(s string) {
	b.Tab = s
}

// SetOwner sets Owner
func (b *Behaviour) SetOwner(s string) {
	b.Owner = s
}

// PopupMessage adds a line to PopupMessages
func (b *Behaviour) PopupMessage(s string) {
	s = strings.TrimSpace(s)
	if s != "" {
		b.PopupMessages = append(b.PopupMessages, TimedLine{At: time.Now(), Line: s})
	}
}

// Comment adds a line to Comments
func (b *Behaviour) Comment(s string) {
	s = strings.TrimSpace(s)
	if s != "" {
		b.Comments = append(b.Comments, TimedLine{At: time.Now(), Line: s})
	}
}

// LastState returns LastStateName that needs to be managed during processing
func (b *Behaviour) LastState() workflow.Statename {
	return b.LastStateName
}

// --------------------------------------------------------------------------------------

// SendMail needs to be implemented
func (b *Behaviour) SendMail(emailmode, transformkey string) error {
	if !b.EnableErrors {
		return nil
	}
	return fmt.Errorf("can't send %q %s-ly", transformkey, emailmode)
}

// Lookup needs to be implemented
func (b *Behaviour) Lookup(key, param string) error {
	if !b.EnableErrors {
		return nil
	}
	return fmt.Errorf("can't lookup %q with param %q", key, param)
}

// ExecuteRuleset needs to be implemented, e.g. by running a JavaScript function over JSON data
func (b *Behaviour) ExecuteRuleset(key, param string) error {
	if b.VM == nil {
		if !b.EnableErrors {
			return nil
		}
		return fmt.Errorf("can't execute ruleset %q with param %q without a VM", key, param)
	}

	rule := b.VM.Runtime.Get("$rule").Export()
	if rule == nil {
		return errors.New("global function $rule not found")
	}
	rulefunc, ok := rule.(func(string) string)
	if !ok {
		return fmt.Errorf("$rule is not string -> string but %T", rulefunc)
	}
	ws := workingset{
		Data:    newRawJSON("{}"),
		Ruleset: key,
		Action:  param,
	}
	wsnew, err := ws.mapThrough(rulefunc)
	if err != nil {
		return err
	}
	_ = wsnew // TODO
	return nil
}

// Export needs to be implemented, it should export "the data" to a file, a message queue or somewhere else as specified by the configuration
func (b *Behaviour) Export(key string) error {
	if !b.EnableErrors {
		return nil
	}
	return fmt.Errorf("can't export data by %q", key)
}

// AttsExport needs to be implemented, it should export binary attachments to the workflow as specified by the configuration
func (b *Behaviour) AttsExport(key string) error {
	if !b.EnableErrors {
		return nil
	}
	return fmt.Errorf("can't export attachments by %q", key)
}
