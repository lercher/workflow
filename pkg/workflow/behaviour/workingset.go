package behaviour

import (
	"bytes"
	"encoding/json"
	"strings"
)

/*
interface WorkingSet {
    data : $model
    ruleset: string // from ExecuteRuleset activity
    action: string // from ExecuteRuleset activity
    envelope: ReturnType<typeof $envelope>
    index: ReturnType<typeof $index>
    user: any // current user, its roles and properties
    controller: any // interaction between UI, wf host and model
}
*/

type workingset struct {
	Data       rawjson                `json:"data,omitempty"`
	Ruleset    string                 `json:"ruleset,omitempty"`
	Action     string                 `json:"action,omitempty"`
	Envelope   map[string]interface{} `json:"envelope,omitempty"`
	Index      map[string]interface{} `json:"index,omitempty"`
	User       map[string]interface{} `json:"user,omitempty"`
	Controller map[string]interface{} `json:"controller,omitempty"`
}

func (w *workingset) mapThrough(jsrulefunc func(string) string) (*workingset, error) {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	err := enc.Encode(w)
	if err != nil {
		return nil, err
	}

	processed := jsrulefunc(string(buf.Bytes()))

	rd := strings.NewReader(processed)
	dec := json.NewDecoder(rd)
	var newws workingset
	err = dec.Decode(&newws)
	if err != nil {
		return nil, err
	}

	return &newws, nil
}
