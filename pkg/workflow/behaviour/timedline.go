package behaviour

import (
	"time"
)

// TimedLine is a string with a timetamp
type TimedLine struct {
	At   time.Time `json:"at,omitempty"`
	Line string    `json:"line,omitempty"`
}
