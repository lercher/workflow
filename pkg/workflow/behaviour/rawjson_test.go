package behaviour

import (
	"encoding/json"
	"testing"
)

func Test_rawjson_marshall(t *testing.T) {
	rj := newRawJSON(`{"A":"a","One":1}`)
	b, err := json.Marshal(rj)
	if err != nil {
		t.Fatal(err)
	}
	got := string(b)
	if got != rj.String() {
		t.Errorf("want %v, got %v", rj, got)
	}
}

func Test_rawjson_unmarshall(t *testing.T) {
	raw := `{"A": "a", "One": 1}`
	var rj rawjson
	err := json.Unmarshal([]byte(raw), &rj)
	if err != nil {
		t.Fatal(err)
	}
	got := rj.String()
	want := raw
	if got != want {
		t.Errorf("want %v, got %v", want, got)
	}
}
