package workflow

import (
	"time"
)

// Context stores data for the processing of workflow instances
type Context struct {
	Instance
	NextState            Statename
	NextTrigger          Trigger
	TriggersProcessed    int
	TriggersProcessedMax int
	TriggerData          interface{}
	RuntimeServices      interface{}
	Logf                 func(fmt string, v ...interface{})
	Now                  time.Time
}

// NewContext creates a Context for a particular wf Instance
// that is going to be executed now.
func NewContext(instance Instance, svc interface{}, now time.Time) *Context {
	ctx := &Context{
		Instance:             instance,
		TriggersProcessedMax: 20,
		Logf:                 NullLoggerf,
		Now:                  now,
		RuntimeServices:      svc,
	}
	return ctx
}

// NullLoggerf is the default Logf function for a Context
func NullLoggerf(fmt string, v ...interface{}) {
}

// InstanceDescriptor gets the identity and the next wakeup from a processed workflow
// usually to be returned to a scheduler
func (ctx *Context) InstanceDescriptor() InstanceDescriptor {
	return InstanceDescriptor{
		ID:     ctx.Instance.ID,
		WakeUp: ctx.WakeUp,
	}
}
