package scheduler

import (
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

// Database provides stored wf instances and their wake-up times to the Scheduler
type Database interface {
	// InstancesDue lists all instances that have WakeUps before or equal the given time via the item callback.
	// initial is true on the first query and false on every timeout. item must not be used after this call returned.
	// InstancesDue's return marks the end of the list or an error condition.
	InstancesDue(initial bool, until time.Time, item func(workflow.InstanceDescriptor)) error
}
