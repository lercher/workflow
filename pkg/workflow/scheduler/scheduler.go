package scheduler

import (
	"fmt"
	"sync/atomic"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

const timeOnly = "15:04:05"

// TODO: Need to signal two host specialized signals with data (either a byte slice
// or an interface{}) for serialized read and mutation of associated data
// in the worker queue (reading from Scheduler.Due)

// TODO: database query should get a callback function for
// enumeration instead of returning a slice

// Scheduler coordinates work and wake-ups of
// workflow instances based inside an abstract
// database. It should be Run() in a go routine
// and communicates via channels: Due
// workflows are sent to Due for processing
// and must be sent to Return after processing
// and error handling. It communicates regularly with
// a Database type to load its working set into memory.
// If something gets signaled to a currently processed (i.e. running)
// instance, the signal is queued for the instance and then dequeued and
// sent to Due, as soon as processing sent it back to Return.
type Scheduler struct {
	// Signal is the channel to signal a Trigger to a particular wf instance.
	// A receiver of Due must not send to Signal before returning
	// its ID to Return.
	Signal chan workflow.Signal
	// Due is the channel where the scheduler announces workflows that needs to be processed.
	// It is expected to have some dispatcher attached to this channel. The scheduler guarantees that
	// any wf instance is signaled at most once at any point in time. Any signaled
	// wf instance must be returned to the Scheduler via sendig it to Return after processing.
	Due chan workflow.Signal
	// Return is the channel to return wf instances after they are processed
	Return chan workflow.InstanceDescriptor
	// Close is the channel to signal the scheduler to drain all running instances
	// and to return from the Start method eventually. The value sent is not relevant.
	Close chan bool
	// Logf is a function to log to
	Logf func(fmt string, v ...interface{})
	// Now is a function to get the current time for an operation, defaults to time.Now
	Now            func() time.Time
	database       Database
	planfor        time.Duration
	refreshafter   time.Duration
	running        map[workflow.ID][]workflow.Signal
	runningCount   int64
	scheduled      map[workflow.ID]workflow.InstanceDescriptor
	scheduledCount int64
	lookAhead      time.Time
	lookAheadTimer *timer
	nextQuery      time.Time
	nextEvent      workflow.InstanceDescriptor
	nextEventTimer *timer
}

// New creates a new scheduler that plans workflow
// wakeups for the planfor duration and asks for
// workflow instances after each refreshafter period.
func New(database Database, planfor, refreshafter time.Duration) *Scheduler {
	return &Scheduler{
		Signal:         make(chan workflow.Signal, 0),
		Due:            make(chan workflow.Signal, 0),
		Return:         make(chan workflow.InstanceDescriptor, 0),
		Close:          make(chan bool, 0),
		database:       database,
		planfor:        planfor,
		refreshafter:   refreshafter,
		running:        make(map[workflow.ID][]workflow.Signal),
		scheduled:      make(map[workflow.ID]workflow.InstanceDescriptor),
		nextEvent:      nothingToDo,
		lookAheadTimer: newTimer("look-ahead"),
		nextEventTimer: newTimer("next-event"),
		Logf:           NullLoggerf,
		Now:            time.Now,
	}
}

var nothingToDo = workflow.InstanceDescriptor{WakeUp: workflow.Never}

const minRefresh = 10 * time.Millisecond

// Run instructs the Scheduler to start loading and dispatching due wf instances
func (s *Scheduler) Run() error {
	if s.refreshafter < minRefresh {
		return fmt.Errorf("scheduler refresh-after period must be at least %v, got %v", minRefresh, s.refreshafter)
	}
	if s.refreshafter >= s.planfor {
		return fmt.Errorf("scheduler refresh-after period %v must be less than its plan-for period %v", s.refreshafter, s.planfor)
	}

	s.Logf("initial load from database ...")
	err := s.load(true)
	if err != nil {
		return err
	}
	s.Logf("start dispatch loop ...")
	err = s.dispatchLoop()
	s.Logf("dispatch loop stoped")
	s.updateStats()
	return err
}

func (s *Scheduler) load(initial bool) error {
	now := s.Now()
	s.lookAhead = now.Add(s.planfor)
	s.Logf("looking for instances due until %v", s.lookAhead)
	err := s.database.InstancesDue(initial, s.lookAhead, s.schedule)
	if err != nil {
		return err
	}

	s.reorganizeEventTimer(now)
	s.lookAheadTimer.reset(s.refreshafter)
	return nil
}

func (s *Scheduler) drain() {
	for len(s.running) > 0 {
		s.Logf("draining %d instance(s) ...", len(s.running))
		ii := <-s.Return
		delete(s.running, ii.ID)
		s.Logf("drained %v", ii.ID)
	}
	s.Logf("drained all instances")
	// normally the producer closes, but we have probably many of them.
	// so close the channel, if sth sends on it unexpectedly it's worth the panic
	close(s.Return)
}

// schedule must only be called from the Scheduler loop
func (s *Scheduler) schedule(ii workflow.InstanceDescriptor) {
	if ii.WakeUp.After(s.lookAhead) { // Due
		s.Logf("not scheduled %v, wakeup %v, b/c it is after %v", ii.ID, ii.WakeUp.Format(timeOnly), s.lookAhead.Format(timeOnly))
	} else {
		if _, ok := s.running[ii.ID]; !ok { // and not running
			s.scheduled[ii.ID] = ii
			s.Logf("scheduled %v with wakeup at %v", ii.ID, ii.WakeUp.Format(timeOnly))
		} else {
			s.Logf("not scheduled %v, wakeup %v, b/c it is running currently", ii.ID, ii.WakeUp.Format(timeOnly))
		}
	}
}

func (s *Scheduler) reorganizeEventTimer(now time.Time) {
	s.Logf("looking for next wakeup in %d instance(s)", len(s.scheduled))
	s.nextEvent = nothingToDo
	for _, ii := range s.scheduled {
		if ii.WakeUp.Before(s.nextEvent.WakeUp) {
			s.nextEvent = ii
		}
	}
	if s.nextEvent.WakeUp.Before(workflow.Never) {
		d := s.nextEvent.WakeUp.Sub(now)
		s.Logf("nextEventTimer in %v at %v for %v", d, s.nextEvent.WakeUp.Format(timeOnly), s.nextEvent.ID)
		s.nextEventTimer.reset(d)
	} else {
		s.Logf("no wakeup due, stoped nextEventTimer")
		s.nextEventTimer.stop()
	}
}

func (s *Scheduler) updateStats() {
	atomic.StoreInt64(&s.runningCount, int64(len(s.running)))
	atomic.StoreInt64(&s.scheduledCount, int64(len(s.scheduled)))
}

func (s *Scheduler) dispatchLoop() error {
	for {
		s.updateStats()
		s.Logf("waiting for an event ...")
		select {
		case <-s.lookAheadTimer.tmr.C: // refresh due list from database
			s.lookAheadTimer.drained = true

			err := s.load(false)
			if err != nil {
				// TODO: find some better recovery for a no more working database
				close(s.Due)
				s.drain()
				return err
			}

		case sig := <-s.Signal: // external trigger signaled
			delete(s.scheduled, sig.InstanceDescriptor.ID)
			if list, ok := s.running[sig.InstanceDescriptor.ID]; ok {
				// sig.ID is running currently, so just append to its signal list
				list = append(list, sig)
				s.Logf("received external signal %v for %v which is running, queueing it for Due, %d signal(s) queued ...", sig.Trigger, sig.InstanceDescriptor.ID, len(list))
				s.running[sig.InstanceDescriptor.ID] = list
			} else {
				// sig.ID is not running currently
				s.running[sig.InstanceDescriptor.ID] = nil
				s.Logf("received external signal %v for %v, sending it on Due, %d runnning instance(s) ...", sig.Trigger, sig.InstanceDescriptor.ID, len(s.running))
				s.due(sig)
				s.Logf("sent (external) %v %v", sig.InstanceDescriptor.ID, sig.Trigger)
			}
			now := s.Now()
			s.reorganizeEventTimer(now)

		case <-s.nextEventTimer.tmr.C: // the next timer needs to be processed
			s.nextEventTimer.drained = true

			delete(s.scheduled, s.nextEvent.ID)
			if _, ok := s.running[s.nextEvent.ID]; ok {
				// sig.ID is running currently, so it shouldn't have been triggered by a timer
				// TODO: either we panic here, or we just ignore it
				panic(fmt.Sprint(s.nextEvent.ID, "was triggered by a timer, but it is running currently; is this a bug in scheduler.go?"))
			} else {
				s.running[s.nextEvent.ID] = nil
				s.Logf("nextEventTimer expired, sending NullSignal for %v on Due, %d runnning instance(s) ...", s.nextEvent.ID, len(s.running))
				sig := workflow.NullSignal(s.nextEvent.ID)
				s.due(sig)
				s.Logf("sent (timer expired) %v", s.nextEvent.ID)
			}
			now := s.Now()
			s.reorganizeEventTimer(now)

		case ii := <-s.Return: // a wf instance is processed
			s.done(ii)

		case <-s.Close: // the host wants to shut down
			s.Logf("received Close, closing Due and draining running wf instances ...")
			close(s.Due) // nothing can be Due any more
			s.drain()    // wait for all running wf instances to complete
			return nil
		}
	}
}

// due sends to Due concurrently with processing events on Return.
// We avoid a deadlock, if sending to a goroutine that just wants
// to return its work packet to the scheduler right in that moment.
func (s *Scheduler) due(sig workflow.Signal) {
	for {
		select {
		case s.Due <- sig:
			return
		case ii := <-s.Return:
			s.done(ii)
		}
	}
}

func (s *Scheduler) done(ii workflow.InstanceDescriptor) {
	if list, ok := s.running[ii.ID]; ok {
		if len(list) > 0 {
			dequeued := list[0]
			list = list[1:]
			s.running[ii.ID] = list
			s.Logf("return %v received, dequeued signal %v, %d signal(s) left", ii.ID, dequeued.Trigger, len(list))
			s.due(dequeued)
			s.Logf("sent (dequeued) %v %v", dequeued.InstanceDescriptor.ID, dequeued.Trigger)
			return
		}
		delete(s.running, ii.ID)
		s.Logf("return %v received, next wakeup at %v, %d running instance(s) left", ii.ID, ii.WakeUp.Format(timeOnly), len(s.running))
		s.schedule(ii)
		now := s.Now()
		s.reorganizeEventTimer(now)
		return
	}
	s.Logf("%v was not running, Return<-/done ignored")
}

// RefreshAfter returns the polling period against the Database
func (s *Scheduler) RefreshAfter() time.Duration {
	return s.refreshafter
}

// PlanFor returns the lookahead after Now() of the database
func (s *Scheduler) PlanFor() time.Duration {
	return s.planfor
}

// Database returns the Scheduler's Database interface against which it is polling
func (s *Scheduler) Database() Database {
	return s.database
}

// RunningCount is only a statistical indication of the count
func (s *Scheduler) RunningCount() int {
	return int(atomic.LoadInt64(&s.runningCount))
}

// ScheduledCount is only a statistical indication of the count
func (s *Scheduler) ScheduledCount() int {
	return int(atomic.LoadInt64(&s.scheduledCount))
}

// NullLoggerf is the default Logf function for a Scheduler
func NullLoggerf(fmt string, v ...interface{}) {
}
