package scheduler

import (
	"time"
)

type timer struct {
	tmr     *time.Timer
	drained bool
	name    string
}

func newTimer(name string) *timer {
	tmr := time.NewTimer(time.Hour)
	t := &timer{
		tmr:     tmr,
		drained: false,
		name:    name,
	}
	t.stop()
	return t
}

func (t *timer) reset(d time.Duration) {
	t.stop()
	t.tmr.Reset(d)
	t.drained = false
}

func (t *timer) stop() {
	if !t.tmr.Stop() {
		// the timer was stoped
		if !t.drained {
			// log.Println("need to drain", t.name)
			<-t.tmr.C
		}
	}
	t.drained = true
}
