package scheduler_test

import "testing"

var q = []int{0, 0, 0, 0}

func TestQueueCapIsLimitedWhenOnlyShifted(t *testing.T) {
	// This test is important b/c we use a a similar queue
	// in the scheduler for signals that are sent, while the
	// instance is running currently.
	// It tests that we don't get a mem leak if some instance
	// get so many signals that it never stops running but doesn't pile
	// up signals in the queue, i.e. it has approximatly constant
	// queue length.

	// In this test the cap stays below 10 after 1M shifts
	for i := 0; i < 1000000; i++ {
		q = append(q, i) // queue
		q = q[1:]        // de-queue
	}
	want := 10
	if cap(q) > want {
		t.Error("want q cap below", want, "got", cap(q), "at len", len(q))
	}
}
