package scheduler

import (
	"errors"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

var testNow0 = time.Date(2019, 10, 15, 14, 30, 0, 0, time.UTC)

func testNow() time.Time {
	return testNow0
}

type db struct {
	instances []workflow.InstanceDescriptor
}

func (d db) InstancesDue(initial bool, until time.Time, item func(workflow.InstanceDescriptor)) error {
	for _, ii := range d.instances {
		item(ii)
	}
	return nil
}

type dberrorOnInit bool

func (d dberrorOnInit) InstancesDue(initial bool, until time.Time, item func(workflow.InstanceDescriptor)) error {
	if bool(d) || !initial {
		return errors.New("error")
	}
	return nil
}

func TestStartErrDBInitial(t *testing.T) {
	s := New(dberrorOnInit(true), 10*time.Second, 50*time.Millisecond)
	s.Now = testNow
	err := s.Run()
	if err == nil {
		t.Errorf("want error from init, got nil")
	}
	if err.Error() != "error" {
		t.Errorf("want error, got %v", err)
	}
}

func TestStartErrDBFromLoop(t *testing.T) {
	s := New(dberrorOnInit(false), 10*time.Second, 50*time.Millisecond)
	s.Now = testNow
	t0 := time.Now()
	err := s.Run()
	t1 := time.Now()
	if err == nil {
		t.Errorf("want error from loop, got nil")
	}
	if err.Error() != "error" {
		t.Errorf("want error, got %v", err)
	}
	rt := t1.Sub(t0)
	if rt <= s.RefreshAfter() {
		t.Errorf("runtime must not be under %v but was %v", s.RefreshAfter(), rt)
	}
	if s.RefreshAfter()*2 <= rt {
		t.Errorf("runtime should be around %v (test is %v) but was %v. It can be late under heavy load, so this is probably OK", s.RefreshAfter(), s.RefreshAfter()*2, rt)
	}
}

func TestScheduler(t *testing.T) {
	d := &db{
		[]workflow.InstanceDescriptor{ii(1, testNow0), ii(2, testNow0)},
	}
	s := New(d, 10*time.Second, 5*time.Second)
	s.Now = testNow
	s.Logf = t.Logf

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		err := s.Run()
		if err != nil {
			t.Errorf("Run error: %v", err)
		}
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		st := true
		for sig := range s.Due {
			if st {
				st = false
				s.Return <- workflow.InstanceDescriptor{ID: sig.InstanceDescriptor.ID, WakeUp: workflow.Never}
			} else {
				s.Return <- workflow.InstanceDescriptor{ID: sig.InstanceDescriptor.ID, WakeUp: testNow0.Add(5 * time.Second)}
			}
		}
		wg.Done()
	}()

	time.Sleep(100 * time.Millisecond) // let the Return/s be processed

	s.Close <- true
	wg.Wait()

	rc := s.RunningCount()
	if rc != 0 {
		t.Errorf("want running count 0, got %v", rc)
	}

	sc := s.ScheduledCount()
	if sc != 1 {
		t.Errorf("want scheduled count 1, got %v", sc)
	}
}

func TestDontBlockParallel(t *testing.T) {
	// we have 5 things ready to be processed
	d := &db{
		[]workflow.InstanceDescriptor{
			ii(1, testNow0),
			ii(2, testNow0),
			ii(3, testNow0),
			ii(4, testNow0),
			ii(5, testNow0),
		},
	}
	s := New(d, 10*time.Second, 5*time.Second)
	s.Now = testNow
	s.Logf = t.Logf

	// dispatcher
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		err := s.Run()
		if err != nil {
			t.Errorf("Run error: %v", err)
		}
		wg.Done()
	}()

	// slow processors
	wg.Add(1)
	go func() {
		for sig := range s.Due {
			wg.Add(1)
			go func(sig workflow.Signal) {
				time.Sleep(1 * time.Second)
				s.Return <- workflow.InstanceDescriptor{ID: sig.InstanceDescriptor.ID, WakeUp: testNow0.Add(5 * time.Second)}
				wg.Done()
			}(sig)
		}
		wg.Done()
	}()

	time.Sleep(100 * time.Millisecond) // let the scheduler start some tasks
	s.Close <- true
	wg.Wait()
}

func TestDontBlockSerialized(t *testing.T) {
	// we have 5 things ready to be processed
	d := &db{
		[]workflow.InstanceDescriptor{
			ii(0, testNow0),
			ii(1, testNow0),
			ii(2, testNow0),
			ii(3, testNow0),
			ii(4, testNow0),
		},
	}
	what := make([]int, len(d.instances))
	s := New(d, 10*time.Second, 5*time.Second)
	s.Now = testNow
	s.Logf = t.Logf

	timing := time.Now()
	// dispatcher
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		err := s.Run()
		if err != nil {
			t.Errorf("Run error: %v", err)
		}
		wg.Done()
	}()

	const sleep200 = 200 * time.Millisecond

	// slow processors
	wg.Add(1)
	go func() {
		for sig := range s.Due {
			what[sig.InstanceDescriptor.ID[0]]++
			t.Log("sleeping", sig.InstanceDescriptor.ID)
			time.Sleep(sleep200)
			what[sig.InstanceDescriptor.ID[0]]++
			s.Return <- workflow.InstanceDescriptor{ID: sig.InstanceDescriptor.ID, WakeUp: testNow0.Add(50 * time.Second)}
			what[sig.InstanceDescriptor.ID[0]]++
		}
		wg.Done()
	}()

	time.Sleep(900 * time.Millisecond) // sleep a bit, so the scheduler can do its work
	s.Close <- true
	wg.Wait()
	delta := time.Now().Sub(timing)

	t.Log(delta, what)
	if delta > time.Duration(len(d.instances)+1)*sleep200 {
		t.Errorf("wanted runtime around %d * %v, got %v", len(d.instances), sleep200, delta)
	}
	if delta < time.Duration(len(d.instances)-1)*sleep200 {
		t.Errorf("wanted runtime around %d * %v, got %v", len(d.instances), sleep200, delta)
	}
}

func TestSignalSameWhileRunning(t *testing.T) {
	d1 := &db{
		[]workflow.InstanceDescriptor{
			ii(1, testNow0.Add(time.Second)),
		},
	}
	s := New(d1, 10*time.Second, 5*time.Second)
	s.Now = testNow
	var wg sync.WaitGroup
	var wgSig sync.WaitGroup

	// dispatcher
	wg.Add(1)
	go func() {
		err := s.Run()
		if err != nil {
			t.Errorf("Run error: %v", err)
		}
		wg.Done()
	}()

	// processor
	var parallel, maxparallel int32
	mu := new(sync.Mutex)
	wg.Add(1)
	go func() {
		n := time.Duration(0)
		for sig := range s.Due {
			n++
			go func(sig workflow.Signal, n time.Duration) {
				p := atomic.AddInt32(&parallel, 1)
				mu.Lock()
				if p > maxparallel {
					maxparallel = p
				}
				mu.Unlock()
				t.Log("Processing data", sig.Data, "with parallel", p, "...")
				time.Sleep(100 * time.Millisecond)
				t.Log("Processed data", sig.Data, "- returning due after", n*time.Second)
				atomic.AddInt32(&parallel, -1)
				s.Return <- workflow.InstanceDescriptor{ID: sig.InstanceDescriptor.ID, WakeUp: testNow0.Add(n * time.Second)}
				wgSig.Done()
			}(sig, n)
		}
		wg.Done()
	}()

	t0 := time.Now()
	for i := 0; i < 10; i++ {
		sig := workflow.Signal{
			InstanceDescriptor: d1.instances[0],
			Trigger:            workflow.NullTrigger,
			Data:               []byte{byte(i)},
		}
		wgSig.Add(1)
		t.Log("Signalling", sig)
		s.Signal <- sig
	}

	duration := time.Now().Sub(t0)
	t.Log("After", duration, "Waiting for all to be processed ...")
	if duration > 50*time.Millisecond {
		// the processor should take about 1s, we don't want to couple signalling to it
		// so it must be less
		t.Error("signalling must complete quickly, but it took", duration)
	}

	wgSig.Wait()
	durationSig := time.Now().Sub(t0)
	t.Log("After", durationSig, "all was processed")
	if durationSig < 1000*time.Millisecond {
		t.Error("the processing should take about 1s, but it took only", durationSig, "If that is near 100ms, it means full parallelism")
	}
	if maxparallel != 1 {
		t.Error("max degree of parallelism in processor must be 1, got", maxparallel)
	}

	s.Close <- true
	wg.Wait()
}

func ii(i byte, due time.Time) workflow.InstanceDescriptor {
	return workflow.InstanceDescriptor{
		ID:     workflow.ByteID(i),
		WakeUp: due,
	}
}
