package workflow_test

import (
	"strings"
	"testing"

	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/activity"
	"gitlab.com/lercher/workflow/pkg/workflow/behaviour"
)

func TestLoadSimplifiedWF(t *testing.T) {
	rd := strings.NewReader(simplifiedxaml)
	ld := workflow.NewLoader(rd)
	activity.RegisterAll(ld)
	def, err := ld.Decode()
	if err != nil {
		t.Fatal(err)
	}
	if def == nil {
		t.Fatal("definition nil and err nil")
	}
	if def.InitialState == nil {
		t.Fatal("initial state is nil")
	}
	if def.InitialState.DisplayName != "Init" {
		t.Errorf("InitialState.DisplayName want 'Init' got %q", def.InitialState.DisplayName)
	}
	if len(def.State) != 3 {
		t.Errorf("want 3 states, got %d", len(def.State))
	}
	t.Log(def)

	errs := def.Validate(behaviour.New(false, nil))
	if len(errs) > 0 {
		t.Errorf("got validation errors %v", errs)
	}
}

func TestLoadSimpleWF(t *testing.T) {
	rd := strings.NewReader(simplexml)
	ld := workflow.NewLoader(rd)
	activity.RegisterAll(ld)
	def, err := ld.Decode()
	if err != nil {
		t.Fatal(err)
	}
	if def == nil {
		t.Fatal("definition nil and err nil")
	}
	if def.InitialState == nil {
		t.Fatal("initial state is nil")
	}
	if def.InitialState.DisplayName != "Init" {
		t.Errorf("InitialState.DisplayName want 'Init' got %q", def.InitialState.DisplayName)
	}
	if len(def.State) != 12 {
		t.Errorf("want 12 states, got %d", len(def.State))
	}
	ds := def.String()
	// t.Log(ds)
	if ds != simple {
		t.Errorf("want\n%s\nas string, got\n%s", simple, ds)
	}

	errs := def.Validate(behaviour.New(false, nil))
	if len(errs) > 0 {
		t.Errorf("got validation errors %v", errs)
	}
}

const simple = `Definition {
  State "Abgelehnt" { "InitAbgelehnt" -> _Beenden }
  State "Angebot" { "T0" -> StornoContainer  "AngebotTransition1" -> StornoContainer  "AngebotTransition2" -> StornoContainer  "AngebotTransition3" -> StornoContainer  "AntragEvent" -> Antrag }
  State "Antrag" { "T1" -> StornoContainer  "SendenEvent" -> Genehmigung  "ÄndernEvent" -> Angebot }
  State "FUS" { "InitFUS" -> _Beenden }
  State "Genehmigung" { "T2" -> StornoContainer  "ÄndernEvent2" -> Angebot  "AblehnenEvent" -> Abgelehnt  "GenehmigenEvent" -> FUS }
  -> State "Init" { "Initialisierung" -> Angebot }
  State "Storno" { "InitStorno" -> _Beenden }
  State "StornoContainer" { "StornoEvent" -> Stornoauswahl }
  State "Stornoauswahl" { "ZurückEvent" -> Angebot  "StornoOkEvent" -> Storno }
  State "_Beenden" { "BeendenDurchführen" -> _Beendet }
  Final State "_Beendet" {}
  State "_Global" {}
}`

const simplifiedxaml = `
<Activity x:Class="{x:Null}" 
    xmlns="http://schemas.microsoft.com/netfx/2009/xaml/activities" 
    xmlns:lwa="clr-namespace:Lercher.Workflow.Activities;assembly=Lercher.Workflow.Activities" 
    xmlns:scg="clr-namespace:System.Collections.Generic;assembly=mscorlib" 
    xmlns:sco="clr-namespace:System.Collections.ObjectModel;assembly=mscorlib" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
  >
  <StateMachine InitialState="{x:Reference __ReferenceID10}">
    <State DisplayName="_Global">
      <State.Exit>
        <Pick DisplayName="Global Events">
          <PickBranch DisplayName="UploadEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Upload" CommandRoles="*" Description="" DisplayName="Anhang Upload" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="UploadEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="UploadRules" Enabled="True" RulesetName="#Upload" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="DatenErhaltenEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Receive" CommandRoles="*" Description="" DisplayName="Daten geändert" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="DatenErhaltenEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="GlobalRegeln" Enabled="True" RulesetName="#Global" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ChecksRegeln" Enabled="True" RulesetName="Checks" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="CommandsBeforeRules" Enabled="True" RulesetName="#CommandsBefore" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="RechnenRules" Enabled="True" RulesetName="Rechnen" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="CommandsPostRules" Enabled="True" RulesetName="#CommandsAfter" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="TexteRegeln1" Enabled="True" RulesetName="Texte" />
            </Sequence>
          </PickBranch>
        </Pick>
      </State.Exit>
    </State>
    <State x:Name="__ReferenceID0" DisplayName="StornoContainer">
      <State.Transitions>
      </State.Transitions>
    </State>
    <State x:Name="__ReferenceID10" DisplayName="Init">
      <State.Entry>
        <Sequence DisplayName="Initialisierung body">
          <lwa:ExecuteRulesetActivity Action="" Description="Daten initialisieren, Konstanten naschlagen, etc." DisplayName="SetupRegeln" Enabled="True" RulesetName="Setup" />
        </Sequence>
      </State.Entry>
      <State.Transitions>
      </State.Transitions>
    </State>
    <x:Reference>__ReferenceID10</x:Reference>
  </StateMachine>
</Activity>`

const simplexml = `
<Activity x:Class="{x:Null}" xmlns="http://schemas.microsoft.com/netfx/2009/xaml/activities" xmlns:lwa="clr-namespace:Lercher.Workflow.Activities;assembly=Lercher.Workflow.Activities" xmlns:scg="clr-namespace:System.Collections.Generic;assembly=mscorlib" xmlns:sco="clr-namespace:System.Collections.ObjectModel;assembly=mscorlib" xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml">
  <TextExpression.NamespacesForImplementation>
    <sco:Collection x:TypeArguments="x:String">
      <x:String>System</x:String>
      <x:String>System.Collections.Generic</x:String>
      <x:String>System.Data</x:String>
      <x:String>System.Linq</x:String>
      <x:String>System.Text</x:String>
    </sco:Collection>
  </TextExpression.NamespacesForImplementation>
  <TextExpression.ReferencesForImplementation>
    <scg:List x:TypeArguments="AssemblyReference" Capacity="16">
      <AssemblyReference>Microsoft.CSharp</AssemblyReference>
      <AssemblyReference>System</AssemblyReference>
      <AssemblyReference>System.Activities</AssemblyReference>
      <AssemblyReference>System.Core</AssemblyReference>
      <AssemblyReference>System.Data</AssemblyReference>
      <AssemblyReference>System.Runtime.Serialization</AssemblyReference>
      <AssemblyReference>System.ServiceModel</AssemblyReference>
      <AssemblyReference>System.ServiceModel.Activities</AssemblyReference>
      <AssemblyReference>System.Xaml</AssemblyReference>
      <AssemblyReference>System.Xml</AssemblyReference>
      <AssemblyReference>System.Xml.Linq</AssemblyReference>
      <AssemblyReference>mscorlib</AssemblyReference>
      <AssemblyReference>CasFiniteStateSample</AssemblyReference>
    </scg:List>
  </TextExpression.ReferencesForImplementation>
  <StateMachine InitialState="{x:Reference __ReferenceID10}">
    <State DisplayName="_Global">
      <State.Exit>
        <Pick DisplayName="Global Events">
          <PickBranch DisplayName="UploadEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Upload" CommandRoles="*" Description="" DisplayName="Anhang Upload" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="UploadEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="UploadRules" Enabled="True" RulesetName="#Upload" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="DatenErhaltenEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Receive" CommandRoles="*" Description="" DisplayName="Daten geändert" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="DatenErhaltenEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="GlobalRegeln" Enabled="True" RulesetName="#Global" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ChecksRegeln" Enabled="True" RulesetName="Checks" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="CommandsBeforeRules" Enabled="True" RulesetName="#CommandsBefore" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="RechnenRules" Enabled="True" RulesetName="Rechnen" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="CommandsPostRules" Enabled="True" RulesetName="#CommandsAfter" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="TexteRegeln1" Enabled="True" RulesetName="Texte" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="AfterCommandEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#AfterCommand" CommandRoles="*" Description="Wird nach einem PostCommand Event ausgeführt, egal ob Kommando oder nicht" DisplayName="AfterCommand" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="AfterCommandEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="TexteRegeln2" Enabled="True" RulesetName="Texte" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="InterceptEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Intercept" CommandRoles="*" Description="" DisplayName="Mehr Messages" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="InterceptEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="InterceptRegeln" Enabled="True" RulesetName="#Intercept" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="AdminEvent">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#Admin" CommandRoles="*" Description="" DisplayName="Admin Aktion" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="AdminEvent body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="AdminRegeln" Enabled="True" RulesetName="#Admin" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="TextToArchivJob">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#TextToArchivJob" CommandRoles="##Alle" Description="" DisplayName="AttExport" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="TextToArchivJob body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ArchivTextJobRules" Enabled="True" RulesetName="ArchivTextJob" />
              <lwa:DataExporterActivity Description="Attachement Export" Destination="TextToArchivJob" DisplayName="TextToArchivJobExport" Enabled="True" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ArchivTextToReportListRules" Enabled="True" RulesetName="ArchivTextToReportList" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="EmailKunde">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#EmailKunde" CommandRoles="##Alle" Description="" DisplayName="" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="EmailKunde body">
              <lwa:SendTextMailActivity Description="Email Kunden" DisplayName="sendEmailAnschreiben" EmailMode="Interactive" Enabled="True" XSLTransformKey="Email_Kunde" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ArchivNachMailversandRules" Enabled="True" RulesetName="ArchivNachMailversand" />
            </Sequence>
          </PickBranch>
          <PickBranch DisplayName="AttToArchivJob">
            <PickBranch.Trigger>
              <lwa:CommandActivity CommandKey="#AttToArchivJob" CommandRoles="##Alle" Description="" DisplayName="AttExport" Enabled="True" />
            </PickBranch.Trigger>
            <Sequence DisplayName="AttToArchivJob body">
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ArchivAttachmentJobRules" Enabled="True" RulesetName="ArchivAttachmentJob" />
              <lwa:DataExporterActivity Description="Attachement Export" Destination="AttToArchivJob" DisplayName="AttToArchivJobExport" Enabled="True" />
              <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="ArchivAttachmentDoneRules" Enabled="True" RulesetName="ArchivAttachmentDone" />
            </Sequence>
          </PickBranch>
        </Pick>
      </State.Exit>
    </State>
    <State x:Name="__ReferenceID0" DisplayName="StornoContainer">
      <State.Transitions>
        <Transition DisplayName="StornoEvent">
          <Transition.Trigger>
            <lwa:CommandActivity CommandKey="?Stornieren" CommandRoles="##Alle" Description="" DisplayName="Stornieren" Enabled="True" />
          </Transition.Trigger>
          <Transition.To>
            <State x:Name="__ReferenceID6" DisplayName="Stornoauswahl">
              <State.Entry>
                <Sequence DisplayName="InitStornoauswahl body">
                  <lwa:SetTabActivity Description="" DisplayName="AntragTabStornoauswahl" Enabled="True" NewTab="100ANTRAG" />
                  <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="StornoauswahlRules" Enabled="True" RulesetName="Stornoauswahl" />
                </Sequence>
              </State.Entry>
              <State.Transitions>
                <Transition DisplayName="ZurückEvent">
                  <Transition.Trigger>
                    <lwa:CommandActivity CommandKey="BackFromStorno" CommandRoles="##Alle" Description="" DisplayName="Zurück zu Angebot" Enabled="True" />
                  </Transition.Trigger>
                  <Transition.To>
                    <State x:Name="__ReferenceID1" DisplayName="Angebot">
                      <State.Entry>
                        <Sequence DisplayName="InitAngebot body">
                          <lwa:SetTabActivity Description="" DisplayName="AnsichtAntrag" Enabled="True" NewTab="100ANTRAG" />
                          <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="AngebotRules" Enabled="True" RulesetName="Angebot" />
                        </Sequence>
                      </State.Entry>
                      <State.Transitions>
                        <Transition DisplayName="T0" To="{x:Reference __ReferenceID0}" />
<Transition DisplayName="AngebotTransition1" To="{x:Reference __ReferenceID0}" />
<Transition DisplayName="AngebotTransition2" To="{x:Reference __ReferenceID0}" />
<Transition DisplayName="AngebotTransition3" To="{x:Reference __ReferenceID0}" />
                        <Transition DisplayName="AntragEvent">
                          <Transition.Trigger>
                            <lwa:CommandActivity CommandKey="Antrag" CommandRoles="##Alle" Description="" DisplayName="Antrag" Enabled="True" />
                          </Transition.Trigger>
                          <Transition.To>
                            <State x:Name="__ReferenceID8" DisplayName="Antrag">
                              <State.Entry>
                                <Sequence DisplayName="InitAntrag body">
                                  <lwa:SetTabActivity Description="" DisplayName="AnsichtAlle" Enabled="True" NewTab="900ALL" />
                                  <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="AntragRules" Enabled="True" RulesetName="Antrag" />
                                </Sequence>
                              </State.Entry>
                              <State.Transitions>
                                <Transition DisplayName="T1" To="{x:Reference __ReferenceID0}" />
                                <Transition DisplayName="SendenEvent">
                                  <Transition.Trigger>
                                    <lwa:CommandActivity CommandKey="Send" CommandRoles="##Alle" Description="" DisplayName="Abschicken" Enabled="True" />
                                  </Transition.Trigger>
                                  <Transition.To>
                                    <State x:Name="__ReferenceID9" DisplayName="Genehmigung">
                                      <State.Entry>
                                        <Sequence DisplayName="InitGenehmigung body">
                                          <lwa:SetTabActivity Description="" DisplayName="SetTabUnterlagen" Enabled="True" NewTab="700ATT" />
                                          <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="GenehmigungRules" Enabled="True" RulesetName="Genehmigung" />
                                        </Sequence>
                                      </State.Entry>
                                      <State.Transitions>
                                        <Transition DisplayName="T2" To="{x:Reference __ReferenceID0}" />
                                        <Transition DisplayName="ÄndernEvent2">
                                          <Transition.Trigger>
                                            <lwa:CommandActivity CommandKey="ChangeGen" CommandRoles="##Alle" Description="" DisplayName="Ändern" Enabled="True" />
                                          </Transition.Trigger>
                                          <Transition.To>
                                            <x:Reference>__ReferenceID1</x:Reference>
                                          </Transition.To>
                                        </Transition>
                                        <Transition DisplayName="AblehnenEvent">
                                          <Transition.Trigger>
                                            <lwa:CommandActivity CommandKey="?Ablehnen" CommandRoles="##Alle" Description="" DisplayName="Ablehnen" Enabled="True" />
                                          </Transition.Trigger>
                                          <Transition.To>
                                            <State x:Name="__ReferenceID5" DisplayName="Abgelehnt">
                                              <State.Entry>
                                                <Sequence DisplayName="InitAbgelehnt body">
                                                  <lwa:SetTabActivity Description="" DisplayName="TimelineTabAbg" Enabled="True" NewTab="800TIMELINE" />
                                                  <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="AbgelehntRules" Enabled="True" RulesetName="Abgelehnt" />
                                                  <lwa:DataExporterActivity Description="Daten Ins Dateisystem schreiben" Destination="RawFile" DisplayName="RawExportAbg" Enabled="True" />
                                                  <lwa:SendTextMailActivity Description="" DisplayName="AbgelehntEmail" EmailMode="Immediate" Enabled="True" XSLTransformKey="EMail_Abgelehnt" />
                                                </Sequence>
                                              </State.Entry>
                                              <State.Transitions>
                                                <Transition DisplayName="InitAbgelehnt">
                                                  <Transition.Trigger>
                                                    <lwa:ImmediateActivity Description="" DisplayName="Init Abgelehnt" Enabled="True" />
                                                  </Transition.Trigger>
                                                  <Transition.To>
                                                    <State x:Name="__ReferenceID2" DisplayName="_Beenden">
                                                      <State.Entry>
                                                        <Sequence DisplayName="BeendenDurchführen body">
                                                          <lwa:ExecuteRulesetActivity Action="{x:Null}" Description="" DisplayName="BeendetRules" Enabled="True" RulesetName="Beendet" />
                                                        </Sequence>
                                                      </State.Entry>
                                                      <State.Transitions>
                                                        <Transition DisplayName="BeendenDurchführen">
                                                          <Transition.Trigger>
                                                            <lwa:ImmediateActivity Description="" DisplayName="Init _Beenden" Enabled="True" />
                                                          </Transition.Trigger>
                                                          <Transition.To>
                                                            <State x:Name="__ReferenceID7" DisplayName="_Beendet" IsFinal="True" />
                                                          </Transition.To>
                                                        </Transition>
                                                      </State.Transitions>
                                                    </State>
                                                  </Transition.To>
                                                </Transition>
                                              </State.Transitions>
                                            </State>
                                          </Transition.To>
                                        </Transition>
                                        <Transition DisplayName="GenehmigenEvent">
                                          <Transition.Trigger>
                                            <lwa:CommandActivity CommandKey="?Genehmigen" CommandRoles="##Alle" Description="" DisplayName="Genehmigen" Enabled="True" />
                                          </Transition.Trigger>
                                          <Transition.To>
                                            <State x:Name="__ReferenceID3" DisplayName="FUS">
                                              <State.Entry>
                                                <Sequence DisplayName="InitFUS body">
                                                  <lwa:SetTabActivity Description="" DisplayName="TimelineTabFUS" Enabled="True" NewTab="800TIMELINE" />
                                                  <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="FUSRules" Enabled="True" RulesetName="FUS" />
                                                  <lwa:DataExporterActivity Description="Daten an ls.pro senden" Destination="sendFUS" DisplayName="ExportPro" Enabled="True" />
                                                  <lwa:DataExporterActivity Description="Daten Ins Dateisystem schreiben" Destination="RawFile" DisplayName="RawExportFUS" Enabled="True" />
                                                  <lwa:SendTextMailActivity Description="" DisplayName="GenehmigtEmail" EmailMode="Immediate" Enabled="True" XSLTransformKey="EMail_Genehmigt" />
                                                </Sequence>
                                              </State.Entry>
                                              <State.Transitions>
                                                <Transition DisplayName="InitFUS">
                                                  <Transition.Trigger>
                                                    <lwa:ImmediateActivity Description="" DisplayName="Init FUS" Enabled="True" />
                                                  </Transition.Trigger>
                                                  <Transition.To>
                                                    <x:Reference>__ReferenceID2</x:Reference>
                                                  </Transition.To>
                                                </Transition>
                                              </State.Transitions>
                                            </State>
                                          </Transition.To>
                                        </Transition>
                                      </State.Transitions>
                                    </State>
                                  </Transition.To>
                                </Transition>
                                <Transition DisplayName="ÄndernEvent">
                                  <Transition.Trigger>
                                    <lwa:CommandActivity CommandKey="Change" CommandRoles="##Alle" Description="" DisplayName="Überarbeiten" Enabled="True" />
                                  </Transition.Trigger>
                                  <Transition.To>
                                    <x:Reference>__ReferenceID1</x:Reference>
                                  </Transition.To>
                                </Transition>
                              </State.Transitions>
                            </State>
                          </Transition.To>
                        </Transition>
                      </State.Transitions>
                    </State>
                  </Transition.To>
                </Transition>
                <Transition DisplayName="StornoOkEvent">
                  <Transition.Trigger>
                    <lwa:CommandActivity CommandKey="?Archivieren" CommandRoles="##Alle" Description="" DisplayName="Archivieren" Enabled="True" />
                  </Transition.Trigger>
                  <Transition.To>
                    <State x:Name="__ReferenceID4" DisplayName="Storno">
                      <State.Entry>
                        <Sequence DisplayName="InitStorno body">
                          <lwa:SetTabActivity Description="" DisplayName="TimlineTabStorno" Enabled="True" NewTab="800TIMELINE" />
                          <lwa:ExecuteRulesetActivity Action="" Description="" DisplayName="StornoRules" Enabled="True" RulesetName="Storno" />
                        </Sequence>
                      </State.Entry>
                      <State.Transitions>
                        <Transition DisplayName="InitStorno">
                          <Transition.Trigger>
                            <lwa:ImmediateActivity Description="" DisplayName="Init Storno" Enabled="True" />
                          </Transition.Trigger>
                          <Transition.To>
                            <x:Reference>__ReferenceID2</x:Reference>
                          </Transition.To>
                        </Transition>
                      </State.Transitions>
                    </State>
                  </Transition.To>
                </Transition>
              </State.Transitions>
            </State>
          </Transition.To>
        </Transition>
      </State.Transitions>
    </State>
    <State x:Name="__ReferenceID10" DisplayName="Init">
      <State.Entry>
        <Sequence DisplayName="Initialisierung body">
          <lwa:ExecuteRulesetActivity Action="" Description="Daten initialisieren, Konstanten naschlagen, etc." DisplayName="SetupRegeln" Enabled="True" RulesetName="Setup" />
        </Sequence>
      </State.Entry>
      <State.Transitions>
        <Transition DisplayName="Initialisierung">
          <Transition.Trigger>
            <lwa:ImmediateActivity Description="" DisplayName="Init Init" Enabled="True" />
          </Transition.Trigger>
          <Transition.To>
            <x:Reference>__ReferenceID1</x:Reference>
          </Transition.To>
        </Transition>
      </State.Transitions>
    </State>
    <x:Reference>__ReferenceID3</x:Reference>
    <x:Reference>__ReferenceID4</x:Reference>
    <x:Reference>__ReferenceID5</x:Reference>
    <x:Reference>__ReferenceID6</x:Reference>
    <x:Reference>__ReferenceID2</x:Reference>
    <x:Reference>__ReferenceID7</x:Reference>
    <x:Reference>__ReferenceID8</x:Reference>
    <x:Reference>__ReferenceID1</x:Reference>
    <x:Reference>__ReferenceID9</x:Reference>
  </StateMachine>
</Activity>`
