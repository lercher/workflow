package engine

import (
	"fmt"

	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/inspect"
)

// Run processes the NullTrigger, i.e. look for due timers (Delay, Every) and execute them
func Run(ctx *workflow.Context) error {
	return Process(ctx, workflow.NullTrigger)
}

// Process serches for an active Trigger in the current state
// that processes t and then runs those trigger's activities
// until the workflow idles or terminates.
func Process(ctx *workflow.Context, trg workflow.Trigger) error {
	// This has to be done here
	// ------------------------
	// increment ctx.TriggersProcessed and check upper bound to avoid long loops
	// find the trigger and its consequence
	// run the consequence
	// check ctx.NextState
	// Goto that State
	// check state is Final, if so set Terminated to true and return
	// Initialize it, i.e. Process the state's Init
	// Find immediate Triggers and run their consequences
	// Look into ctx.NextTrigger and Process it
	// repeat until no more needs to be done, signal idle
	// ------------------------

	ctx.TriggersProcessed = 0

	if !ctx.Instance.Alive() {
		// gracefully do nothing on a terminated instance
		return nil
	}

	if ctx.Instance.Current == "" {
		// Instance is a newly created one, we initialize the first state up front
		ctx.NextState = ctx.Instance.Definition.InitialState.DisplayName
		err := processStateChange(ctx)
		if err != nil {
			return err
		}
	}

	// normal processing
	ctx.NextState = ""
	tr := trg
	for {
		// first process all command triggers coming from the func call
		// in subsequent iterations we only process a due timer (Delay, Every)
		err := processTriggerWithoutDueTimers(ctx, tr)
		if err != nil {
			return err
		}

		// now we have processed every trigger of a single chain of ctx.NextTrigger.
		// so look for a due timer. if there is at least one, we process it.
		// if not, we can go idle
		if due, wakeup, ok := inspect.TriggerDue(ctx.Instance, ctx.Now); ok {
			ctx.Logf("trigger %v timed out by ctx.now %v", due, ctx.Now.Sub(wakeup))
			ctx.Instance.Timer[due.ID] = ctx.Now
			tr = due // next trigger to process
			continue
		}

		// all triggers due are processed here, so
		// find the next wake up time, if any
		ctx.WakeUp = workflow.Never
		if _, wu, ok := inspect.TriggerNextWakeUp(ctx.Instance, ctx.Now); ok {
			ctx.WakeUp = wu
		}

		// hurray, we can go idle now (or we terminated already)
		return nil
	}
}

func processTriggerWithoutDueTimers(ctx *workflow.Context, trg workflow.Trigger) (err error) {
	for tr := trg; !tr.IsNull(); tr = ctx.NextTrigger {
		ctx.NextTrigger = workflow.NullTrigger

		// guard for too many triggers fireing (e.g. in a never ending loop)
		ctx.TriggersProcessed++
		ctx.Logf("#%d processing trigger %v", ctx.TriggersProcessed, tr)
		if ctx.TriggersProcessed > ctx.TriggersProcessedMax {
			return fmt.Errorf("too much triggers %v (max %v) at %v, canceling execution", ctx.TriggersProcessed, ctx.TriggersProcessedMax, tr)
		}

		// find the trigger's consequence(r) and it's target state(to), if any
		r, to, ok := inspect.Runner(ctx.Instance, tr)
		if !ok {
			ctx.Logf("WARNING: unknown trigger %v ignored", tr)
			return nil
		}

		// record the target state. We change to it, unless a NewState activity directs us somewhere else
		if to != nil {
			ctx.NextState = to.DisplayName
			ctx.Logf("NextState by Trigger <- %q", ctx.NextState)
		}

		// run the trigger's consequence r
		err = processConsequence(ctx, r)
		if err != nil {
			return err
		}

		// evaluate ctx.NextState and initialize it if needed
		err := processStateChange(ctx)
		if err != nil {
			return err
		}

		// in case a business rule scheduled a new trigger, we process it next
	}
	return nil
}

func processStateChange(ctx *workflow.Context) error {
	for ctx.NextState != "" {
		if ctx.NextState == ctx.Instance.Current {
			ctx.Logf("NextState %q is already current state, nothing to do", ctx.NextState)
			ctx.NextState = ""
			return nil
		}
		ctx.Logf("Goto NextState %q", ctx.NextState)
		ctx.Instance = ctx.Goto(ctx.NextState, ctx.Now)
		ctx.NextState = ""

		err := initializeCurrentState(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

func initializeCurrentState(ctx *workflow.Context) (err error) {
	st, ok := ctx.Instance.Definition.State[ctx.Instance.Current]
	if !ok {
		return fmt.Errorf("unknown state %q to initialize", ctx.Instance.Current)
	}
	ctx.Logf("Initializing state %q", st.DisplayName)

	if st.IsFinal {
		ctx.Logf("State is final, instance terminates here")
	}

	err = processConsequence(ctx, st.Entry)
	if err != nil {
		return err
	}

	err = processImmediateTriggers(ctx)
	if err != nil {
		return err
	}

	return nil
}

func processImmediateTriggers(ctx *workflow.Context) error {
	for _, ti := range inspect.Triggers(ctx.Instance) {
		if ti.Enabled && ti.Trigger.IsImmediateKind() {
			err := processTriggerWithoutDueTimers(ctx, ti.Trigger)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func processConsequence(ctx *workflow.Context, r workflow.Runner) error {
	if r == nil {
		ctx.Logf("empty consequence")
		return nil
	}
	return r.Run(ctx)
}
