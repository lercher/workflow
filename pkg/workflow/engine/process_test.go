package engine

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/lercher/workflow/pkg/workflow"
	"gitlab.com/lercher/workflow/pkg/workflow/inspect"
)

func TestInitialize(t *testing.T) {
	def, _, _, svc := setup(t)
	i := def.CreateInstance()
	ctx := workflow.NewContext(i, svc, testNow)

	var list []string
	ctx.Logf = func(f string, v ...interface{}) {
		s := fmt.Sprintf(f, v...)
		t.Logf("%s", s)
		list = append(list, "{"+s+"}")
	}

	err := Run(ctx)
	if err != nil {
		t.Fatalf("Initialize (Run) error: %v", err)
	}

	want := `[{Goto NextState "Setup only"} {Initializing state "Setup only"} {*activity.ExecuteRuleset Setup Rules: "Setup" ""} {#1 processing trigger {3 }} {NextState by Trigger <- "Neu"} {empty consequence} {Goto NextState "Neu"} {Initializing state "Neu"} {*activity.Sequence (1)} {*activity.SetOwner #C-Owner: #C}]`
	got := fmt.Sprint(list)
	if got != want {
		t.Errorf("want %s, got %s", want, got)
	}

	wantSt := workflow.Statename("Neu")
	if ctx.Instance.Current != wantSt {
		t.Errorf("Run want state %q, got %q", wantSt, ctx.Instance.Current)
	}

	wantWakeup := testNow.Add(20 * time.Second)
	gotWakeup := ctx.WakeUp
	if !wantWakeup.Equal(gotWakeup) {
		t.Errorf("Wakeup after 20s, want %v, got %v", wantWakeup, gotWakeup)
	}
}

func TestProcess(t *testing.T) {
	def, angebot, sc, svc := setup(t)
	_ = angebot
	st := sc

	i := def.CreateInstance()
	i = i.Goto(st.DisplayName, testNow)
	ctx := workflow.NewContext(i, svc, testNow)

	var list []string
	ctx.Logf = func(f string, v ...interface{}) {
		s := fmt.Sprintf(f, v...)
		t.Logf("%s", s)
		list = append(list, "{"+s+"}")
	}

	t.Log("State", st.DisplayName, "triggers:", inspect.Triggers(i))
	err := Process(ctx, workflow.Trigger{Kind: 1, ID: "AnbGlobal"})
	if err != nil {
		t.Errorf("Process error: %v", err)
	}

	want := `[{#1 processing trigger {1 AnbGlobal}} {*activity.NewState Gehe zu Angebot: Angebot} {Goto NextState "Angebot"} {Initializing state "Angebot"} {*activity.Sequence (1)} {*activity.SetOwner : Angebot-Owner}]`
	got := fmt.Sprint(list)
	if got != want {
		t.Errorf("Process want %s, got %s", want, got)
	}
}

func TestRunPlusProcess(t *testing.T) {
	def, _, _, svc := setup(t)
	i := def.CreateInstance()
	ctx := workflow.NewContext(i, svc, testNow)

	var list []string
	ctx.Logf = func(f string, v ...interface{}) {
		s := fmt.Sprintf(f, v...)
		t.Logf("%s", s)
		list = append(list, "{"+s+"}")
	}

	t.Log("------------")
	err := Run(ctx)
	if err != nil {
		t.Fatalf("Initialize (Run) error: %v", err)
	}
	list = nil

	t.Log("------------")
	ctx.Now = testNow.Add(time.Second)
	err = Process(ctx, workflow.Trigger{Kind: workflow.TriggerKindAwait, ID: workflow.TriggerID("Offer")})
	if err != nil {
		t.Fatalf("Initialize (Run) error: %v", err)
	}

	want := `[{#1 processing trigger {1 Offer}} {NextState by Trigger <- "Angebot"} {*activity.Sequence (1)} {*activity.SetOwner : Anbieten-Owner} {Goto NextState "Angebot"} {Initializing state "Angebot"} {*activity.Sequence (1)} {*activity.SetOwner : Angebot-Owner}]`
	got := fmt.Sprint(list)
	if got != want {
		t.Errorf("want %s, got %s", want, got)
	}

	wantSt := workflow.Statename("Angebot")
	if ctx.Instance.Current != wantSt {
		t.Errorf("Run want state %q, got %q", wantSt, ctx.Instance.Current)
	}

	if ctx.TriggersProcessed != 1 {
		t.Errorf("ctx.TriggersProcessed want 1, got %d", ctx.TriggersProcessed)
	}

	// ------------------- Timer
	t.Log("------------")
	ctx.Now = testNow.Add(10 * time.Minute)
	list = nil
	err = Run(ctx)
	if err != nil {
		t.Fatalf("Timer (Run) error: %v", err)
	}

	want = `[{trigger {4 After 10s} timed out by ctx.now 9m49s} {#1 processing trigger {4 After 10s}} {NextState by Trigger <- "Neu"} {*activity.AddComment : "Zurück wegen 10s Timeout"} {Goto NextState "Neu"} {Initializing state "Neu"} {*activity.Sequence (1)} {*activity.SetOwner #C-Owner: #C}]`
	got = fmt.Sprint(list)
	if got != want {
		t.Errorf("want %s, got %s", want, got)
	}

	wantSt = workflow.Statename("Neu")
	if ctx.Instance.Current != wantSt {
		t.Errorf("Run want state %q, got %q", wantSt, ctx.Instance.Current)
	}

	if ctx.TriggersProcessed != 1 {
		t.Errorf("ctx.TriggersProcessed want 1, got %d", ctx.TriggersProcessed)
	}
}
