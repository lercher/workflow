package engine

import (
	"fmt"
	"sync/atomic"

	"gitlab.com/lercher/workflow/pkg/workflow"
)

var engines int64

type Engine struct {
	Name   string
	Ctx    *workflow.Context
	Idle   chan *Engine
	Signal chan workflow.Signal
}

func New(
	ctx *workflow.Context,
	idle chan *Engine,
	signal chan workflow.Signal,
) *Engine {
	n := atomic.AddInt64(&engines, 1)
	e := &Engine{
		Name:   fmt.Sprint("Engine", n),
		Ctx:    ctx,
		Idle:   idle,
		Signal: signal,
	}
	return e
}

// Run runs the workflow instance until it terminates.
// Should be called in a go routine.
// If the workflow idles it sends on the Idle channel
// If it terminates it closes the Signal channel
func (e *Engine) Run() {
	for s := range e.Signal {
		// need to step here
		e.Ctx.TriggerData = s.Data
		Process(e.Ctx, s.Trigger)
		e.Idle <- e
	}
	close(e.Signal)
}
