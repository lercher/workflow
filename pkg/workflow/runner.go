package workflow

// Runner is an interface an activity implements to initiate the activity's host defined behaviour
type Runner interface {
	Run(ctx *Context) error
}
