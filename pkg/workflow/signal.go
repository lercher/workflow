package workflow

// Signal is used to trigger a particular wf instance
// with data from e.g. a message queue
type Signal struct {
	InstanceDescriptor
	Trigger
	Data interface{}
}

// NullSignal constructs a Signal with the null trigger
func NullSignal(ID ID) Signal {
	return Signal{
		InstanceDescriptor: InstanceDescriptor{
			ID:     ID,
			WakeUp: Never,
		},
		Trigger: NullTrigger,
		Data:    nil,
	}
}
