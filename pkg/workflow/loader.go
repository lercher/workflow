package workflow

import (
	"encoding/xml"
	"fmt"
	"io"
)

// Forward is a function that keeps the location of a XAML forward reference to an object.
// It is called at the end of the decoding process, when all objects are known.
type Forward func() error

// Loader keeps the state during decoding a Definition from a XAML source
type Loader struct {
	XR           *xml.Decoder
	StartElement xml.StartElement
	Factory      map[xml.Name]DecoderFactory
	Definition   *Definition
	Reference    map[string]interface{} // Reference stores objects for later XAML references
	forwards     []Forward
	Indent       int
	id           int
}

// NewLoader creates a Loader from a XAML source
func NewLoader(r io.Reader) *Loader {
	xr := xml.NewDecoder(r)
	ld := &Loader{
		XR:         xr,
		Factory:    make(map[xml.Name]DecoderFactory),
		Definition: NewDefinition(),
		Reference:  make(map[string]interface{}),
	}
	ld.stdFactories()
	return ld
}

func (ld *Loader) stdFactories() {
	ld.Factory[Xname(XmlnsX, "Reference")] = func() Decoder { return &xamlreference{} }
	ld.Factory[Xname(XmlnsStd, "State")] = func() Decoder { return &State{} }
	ld.Factory[Xname(XmlnsStd, "Transition")] = func() Decoder { return &Transition{} }
}

// Token returns the current xml.Token the Loader currently looks at.
func (ld *Loader) Token() (xml.Token, error) {
again:
	tok, err := ld.XR.Token()
	if err != nil {
		return tok, err
	}
	switch t := tok.(type) {
	case xml.StartElement:
		if ignoredNamespaces[t.Name.Space] {
			ld.XR.Skip()
			goto again
		}
		ld.StartElement = t
		// log.Printf("%d+TOKEN <%s %v>", ld.Indent, t.Name.Local, kv(t.Attr))
	case xml.CharData:
		// if strings.TrimSpace(string(t)) != "" {
		//   log.Printf("%d=TOKEN '%s'", ld.Indent, t)
		// }
	case xml.EndElement:
		// log.Printf("%d-TOKEN </%s>", ld.Indent-1, t.Name.Local)
	}
	return tok, nil
}

func kv(attr []xml.Attr) (list []string) {
	for _, a := range attr {
		v := fmt.Sprintf("%s=%q", a.Name.Local, a.Value)
		list = append(list, v)
	}
	return list
}

// TokenString returns the string content of the current Token if it is a string, otherwise an error
func (ld *Loader) TokenString() (string, error) {
	tok, err := ld.Token()
	if err != nil {
		return "", fmt.Errorf("expected string content: %v", err)
	}
	txt, ok := tok.(xml.CharData)
	if !ok {
		return "", fmt.Errorf("expected string content, got %T", tok)
	}
	return string(txt), nil
}

// Decode decodes a Definition from the XAML source this Loader was created on
func (ld *Loader) Decode() (*Definition, error) {
	engineFound := false
	for {
		tok, err := ld.Token()
		if err != nil && err != io.EOF {
			return nil, err
		}
		if tok == nil {
			// complete
			if !engineFound {
				return nil, fmt.Errorf("there is no <StateEngine> in the xml")
			}
			// log.Println("RESOLVING", len(ld.forwards), "reference(s)")
			for _, fwd := range ld.forwards {
				err = fwd()
				if err != nil {
					return nil, err
				}
			}
			return ld.Definition, nil
		}

		if isStateMachine(tok) {
			engineFound = true
			err := ld.Definition.Decode(ld)
			if err != nil {
				return nil, err
			}
		}
	}
}

// AddForward adds a Forward function to this Loaders state. These
// funcs are called at the end of decoding, when all XAML forward references are known
func (ld *Loader) AddForward(fwd Forward) {
	ld.forwards = append(ld.forwards, fwd)
}

func isStateMachine(tok xml.Token) bool {
	if st, ok := tok.(xml.StartElement); ok {
		return st.Name.Space == XmlnsStd && st.Name.Local == "StateMachine"
	}
	return false
}

// Register registers an Activity factory for a particular <xmlns:localname> XAML element
func (ld *Loader) Register(xmlns, localname string, df DecoderFactory) {
	name := Xname(xmlns, localname)
	ld.Factory[name] = df
	// log.Printf("REGISTER <%s> in {%s}", localname, xmlns)
}

// NewContext creates a one level deeper LoaderContext for an object to load the inner XAML structure of an XML element
func (ld *Loader) NewContext(object interface{}) *LoaderContext {
	lc := NewLoaderContext(ld)
	if object != nil {
		lc.Attribute[Xname(XmlnsX, "Name")] = func(val interface{}) error {
			attribValue := val.(string)
			// log.Printf("SET-REF %s = %T", attribValue, object)
			if other, ok := ld.Reference[attribValue]; ok {
				return fmt.Errorf("set reference %s = %T is not allowed b/c it was %T already", attribValue, object, other)
			}
			ld.Reference[attribValue] = object
			return nil
		}
	}
	ld.Indent++
	return lc
}
