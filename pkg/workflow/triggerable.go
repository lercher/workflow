package workflow

type Triggerable interface {
	Trigger(Instance) TriggerInfo
}
