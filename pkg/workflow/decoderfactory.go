package workflow

// DecoderFactory creates a new workflow activity
// that is then initialized from XAML
// via its Decode method
type DecoderFactory func() Decoder
