package workflow

import (
	"testing"
	"time"
)

func TestNextWakeUpMiddle(t *testing.T) {
	every := 10 * time.Minute
	start := time.Date(2019, 1, 1, 12, 5, 0, 0, time.UTC)
	now := time.Date(2019, 1, 1, 15, 33, 0, 0, time.UTC)
	want := time.Date(2019, 1, 1, 15, 35, 0, 0, time.UTC)
	got := NextWakeup(start, now, every)
	if got != want {
		t.Errorf("NextWakeup(%v, %v, %v) got %v, want %v", start, now, every, got, want)
	}
}

func TestNextWakeUpEdgeCaseWantFollowing(t *testing.T) {
	every := 10 * time.Minute
	start := time.Date(2019, 1, 1, 12, 5, 0, 0, time.Local)
	now := time.Date(2019, 2, 15, 15, 45, 0, 0, time.Local)
	want := time.Date(2019, 2, 15, 15, 55, 0, 0, time.Local)
	got := NextWakeup(start, now, every)
	if got != want {
		t.Errorf("NextWakeup(%v, %v, %v) got %v, want %v", start, now, every, got, want)
	}
}
