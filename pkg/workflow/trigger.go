package workflow

var (
	// NullTrigger is the trigger of kind Null and with an empty TriggerID
	NullTrigger = Trigger{Kind: TriggerKindNull, ID: NullTriggerID}

	// HostLoadAssociatedDataTrigger is the Trigger to signal a host data load
	HostLoadAssociatedDataTrigger = Trigger{Kind: TriggerKindHost, ID: HostLoadAssociatedDataTriggerID}

	// HostChangeAssociatedDataTrigger is the Trigger to signal a change and store data read
	HostChangeAssociatedDataTrigger = Trigger{Kind: TriggerKindHost, ID: HostChangeAssociatedDataTriggerID}
)

// Trigger is a typed string to describe a single event that can happen for a workflow
type Trigger struct {
	Kind TriggerKind
	ID   TriggerID
}

// Is compares this Trigger with an other one on same Kind and ID
func (t Trigger) Is(other Trigger) bool {
	return t.Kind == other.Kind && t.ID == other.ID
}

// IsNull returns if the Trigger's Kind is TriggerKindNull
func (t Trigger) IsNull() bool {
	switch t.Kind {
	case TriggerKindNull:
		return true
	default:
		return false
	}
}

// IsTimeoutKind returns if the Trigger's Kind is TriggerKindWakeup
func (t Trigger) IsTimeoutKind() bool {
	switch t.Kind {
	case TriggerKindWakeup:
		return true
	default:
		return false
	}
}

// IsImmediateKind returns if the Trigger's Kind is TriggerKindImmediate
func (t Trigger) IsImmediateKind() bool {
	switch t.Kind {
	case TriggerKindImmediate:
		return true
	default:
		return false
	}
}

// IsHostKind returns if the Trigger's Kind is TriggerKindHost
func (t Trigger) IsHostKind() bool {
	switch t.Kind {
	case TriggerKindHost:
		return true
	default:
		return false
	}
}

// Signal creates a Signal from this Trigger with the given ID, WakeUp and nil as Data
func (t Trigger) Signal(descriptor InstanceDescriptor) Signal {
	return t.SignalWithData(descriptor, nil)
}

// SignalWithData creates a Signal from this Trigger with the given ID, WakeUp and Data
func (t Trigger) SignalWithData(descriptor InstanceDescriptor, data interface{}) Signal {
	return Signal{
		InstanceDescriptor: descriptor,
		Trigger:            t,
		Data:               data,
	}
}
