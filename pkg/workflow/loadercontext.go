package workflow

import (
	"encoding/xml"
	"fmt"
	"sort"
)

type LoaderContext struct {
	Ld        *Loader
	Attribute map[xml.Name]Resolver
	Property  map[xml.Name]Resolver
	Children  Resolver
}

type Resolver func(val interface{}) error

func NewLoaderContext(ld *Loader) *LoaderContext {
	return &LoaderContext{
		Ld:        ld,
		Attribute: make(map[xml.Name]Resolver),
		Property:  make(map[xml.Name]Resolver),
		Children:  func(interface{}) error { return nil },
	}
}

func (lc *LoaderContext) XamlProperty(xmlns, base, prop string, r Resolver) {
	lc.Attribute[Xname("", prop)] = r
	lc.Property[Xname(xmlns, base+"."+prop)] = r
}

func keys(m map[xml.Name]Resolver) []string {
	list := make([]string, 0, len(m))
	for k := range m {
		v := k.Local
		if k.Space == XmlnsX {
			v = "x:" + v
		}
		list = append(list, v)
	}
	sort.Strings(list)
	return list
}

func keysf(m map[xml.Name]DecoderFactory) []string {
	list := make([]string, 0, len(m))
	for k := range m {
		v := k.Local
		if k.Space == XmlnsX {
			v = "x:" + v
		}
		list = append(list, v)
	}
	sort.Strings(list)
	return list
}

func (lc *LoaderContext) Continue() error {
	// if len(lc.Attribute) > 0 {
	// 	log.Printf("EXPECT-ATTR %v (local-names)", keys(lc.Attribute))
	// }
	// if len(lc.Property) > 0 {
	// 	log.Printf("EXPECT-PROP %v (local-names)", keys(lc.Property))
	// }

	thiselement := lc.Ld.StartElement.Name

	// look at all attributes
	for _, a := range lc.Ld.StartElement.Attr {
		resolve, ok := lc.Attribute[a.Name]
		if ok {
			ref, ok := xamlreferenceFrom(a.Value)
			if ok {
				// log.Println("FORWARD-ATTR reference to", ref.s)
				lc.Ld.AddForward(ref.ForwardDereference(lc.Ld, resolve))
			} else {
				v := a.Value
				if v != "{x:Null}" {
					err := resolve(v)
					if err != nil {
						return fmt.Errorf("resolving attribute %v: %v", a, err)
					}
				}
			}
		}
	}

	// then process the token stream
	for {
		tok, err := lc.Ld.Token()
		if err != nil {
			return err
		}
	swtch:
		switch t := tok.(type) {
		case xml.StartElement:
			if resolve, ok := lc.Property[t.Name]; ok {
				// it's a named property, so load its content as object/reference
				ctx := lc.Ld.NewContext(nil)
				ctx.Children = resolve
				err := ctx.Continue()
				if err != nil {
					return fmt.Errorf("<%s> : %v", t.Name.Local, err)
				}
				break swtch
			}
			// it's an object or a xaml reference, load them and put it to children
			if factory, ok := lc.Ld.Factory[t.Name]; ok {
				dec := factory()
				lc.Ld.id++
				// id := lc.Ld.id
				// log.Printf("DECODING-%d %T", id, dec)
				err := dec.Decode(lc.Ld)
				// log.Printf("DECODED--%d %T", id, dec)
				if err != nil {
					return fmt.Errorf("%T.Decode<%v> : %v", dec, t.Name.Local, err)
				}
				if fd, ok := dec.(ForwardDereferencer); ok {
					// log.Println("FORWARD-OBJ reference to", fd)
					lc.Ld.AddForward(fd.ForwardDereference(lc.Ld, lc.Children))
				} else {
					// log.Printf("AS-CHILD %T %v", dec, dec)
					err = lc.Children(dec)
					if err != nil {
						return err
					}
				}
				break swtch
			}
			return fmt.Errorf(
				"there is neither a registered property nor an object factory for child token <%v> in {%v}, known %v",
				t.Name.Local,
				t.Name.Space,
				keysf(lc.Ld.Factory),
			)
		case xml.EndElement:
			lc.Ld.Indent--
			if t.Name.Local == thiselement.Local && t.Name.Space == thiselement.Space {
				// log.Print("RETURN OK")
				return nil
			}
			// log.Print("CONTINUE to next token")
		}
	}
}

func Xname(xmlns, localname string) xml.Name {
	return xml.Name{
		Space: xmlns,
		Local: localname,
	}
}
